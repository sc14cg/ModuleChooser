using System.Web.Mvc;
using ModuleChooser.Repositories;
using SignOn.Service;
using SignOn.Site.Controllers;
using Unity;
using Unity.AspNet.Mvc;

namespace SignOn.Site
{
    public static class Bootstrapper
    {
        public static void Initialise()
        {
            //Create unity container and set dependency resolver
            var container = BuildUnityContainer();

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }

        private static IUnityContainer BuildUnityContainer()
        {
            //Create Container
            var container = new UnityContainer();

            //Register Repositories to container
            container.RegisterType<IModuleRepository, ModuleRepository>();
            container.RegisterType<IAccountRepository, AccountRepository>();
            container.RegisterType<ISignOnService, SignOnService>();

            //Register Controller to container
            container.RegisterType<IController, AccountController>("Module");

            return container;
        }
    }
}
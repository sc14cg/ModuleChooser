﻿using System.Web;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NSubstitute;
using SignOn.Service;
using SignOn.Site.Controllers;

namespace SignOn.Site.Tests
{
    [TestClass]
    public class ControllerTests
    {
        private AccountController _accountController;

        public void SetUp()
        {
            //Mock the Sign on Service
            var service = Substitute.For<ISignOnService>();

            //Create controller to test
            _accountController = new AccountController(service);

            //Create mock of request
            var request = new Mock<HttpRequestBase>();

            request.SetupGet(x => x.IsAuthenticated)
                .Returns(false);

            //Create controllers context mock using user information
            var contextMock = new Mock<HttpContextBase>();
            contextMock.SetupGet(ctx => ctx.Request)
                       .Returns(request.Object);

            var controllerContextMock = new Mock<ControllerContext>();
            controllerContextMock.SetupGet(con => con.HttpContext)
                                 .Returns(contextMock.Object);

            _accountController.ControllerContext = controllerContextMock.Object;
        }

        [TestMethod]
        public void Login_Returns_ActionResult()
        {
            SetUp();

            //Act of loging in
            var result = _accountController.Login("returnURL");

            //Assert that the action result is returned
            Assert.IsInstanceOfType(result, typeof(ActionResult));
        }
    }
}
﻿using SignOn.Service;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Security;

namespace SignOn.Site.Controllers
{
    public class AccountController : Controller
    {
        //Initialise the service to get data
        private readonly ISignOnService _signOnService;

        //Service injected to controller and value is assigned to the variable
        public AccountController(ISignOnService signOnService)
        {
            _signOnService = signOnService;
        }

        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            //Check if request is authenticated, if so then redirect to either the admin site or the student site depending on role
            if (Request.IsAuthenticated)
            {
                if (Roles.IsUserInRole(User.Identity.Name, "Admin"))
                {
                    return Redirect("https://localhost:24080/Administration");
                }
                else
                {
                    return Redirect("https://localhost:24080/Student");
                }
            }

            //Return to the return url and render view
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(string username, string password, string returnUrl)
        {
            //Check if the login details are correct
            var detailsCorrect = await _signOnService.CheckLoginDetails(username, password);

            //If the details are correct then redirect to relevant view.
            if (detailsCorrect)
            {
                //Set authentication cookie. Redirect to return url if there is one.
                FormsAuthentication.SetAuthCookie(username, false);
                if (!string.IsNullOrEmpty(returnUrl))
                {
                    return Redirect(returnUrl);
                }
                else
                {
                    //Get users roles and go to either the admin site or student site depending on this.
                    var roles = await _signOnService.GetRolesForUser(username);
                    if (roles.Contains("Admin"))
                    {
                        return Redirect("https://localhost:24080/Administration");
                    }
                    else
                    {
                        return Redirect("https://localhost:24080/Student");
                    }
                }
            }
            else
            {
                //Show invalid login details validation error and go back to login screen
                ModelState.AddModelError("Password", "Invalid login details");
                ViewBag.ReturnUrl = returnUrl;
                ViewBag.Username = username;

                return View();
            }
        }

        public ActionResult SignOut()
        {
            //Sign out and redirect to potential student page
            FormsAuthentication.SignOut();
            return Redirect("https://localhost:24080/");
        }
    }
}
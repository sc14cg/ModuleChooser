using System.Web.Mvc;
using Unity.AspNet.Mvc;
using ModuleChooser.Repositories;
using AutoMapper;
using Student.Site.Controllers;
using Student.Service;
using Student.Site.Mappings;
using Unity;
using Unity.Injection;

namespace Student
{
    public static class Bootstrapper
    {
        public static void Initialise()
        {
            //Create unity container and set dependency resolver
            var container = BuildUnityContainer();

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }

        private static IUnityContainer BuildUnityContainer()
        {
            //Create Container
            var container = new UnityContainer();

            //Register Repositories to container
            container.RegisterType<IModuleRepository, ModuleRepository>();
            container.RegisterType<ICourseRepository, CourseRepository>();
            container.RegisterType<IStudentRepository, StudentRepository>();
            container.RegisterType<IReportsRepository, ReportsRepository>();

            //Register Services to container
            container.RegisterType<IStudentService, StudentService>();

            //Register AutoMapper to container
            var mapperConfig = ModuleMappings.IntialiseAutoMapper();
            var mapper = mapperConfig.CreateMapper();
            container.RegisterType<IMapper, Mapper>(new InjectionConstructor(mapperConfig));

            //Register Controllers to container
            container.RegisterType<IController,  ModuleController>("Module");

            return container;
        }
    }
}
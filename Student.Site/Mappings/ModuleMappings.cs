﻿using AutoMapper;
using ModuleChooser.Resources;
using ModuleChooser.Resources.Dtos;
using Student.Site.Models;

namespace Student.Site.Mappings
{
    public class ModuleMappings
    {
        public static MapperConfiguration IntialiseAutoMapper()
        {
            //Use AutoMapper to map one class to another
            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<ModuleInformationDto, ModuleModel>();
                cfg.CreateMap<ModuleInformationDto, ModuleListModel>();
                cfg.CreateMap<StudentModuleInformationDto, ModuleListModel>();
                cfg.CreateMap<ModuleModel, ModuleInformationDto>();
            });

            return config;
        }
    }
}
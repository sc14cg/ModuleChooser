﻿using System;
using System.Collections.Generic;
using System.Security.Principal;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ModuleChooser.Resources;
using ModuleChooser.Resources.Dtos;
using ModuleChooser.Resources.Enums;
using Moq;
using NSubstitute;
using Student.Service;
using Student.Site.Controllers;


namespace Student.Site.Tests
{
    [TestClass]
    public class ControllerTests
    {
        private ModuleController moduleController;

        public void SetUp()
        {
            //Mock the Student Service
            var service = Substitute.For<IStudentService>();

            //Mock the Services Methods.
            service.GetModulesForUser(Arg.Any<String>())
                .Returns(Task.FromResult(Substitute.For<List<StudentModuleInformationDto>>()));

            service.GetCourseForStudentUser(Arg.Any<String>())
                .Returns(Substitute.For<CourseCompactDto>());

            service.GetModulePreferencesForStudentUser(Arg.Any<String>())
                .Returns(Task.FromResult(Substitute.For<List<Guid>>()));

            service.GetStudentName(Arg.Any<String>())
                .Returns("Callum");

            service.SaveStudentPreferredModuleOption(Arg.Any<Guid>(), Arg.Any<String>())
                .Returns(SaveStudentPreferredModuleOptionResponse.Success);

            service.RemoveStudentPreferredModuleOption(Arg.Any<Guid>(), Arg.Any<String>());

            //Create controller to test
            moduleController = new ModuleController(service, Mock.Of<IMapper>());

            //Create mock of user
            var userMock = new Mock<IPrincipal>();
            userMock.Setup(p => p.IsInRole("Student")).Returns(true);
            userMock.Setup(p => p.Identity.Name).Returns("callumgoodridge1");

            //Create controllers context mock using user information
            var contextMock = new Mock<HttpContextBase>();
            contextMock.SetupGet(ctx => ctx.User)
                       .Returns(userMock.Object);

            var controllerContextMock = new Mock<ControllerContext>();
            controllerContextMock.SetupGet(con => con.HttpContext)
                                 .Returns(contextMock.Object);

            moduleController.ControllerContext = controllerContextMock.Object;
        }

        [TestMethod]
        public async Task ModuleList_Returns_ViewResult()
        {
            SetUp();

            var result = await moduleController.ModuleList();

            Assert.AreEqual(result.ViewName, "ModuleList");
            Assert.IsInstanceOfType(result, typeof(ViewResult));
        }

        [TestMethod]
        public async Task SaveStudentPreferredModuleOption_Returns_JsonResult()
        {
            SetUp();

            var result = await moduleController.SaveStudentPreferredModuleOption(Guid.NewGuid());

            Assert.IsInstanceOfType(result, typeof(JsonResult));
        }

        [TestMethod]
        public async Task RemoveStudentPreferredModuleOption_Returns_JsonResult()
        {
            SetUp();

            var result = await moduleController.SaveStudentPreferredModuleOption(Guid.NewGuid());

            Assert.IsInstanceOfType(result, typeof(JsonResult));
        }
    }
}
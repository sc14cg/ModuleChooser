﻿using AutoMapper;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using Student.Service;
using Student.Site.Models;
using SignOn.Helpers;
using System;
using System.Linq;
using ModuleChooser.Resources.Dtos;

namespace Student.Site.Controllers
{
    [ModuleChooserAuthorize(Roles = "Student")]
    public class ModuleController : Controller
    {
        //Initialise the service to get data and the mapper used to map classes of one type to another
        private readonly IStudentService _service;
        private readonly IMapper _mapper;

        //Service and mappers injected to controller and values are assigned to the variables
        public ModuleController(IStudentService service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        public async Task<ViewResult> ModuleList()
        {
            //Get the user details
            var user = User.Identity.Name;

            //Get modules for this user
            var dtos = await _service.GetModulesForUser(user);

            //Get the students course
            var studentCourse = _service.GetCourseForStudentUser(user);

            //Get the students preferences
            var preferences = await _service.GetModulePreferencesForStudentUser(user);

            //Get the students name
            var studentName = await _service.GetStudentName(user);

            //Order the modules by compulsory ones first, then year of study, then by semester
            dtos = dtos.OrderBy(m => m.Compulsory)
                .ThenBy(m => m.YearOfStudy).ThenBy(m => m.Semester).ToList();

            //Get the compulsory and preferred modules from the list
            var compulsoryModules = dtos.Where(m => m.Compulsory).Select(m => m.ModuleId);
            var preferredModules = dtos.Where(m => m.Preferred).Select(m => m.ModuleId);

            //Don't perform the foreach on compulsory modules.
            foreach (var module in dtos.Where(m => !m.Compulsory))
            {
                //Get the module prerequisites and order these by year and semeseter
                module.ModulePrerequisites = _service.GetPrerequisitesForModule(module.ModuleId)
                    .OrderBy(p => p.PrerequisiteYearOfStudy).ThenBy(p => p.PrerequisiteSemester).ToList();

                foreach (var mp in module.ModulePrerequisites)
                {
                    //Check if the prerequisite module is compulsory
                    if (compulsoryModules.Contains(mp.PrerequisiteModuleId))
                    {
                        mp.PrerequisiteCompulsory = true;
                    }
                    //Check if the prerequisite module is preferred by the user
                    if (preferredModules.Contains(mp.PrerequisiteModuleId))
                    {
                        mp.PrerequisitePreferred = true;
                    }
                }

                //If the moduleprerequisites are all either compulsory or preferred then allow the module to be chosen
                if (module.ModulePrerequisites
                    .All(mp => mp.PrerequisiteCompulsory || mp.PrerequisitePreferred))
                {
                    module.AbleToBeChosen = true;
                }

                //Get the module FutureAvailableOptions and order these by year and semeseter
                module.FutureAvailableOptions = _service.GetFutureAvailableOptionsForModule(module.ModuleId)
                    .OrderBy(fao => fao.FutureAvailableYearOfStudy).ThenBy(fao => fao.FutureAvailableSemester).ToList();

                //Set the module as prefered if it is.
                module.Preferred = preferences.Contains(module.ModuleId);

                var preferredTogetherModule = new PreferredTogetherModulesChartDataDto();

                //Get the preferred module together suggestion
                preferredTogetherModule = await _service.PreferredTogetherModuleSuggestion(module.ModuleCode, user);

                //Assign this suggestion to the module
                module.PreferredTogetherModule = preferredTogetherModule;
            }

            //Map the dto list values to the list of module models to be used with the view
            var model = _mapper.Map<List<ModuleListModel>>(dtos);

            //Set the course tile and student name on the page
            ViewBag.Course = studentCourse.Title;
            ViewBag.StudentName = studentName;

            //Render the module list page, passing through the view
            return View("ModuleList", model);
        }

        public async Task<JsonResult> SaveStudentPreferredModuleOption(Guid moduleId)
        {
            //Get the username and pass the module id, and username to the SaveStudentPreferredModuleOption method
            var user = User.Identity.Name;
            var saveResponse = await _service.SaveStudentPreferredModuleOption(moduleId, user);

            //Return JSON save response message
            return Json(saveResponse);
        }

        public async Task<JsonResult> RemoveStudentPreferredModuleOption(Guid moduleId, int yearOfStudy)
        {
            //Get the username and pass the module id, and username to the RemoveStudentPreferredModuleOption method
            var user = User.Identity.Name;
            await _service.RemoveStudentPreferredModuleOption(moduleId, user);

            //Return json response with yearofstudy and moduleid
            return Json(new {  moduleId, yearOfStudy });
        }
    }
}
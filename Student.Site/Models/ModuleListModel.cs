﻿using ModuleChooser.Resources.Dtos;
using System.Collections.Generic;

namespace Student.Site.Models
{
    public class ModuleListModel : ModuleModel
    {
        public bool Compulsory { get; set; }

        public List<PrerequisitesForModuleDto> ModulePrerequisites { get; set; }

        public List<FutureAvailableOptionsFromModuleDto> FutureAvailableOptions { get; set; }

        public bool Preferred { get; set; }

        public bool AbleToBeChosen { get; set; }

        public PreferredTogetherModulesChartDataDto PreferredTogetherModule { get; set; }
    }
}
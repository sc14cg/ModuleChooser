﻿using System;

namespace Student.Site.Models
{
    public class ModuleModel
    {
        public string ModuleCode { get; set; }

        public string Title { get; set; }

        public string Semester { get; set; }

        public int Credits { get; set; }

        public int YearOfStudy { get; set; }

        public Guid ModuleId { get; set; }

        public string Url { get; set; }
    }
}
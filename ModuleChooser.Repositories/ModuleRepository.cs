﻿using ModuleChooser.Entities;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using System;
using ModuleChooser.Repositories.Mappings;
using ModuleChooser.Resources;
using ModuleChooser.Resources.Dtos;
using ModuleChooser.Resources.Enums;

namespace ModuleChooser.Repositories
{
    public class ModuleRepository : IModuleRepository
    {
        //Initialise the context to get data and the mapper to map some values from one class to another.
        private readonly Context _entities;
        private readonly IMapper _mapper;

        //Entities injected to repository and value is assigned to the variable
        public ModuleRepository(Context entities)
        {
            _entities = entities;

            //Initialise the automapper and assign details to variable
            var mapperConfig = ModuleMappings.IntialiseAutoMapper();
            var mapper = mapperConfig.CreateMapper();
            _mapper = mapper;
        }

        public async Task<List<ModuleInformationDto>> GetModules()
        {
            //Get all modules and information, return this as a list of ModuleInformationDtos
            return await _entities.Modules.Select(s => new ModuleInformationDto()
            {
                Credits = s.Credits,
                ModuleCode = s.ModuleCode,
                Semester = s.Semester,
                Title = s.Title,
                YearOfStudy = s.YearOfStudy,
                ModuleId = s.ModuleId
            }).ToListAsync();
        }

        public async Task<SaveModuleResponseEnum> AddNewModule(SaveModuleDto dto)
        {
            //Check if the title already exists, if so return TitleExists message
            if (await TitleAlreadyExists(dto))
            {
                return SaveModuleResponseEnum.TitleExists;
            }

            //Check if the code already exists, if so return CodeExists message
            if (await CodeAlreadyExists(dto))
            {
                return SaveModuleResponseEnum.CodeExists;
            }

            //Map the module dto information to the module entity class and add this to the database. Save this. 
            var moduleToAdd = _mapper.Map<Module>(dto);
            _entities.Modules.Add(moduleToAdd);
            await _entities.SaveChangesAsync();

            //Return success response.
            return SaveModuleResponseEnum.Success;
        }

        public ModuleEditInformationDto GetModuleEditInformation(Guid moduleId)
        {
            //Get module
            var module = _entities.Modules.Find(moduleId);

            //Map the module data to the ModuleEditInformationDto and return this
            return _mapper.Map<ModuleEditInformationDto>(module);
        }

        private async Task<bool> CodeAlreadyExists(SaveModuleDto dto)
        {
            //Check if a module code already exists with the same code as the one submitted. Return answer
            if (!dto.ModuleId.HasValue)
            {
                return await _entities.Modules
                    .AnyAsync(m => m.ModuleCode == dto.ModuleCode);
            }
            else
            {
                //For existing modules, check if the ModuleId does not match too.
                return await _entities.Modules
                    .AnyAsync(m => m.ModuleCode == dto.ModuleCode && m.ModuleId != dto.ModuleId);
            }
        }

        private async Task<bool> TitleAlreadyExists(SaveModuleDto dto)
        {
            //Check if a module title already exists with the same title as the one submitted.
            if (!dto.ModuleId.HasValue)
            {
                return await _entities.Modules
                    .AnyAsync(m => m.Title == dto.Title);
            }
            else
            {
                //For existing modules, check if the ModuleId does not match too.
                return await _entities.Modules
                    .AnyAsync(m => m.Title == dto.Title && m.ModuleId != dto.ModuleId);
            }
        }

        public async Task<SaveModuleResponseEnum> SaveExistingModule(SaveModuleDto dto)
        {
            //Check if the module title and code already exist. If so return issue response.
            if (await TitleAlreadyExists(dto))
            {
                return SaveModuleResponseEnum.TitleExists;
            }
            if (await CodeAlreadyExists(dto))
            {
                return SaveModuleResponseEnum.CodeExists;
            }

            //If module details are unique then find the module and save the new details
            var module = _entities.Modules.Find(dto.ModuleId);

            module.ModuleCode = dto.ModuleCode;
            module.Semester = dto.Semester;
            module.Credits = dto.Credits;
            module.Title = dto.Title;
            module.YearOfStudy = dto.YearOfStudy;
            module.Url = dto.Url;

            await _entities.SaveChangesAsync();

            //Return success response
            return SaveModuleResponseEnum.Success;
        }

        public Task<List<ModuleCompactDto>> GetCompactModules()
        {
            //Get basic module information. Lighter than returning all the information if it is not needed.
            var modules = _entities.Modules.Select(s =>
                new ModuleCompactDto()
                {
                    ModuleCode = s.ModuleCode,
                    ModuleId = s.ModuleId,
                    Title = s.Title
                })
                .ToListAsync();

            //Return compact module information as a list
            return modules;
        }

        public async Task<SavePrerequisiteResponseEnum> SavePrerequisite(Guid moduleId, Guid prerequisiteId)
        {
            //Get the module links
            var moduleLinks = _entities.ModuleLinks;

            //Check if the link already exists
            var alreadyPresent = moduleLinks
                .Any(m => m.ModuleId == moduleId && m.PrerequisiteId == prerequisiteId);

            //Don't allow for module links to be made twice
            if (alreadyPresent)
            {
                return SavePrerequisiteResponseEnum.AlreadyExists;
            }

            //Get the modules and get the module information as well as prereq information
            var modules = _entities.Modules;
            var module = await modules.FirstOrDefaultAsync(m => m.ModuleId == moduleId);
            var prerequisite = await modules.FirstOrDefaultAsync(m => m.ModuleId == prerequisiteId);

            //Don't allow for future modules to be assigned as prerequisites for earlier modules
            if (module.YearOfStudy < prerequisite.YearOfStudy ||
                (module.YearOfStudy == prerequisite.YearOfStudy && module.Semester < prerequisite.Semester))
            {
                return SavePrerequisiteResponseEnum.FutureModuleUsed;
            }

            //Add new link and save these changes. Return a success response.
            moduleLinks.Add(new ModuleLink()
            {
                ModuleId = moduleId,
                PrerequisiteId = prerequisiteId
            });

            await _entities.SaveChangesAsync();

            return SavePrerequisiteResponseEnum.Success;
        }

        public async Task<List<StudentModuleInformationDto>> GetModulesForUser(string username)
        {
            //Get the login information of the user and use this to find what course the student is on.
            var login = await _entities.Logins.FindAsync(username);
            var courseId = await _entities.Students
                .Where(s => s.UserId == login.UserId)
                .Select(s => s.CourseId)
                .FirstOrDefaultAsync();

            //Get the modules for a course and return these
            return await GetModulesForCourse(courseId);
        }

        public async Task<List<StudentModuleInformationDto>> GetModulesForCourse(Guid courseId)
        {
            //Using the students course, find the relevent course modules.
            var courseModules = await _entities.CourseModules
                .Where(cm => cm.CourseId == courseId)
                .ToListAsync();

            //return a list of course modules for the user
            var result = courseModules.Select(s => new StudentModuleInformationDto()
            {
                Credits = s.Module.Credits,
                Semester = s.Module.Semester,
                ModuleCode = s.Module.ModuleCode,
                ModuleId = s.Module.ModuleId,
                Title = s.Module.Title,
                YearOfStudy = s.Module.YearOfStudy,
                Compulsory = s.Compulsory,
                Url = s.Module.Url
            }).ToList();

            //Return list of StudentModuleInformationDto
            return result;
        }

        public async Task<List<PrerequisiteListDto>> GetPrerequisites()
        {
            //Get the prerequisites and return the result as a list.
            return await _entities.ModuleLinks.OrderBy(ml => ml.PrerequisiteModule.YearOfStudy)
                .Select(ml => new PrerequisiteListDto()
                {
                    Module = ml.Module.Title,
                    Prerequisite = ml.PrerequisiteModule.Title
                })
                .ToListAsync();
        }

        public List<PrerequisitesForModuleDto> GetPrerequisitesForModule(Guid moduleIdParam)
        {
            //Initialise variables
            var listOfModules = new List<PrerequisitesForModuleDto>();
            var moduleIds = new List<Guid>();
            var prerequisites = new List<PrerequisitesForModuleDto>();

            //Add initial moduleId from argument into list of module ids
            moduleIds.Add(moduleIdParam);

            //Start the loop
            var running = true;
            while (running)
            {
                //This moduleIds list will initially just be the moduleIdParam, but will later be appended to in the code
                foreach (var moduleId in moduleIds)
                {
                    //Get the prerequisite information for the module
                    prerequisites = _entities.ModuleLinks
                        .Where(ml => ml.ModuleId == moduleId)
                        .Select(ml => new PrerequisitesForModuleDto()
                        {
                            PrerequisiteCredits = ml.PrerequisiteModule.Credits,
                            PrerequisiteModuleCode = ml.PrerequisiteModule.ModuleCode,
                            PrerequisiteModuleId = ml.PrerequisiteId,
                            PrerequisiteSemester = ml.PrerequisiteModule.Semester.ToString(),
                            PrerequisiteTitle = ml.PrerequisiteModule.Title,
                            PrerequisiteYearOfStudy = ml.PrerequisiteModule.YearOfStudy,
                        })
                        .ToList();
                }

                if (prerequisites.Count() == 0 || moduleIds.Count() == 0)
                {
                    //No more prerequisites returned from query therefore return the final list of prerequisite modules
                    running = false;
                }
                else
                {
                    //Remove values which have already been checked.
                    moduleIds.Clear();

                    //For each prerequisite, add the prerequisite to the final list.
                    //and then add the prerequisiteId to the moduleids to be checked above 
                    foreach (var prereq in prerequisites)
                    {
                        if (!listOfModules.Any(m => m.PrerequisiteModuleId == prereq.PrerequisiteModuleId))
                        {
                            listOfModules.Add(prereq);
                            moduleIds.Add(prereq.PrerequisiteModuleId);
                        }
                    }
                }
            }

            return listOfModules;
        }

        public List<FutureAvailableOptionsFromModuleDto> GetFutureAvailableOptionsForModule(Guid moduleIdParam)
        {
            //Initialise variables
            var listOfModules = new List<FutureAvailableOptionsFromModuleDto>();
            var moduleIds = new List<Guid>();
            var futureOptions = new List<FutureAvailableOptionsFromModuleDto>();

            //Add initial moduleId from argument into list of module ids
            moduleIds.Add(moduleIdParam);

            //Start the loop
            var running = true;
            while (running)
            {
                foreach (var moduleId in moduleIds)
                {
                    //Get future options using module id
                    futureOptions = _entities.ModuleLinks
                        .Where(ml => ml.PrerequisiteId == moduleId)
                        .Select(ml => new FutureAvailableOptionsFromModuleDto()
                        {
                            FutureAvailableCredits = ml.Module.Credits,
                            FutureAvailableModuleCode = ml.Module.ModuleCode,
                            FutureAvailableModuleId = ml.ModuleId,
                            FutureAvailableSemester = ml.Module.Semester.ToString(),
                            FutureAvailableTitle = ml.Module.Title,
                            FutureAvailableYearOfStudy = ml.Module.YearOfStudy
                        })
                        .ToList();
                }

                if (futureOptions.Count() == 0 || moduleIds.Count() == 0)
                {
                    //No more future options returned from query therefore return the final list of future option modules
                    running = false;
                }
                else
                {
                    //Remove values which have already been checked.
                    moduleIds.Clear();

                    //For each future option, add the option to the final list.
                    //and then add the moduleId to the moduleids to be checked above 
                    foreach (var option in futureOptions)
                    {
                        if (!listOfModules.Any(m => m.FutureAvailableModuleId == option.FutureAvailableModuleId))
                        {
                            listOfModules.Add(option);
                            moduleIds.Add(option.FutureAvailableModuleId);
                        }
                    }
                }
            }

            return listOfModules;
        }

        public async Task<List<Guid>> GetModulePreferencesForStudentUser(string username)
        {
            //Use the students username to find the login details and student Id 
            var login = await _entities.Logins.FindAsync(username);
            var studentId = await _entities.Students
                .Where(s => s.UserId == login.UserId)
                .Select(s => s.StudentId)
                .FirstOrDefaultAsync();

            //Get the current module preferences for that student
            var moduleIds = await _entities.StudentModulePreferences
                .Where(smp => smp.StudentId == studentId)
                .Select(smp => smp.ModuleId)
                .ToListAsync();

            //return a list of module ids
            return moduleIds;
        }

        public async Task<SaveStudentPreferredModuleOptionResponse> SaveStudentPreferredModuleOption(Guid moduleId, string username)
        {
            //Use the students username to find the login details and student Id 
            var login = await _entities.Logins.FindAsync(username);
            var studentId = await _entities.Students
                .Where(s => s.UserId == login.UserId)
                .Select(s => s.StudentId)
                .FirstOrDefaultAsync();

            //Check if the preferred module already exists. If so don't attempt to add new database entry
            var preferredModuleAlreadyExists = _entities.StudentModulePreferences
                .Any(smp => smp.StudentId == studentId && smp.ModuleId == moduleId);

            //If module preference already exists. Return response
            if (preferredModuleAlreadyExists)
            {
                return SaveStudentPreferredModuleOptionResponse.PreferenceAlreadyExists;
            }

            //Get the selected module information
            var selectedModule = await _entities.Modules.FirstAsync(m => m.ModuleId == moduleId);

            //Use the selected module information to get the year of the module and get the number of credits already prefered for that year.
            var creditsForAlreadySelectedPreferences = _entities.StudentModulePreferences
                .Where(smp => smp.Module.YearOfStudy == selectedModule.YearOfStudy && smp.StudentId == studentId)
                .Select(smp => smp.Module.Credits);

            var userModules = await GetModulesForUser(username);

            //Get the credits for compulsory modules
            var creditsForCompulsoryModules = userModules
                .Where(um => um.Compulsory && um.YearOfStudy == selectedModule.YearOfStudy)
                .Select(um => um.Credits);

            //Add the credits together to get a total sum
            var creditsSum = 0;
            foreach (var credits in creditsForCompulsoryModules)
            {
                creditsSum += credits;
            }
            foreach (var credits in creditsForAlreadySelectedPreferences)
            {
                creditsSum += credits;
            }

            //If the credits is above the maximum credits of 120 then return the message response
            if (creditsSum + selectedModule.Credits > 120)
            {
                return SaveStudentPreferredModuleOptionResponse.CreditsLimitReached;
            }

            //If there are no issues, add the new preference and save this information to the database.
            _entities.StudentModulePreferences
                .Add(new StudentModulePreference()
                {
                    ModuleId = moduleId,
                    StudentId = studentId
                });

            await _entities.SaveChangesAsync();

            //return success response
            return SaveStudentPreferredModuleOptionResponse.Success;
        }

        public async Task RemoveStudentPreferredModuleOption(Guid moduleId, string username)
        {
            //Use the students username to find the login details and student Id 
            var login = await _entities.Logins.FindAsync(username);
            var studentId = await _entities.Students
                .Where(s => s.UserId == login.UserId)
                .Select(s => s.StudentId)
                .FirstOrDefaultAsync();

            //Get the preference
            var preference = await _entities.StudentModulePreferences
                .FirstOrDefaultAsync(smp => smp.ModuleId == moduleId && smp.StudentId == studentId);

            //If the preference is definitely there, then remove this and save the changes.
            if (preference != null)
            {
                _entities.StudentModulePreferences.Remove(preference);

                await _entities.SaveChangesAsync();
            }
        }
    }
}

﻿
using AutoMapper;
using ModuleChooser.Entities;
using ModuleChooser.Resources;

namespace ModuleChooser.Repositories.Mappings
{
    public class ModuleMappings
    {
        public static MapperConfiguration IntialiseAutoMapper()
        { 
            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<SaveModuleDto, Module>();

                cfg.CreateMap<ModuleDto, Module>();

                cfg.CreateMap<Module, ModuleEditInformationDto>();
            });

            return config;
        }
    }
}
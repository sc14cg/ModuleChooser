﻿
using AutoMapper;
using ModuleChooser.Entities;
using ModuleChooser.Resources;

namespace ModuleChooser.Repositories.Mappings
{
    public class CourseMappings
    {
        public static MapperConfiguration IntialiseAutoMapper()
        { 
            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<CourseDto, Course>();
            });

            return config;
        }
    }
}
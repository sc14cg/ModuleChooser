﻿using ModuleChooser.Entities;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System;
using ModuleChooser.Resources;
using ModuleChooser.Resources.Dtos;

namespace ModuleChooser.Repositories
{
    public class StudentRepository : IStudentRepository
    {
        //Initialise the context to get data
        private readonly Context _entities;

        //Entities injected to repository and value is assigned to the variable
        public StudentRepository(Context entities)
        {
            _entities = entities;
        }

        public async Task<List<StudentListInformationDto>> GetStudents()
        {
            //Get all the students information and return as a list of StudentListInformationDto
            return await _entities.Students.Select(s => new StudentListInformationDto()
            {
                CourseId = s.CourseId,
                CourseTitle = s.Course.Title,
                FullName = s.User.LastName.ToUpper() + ", " + s.User.FirstName,
                StudentId = s.StudentId,
                YearOfStudy = s.YearOfStudy,
            }).ToListAsync();
        }

        public async Task<StudentEditInformationDto> GetStudentEditInformation(Guid studentId)
        {
            //Get the student using the id and return the StudentEditInformationDto
            var student = await _entities.Students.FindAsync(studentId);
            return new StudentEditInformationDto()
                {
                    CourseId = student.CourseId,
                    FirstName = student.User.FirstName,
                    Surname = student.User.LastName,
                    StudentId = student.StudentId,
                    YearOfStudy = student.YearOfStudy,
                };
        }

        public async Task SaveExistingStudent(SaveStudentDto dto)
        {
            //Get the student using the unique id
            var student = await _entities.Students.FindAsync(dto.StudentId);

            //Assign the values to the entity, save this.
            student.CourseId = dto.CourseId;
            student.YearOfStudy = dto.YearOfStudy;
            student.User.FirstName = dto.FirstName;
            student.User.LastName = dto.Surname;

            await _entities.SaveChangesAsync();
        }

        public async Task SaveNewStudent(SaveStudentDto dto)
        {
            //Add a new user
            var newUser = _entities.Users.Add(new User()
            {
                FirstName = dto.FirstName,
                LastName = dto.Surname,
                UserId = Guid.NewGuid()
            });

            //Add a new default login
            _entities.Logins.Add(new Login()
            {
                UserId = newUser.UserId,
                Username = dto.Username,
                Password = "Password1"
            });

            //Add student information
            _entities.Students.Add(new Student()
            {
                CourseId = dto.CourseId,
                StudentId = Guid.NewGuid(),
                UserId = newUser.UserId,
                YearOfStudy = 1
            });

            //Add student membership details
            _entities.Memberships.Add(new Membership()
            {
                UserId = newUser.UserId,
                RoleId = 2 //Student role id numbers
            });

            //Save this data
            await _entities.SaveChangesAsync();
        }

        public async Task<string> GetStudentName(string username)
        {
            //Get the student login details using the username, then get the student details and return their name.
            var login = await _entities.Logins.FirstAsync(l => l.Username == username);
            var student = await _entities.Students.FirstAsync(s => s.UserId == login.UserId);
            return string.Format("{0} {1}", student.User.FirstName, student.User.LastName);
        }
    }
}

﻿using ModuleChooser.Resources.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ModuleChooser.Repositories
{
    //Interface declerations for repository methods defined here
    public interface IReportsRepository
    {
        Task<List<NumberOfTimesModulePreferredReportDataDto>> NumberOfTimesModulePreferredReportData();

        List<PreferredTogetherModulesChartDataDto> PercentageOfStudentsWhoPreferCertainModulesTogetherChartData(string moduleCode);

        List<PreferredTogetherModulesChartDataDto> PercentageOfTimesModulePreferredIfSelectedModuleHasAlsoBeenPreferredChartData(string moduleCode);

        List<MostPopularSetsOfPreferredModulesDto> MostPopularSetsOfPreferredModules();

        List<PreferredTogetherModulesChartDataDto> PercentageOfTimesModulePreferredIfSelectedModuleHasAlsoBeenPreferred(string moduleCode);
    }
}
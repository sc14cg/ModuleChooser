﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ModuleChooser.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ModuleChooser.Repositories.Tests
{
    [TestClass]
    public class AccountRepositoryTests
    {
        //Repository class to be used by all test methods
        public IAccountRepository _accountRepository = new AccountRepository(new Context());

        [TestMethod]
        public async Task GetRolesForUser_Returns_Roles_If_UserHasRoles()
        {           
            //Assign username to check
            var username = "demo";

            //Get the roles for that user
            var result = await _accountRepository.GetRolesForUser(username);
            
            //Check that the correct role is returned 
            Assert.AreEqual("Admin", result.First());

            //Ensure the return type is a list of strings
            Assert.IsInstanceOfType(result, typeof(List<string>));

            //Assign username to check
            username = "callumgoodridge1";

            //Get the roles for that user
            result = await _accountRepository.GetRolesForUser(username);

            //Check that the correct role is returned 
            Assert.AreEqual("Student", result.First());
        }

        [TestMethod]
        public async Task CheckLoginDetails_Returns_True_If_DetailsCorrect()
        {
            //Correct Details
            var username = "callumgoodridge1";
            var password = "Password1";

            //Login using the above details
            var result = await _accountRepository.CheckLoginDetails(username, password);

            //Check that the login was successful
            Assert.AreEqual(true, result);
        }

        [TestMethod]
        public async Task CheckLoginDetails_Returns_False_If_DetailsCorrect()
        {
            //Incorrect Details
            var username = "callumgoodridge1";
            var password = "Password12345678";

            //Try loging in using the above details
            var result = await _accountRepository.CheckLoginDetails(username, password);

            //Check that the login was unsuccessful
            Assert.AreEqual(false, result);
        }
    }
}

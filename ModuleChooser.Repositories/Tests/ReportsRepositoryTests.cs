﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ModuleChooser.Entities;
using ModuleChooser.Resources.Dtos;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace ModuleChooser.Repositories.Tests
{
    [TestClass]
    public class ReportsRepositoryTests
    {
        //Initialise the test context, and repository 
        public Context _entities;
        public IReportsRepository _reportsRepository = new ReportsRepository(new Context());

        [TestMethod]
        public async Task NumberOfTimesModulePreferredReportData()
        {
            //Get the NumberOfTimesModulePreferredReportData
            var result = await _reportsRepository.NumberOfTimesModulePreferredReportData();

            //Assert that the correct type is returned
            Assert.IsInstanceOfType(result, typeof(List<NumberOfTimesModulePreferredReportDataDto>));
        }

        [TestMethod]
        public void PercentageOfStudentsWhoPreferCertainModulesTogetherChartData()
        {
            //Get the PercentageOfStudentsWhoPreferCertainModulesTogetherChartData
            var moduleCode = "COMP1121";
            var result = _reportsRepository.PercentageOfStudentsWhoPreferCertainModulesTogetherChartData(moduleCode);

            //Assert that the correct type is returned
            Assert.IsInstanceOfType(result, typeof(List<PreferredTogetherModulesChartDataDto>));
        }

        [TestMethod]
        public void PercentageOfTimesModulePreferredIfSelectedModuleHasAlsoBeenPreferredChartData()
        {
            //Get the PercentageOfTimesModulePreferredIfSelectedModuleHasAlsoBeenPreferredChartData
            var moduleCode = "COMP1121";
            var result = _reportsRepository.PercentageOfTimesModulePreferredIfSelectedModuleHasAlsoBeenPreferredChartData(moduleCode);

            //Assert that the correct type is returned
            Assert.IsInstanceOfType(result, typeof(List<PreferredTogetherModulesChartDataDto>));
        }

        [TestMethod]
        public void MostPopularSetsOfPreferredModules_Returns_MostPopularSetsOfPreferredModulesDtos()
        {
            //Get the MostPopularSetsOfPreferredModules
            var result = _reportsRepository.MostPopularSetsOfPreferredModules();

            //Assert that the correct type is returned
            Assert.IsInstanceOfType(result, typeof(List<MostPopularSetsOfPreferredModulesDto>));
        }

        [TestMethod]
        public void PercentageOfTimesModulePreferredIfSelectedModuleHasAlsoBeenPreferred()
        {
            //Get the PercentageOfTimesModulePreferredIfSelectedModuleHasAlsoBeenPreferred data
            var moduleCode = "COMP1121";
            var result = _reportsRepository.PercentageOfTimesModulePreferredIfSelectedModuleHasAlsoBeenPreferred(moduleCode);

            //Assert that the correct type is returned
            Assert.IsInstanceOfType(result, typeof(List<PreferredTogetherModulesChartDataDto>));
        }
    }
}

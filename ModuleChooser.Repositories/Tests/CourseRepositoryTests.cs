﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ModuleChooser.Entities;
using ModuleChooser.Resources;
using ModuleChooser.Resources.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ModuleChooser.Repositories.Tests
{
    [TestClass]
    public class CourseRepositoryTests
    {
        //Initialise the test context, repository and test data
        public Context _entities = new Context();
        public ICourseRepository _courseRepository = new CourseRepository(new Context());
        public Guid moduleId = Guid.Parse("dbd23c4b-0b02-e811-b1b9-bca8a6ba1926");
        public Guid courseId = Guid.Parse("443208f1-638d-4f5f-b74b-e39ba3582d05");

        [TestMethod]
        public async Task GetCourses_Returns_CourseList()
        {
            //Get courses
            var result = await _courseRepository.GetCourses();

            //Get the number of courses which aren't tests
            var numOfCourse = _entities.Courses.Where(c => !c.Title.Contains("Test")).Count();

            //Check that a list of courses is returned
            Assert.AreEqual(numOfCourse, result.Count());
            Assert.IsInstanceOfType(result, typeof(List<CourseCompactDto>));
        }

        [TestMethod]
        public async Task SaveModuleToCourse_Saves_ModuleToCourse()
        { 
            //Find Test Course. Count number of modules.
            var course = await _entities.Courses.FindAsync(courseId);
            var courseModules = _entities.CourseModules.Where(c => c.CourseId == courseId);
            var courseModulesCountBefore = courseModules.Count();

            //Act
            await _courseRepository.SaveModuleToCourse(moduleId, courseId, true);

            courseModules = _entities.CourseModules.Where(c => c.CourseId == courseId);
            var courseModulesCountAfter = courseModules.Count();

            //Assert - Less Modules now as there were before
            Assert.IsTrue(courseModulesCountBefore < courseModulesCountAfter);

            //Remove any course modules added
            courseModules = _entities.CourseModules.Where(c => c.CourseId == courseId);

            foreach (var courseModule in courseModules){
                _entities.CourseModules.Remove(courseModule);
            }

            //save changes
            _entities.SaveChanges();
        }

        [TestMethod]
        public async Task RemoveModuleFromCourse_Removes_ModuleFromCourse()
        {
            //Find Test Course. Add new module. Count number of modules.
            var course = await _entities.Courses.FindAsync(courseId);

            _entities.CourseModules.Add(new CourseModule()
            {
                CourseId = courseId,
                ModuleId = moduleId,
                Compulsory = true
            });

            //save changes
            _entities.SaveChanges();

            var courseModules = _entities.CourseModules.Where(c => c.CourseId == courseId);
            var courseModulesCountBefore = courseModules.Count();

            //Act - Remove Module from course
            await _courseRepository.RemoveModuleFromCourse(moduleId, courseId);

            courseModules = _entities.CourseModules.Where(c => c.CourseId == courseId);
            var courseModulesCountAfter = courseModules.Count();        

            //Assert - More modules at the start than there are now
            Assert.IsTrue(courseModulesCountBefore > courseModulesCountAfter);
        }

        [TestMethod]
        public async Task GetModulesForCourse_Returns_ModulesForCourse(Guid courseId)
        {
            //Act - Get modules for course 
            var result = await _courseRepository.GetModulesForCourse(courseId);
            
            //Assert - Result is a list of course module dtos.
            Assert.IsInstanceOfType(result, typeof(List<CourseModuleDto>));
        }

        [TestMethod]
        public async Task SaveNewCourse_SavesCourse_If_TitleDoesNotExist()
        {
            //Title of course to be added
            var title = "TestCourse" + Guid.NewGuid().ToString();

            //Count of courses before saving new course
            var coursesCountBefore = _entities.Courses.Count();

            //Act - Save new course
            var result = await _courseRepository.SaveNewCourse(new SaveCourseDto()
            {
                NumberOfYears = 2,
                Title = title
            });

            var coursesCountAfter = _entities.Courses.Count();

            //Assert - Result is true meaning save was a success. Also assert that more courses are present afterwards.
            Assert.IsTrue(result);
            Assert.IsTrue(coursesCountBefore < coursesCountAfter);

            //Revert addition of course
            var course = _entities.Courses.First(c => c.Title == title);
            _entities.Courses.Remove(course);

            //Save changes
            _entities.SaveChanges();
        }

        [TestMethod]
        public async Task SaveNewCourse_DoesNotSaveCourse_If_TitleAlreadyExist()
        {
            //Get number of courses
            var coursesCountBefore = _entities.Courses.Count();

            //Add a new course
            var result = await _courseRepository.SaveNewCourse(new SaveCourseDto()
            {
                NumberOfYears = 2,
                Title = "TestCourse"
            });

            //Get the new number of courses
            var coursesCountAfter = _entities.Courses.Count();

            //Check that the course is not added
            Assert.IsFalse(result);
            Assert.IsTrue(coursesCountBefore == coursesCountAfter);
        }

        [TestMethod]
        public void GetCourseForStudentUser_Returns_ListOfCompactDtos()
        {
            //Act
            var result = _courseRepository.GetCourseForStudentUser("student");

            //Assert
            Assert.IsInstanceOfType(result, typeof(CourseCompactDto));
        }
    }
}

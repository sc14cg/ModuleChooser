﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ModuleChooser.Entities;
using ModuleChooser.Resources;
using ModuleChooser.Resources.Dtos;
using ModuleChooser.Resources.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ModuleChooser.Repositories.Tests
{
    [TestClass]
    public class ModuleRepositoryTests
    {
        //Initialise the test context, and repository
        public Context _entities = new Context();
        public IModuleRepository _moduleRepository = new ModuleRepository(new Context());

        [TestMethod]
        public async Task GetModules_Returns_ListOfAllModules()
        {
            //Count number of modules.
            var numberOfModules = _entities.Modules.Count();

            //Act - Get modules.
            var result = await _moduleRepository.GetModules();

            //Get the number of modules from the result.
            var numberOfModulesFromResult = result.Count();

            //Assert that the modules are returned
            Assert.IsInstanceOfType(result, typeof(List<ModuleInformationDto>));
            Assert.IsTrue(numberOfModules == numberOfModulesFromResult);
        }

        [TestMethod]
        public async Task AddNewModule_Returns_Success_IfNewModule()
        {
            //Check if module already there
            var module = _entities.Modules.FirstOrDefault(m => m.Title == "TestTitle");

            //If module there, remove it for the purpose of the test
            if (module != null)
            {
                _entities.Modules.Remove(module);
                await _entities.SaveChangesAsync();
            }

            //Act - Add new module
            var result = await _moduleRepository.AddNewModule(new SaveModuleDto()
            {
                Credits = 20,
                ModuleCode = "TestModule",
                Semester = 1,
                Title = "TestTitle",
                Url = "www.google.com",
                YearOfStudy = 3
            });

            //Assert that the module saved successfully
            Assert.AreEqual(SaveModuleResponseEnum.Success, result);

            //Remove test module 
            var moduleToRemove = _entities.Modules.First(m => m.Title == "TestTitle");
            _entities.Modules.Remove(moduleToRemove);
            await _entities.SaveChangesAsync();
        }

        [TestMethod]
        public async Task AddNewModule_Returns_TitleExists_IfModuleWithSameTitleExistsAlready()
        {
            //Act - Add new module
            var result = await _moduleRepository.AddNewModule(new SaveModuleDto()
            {
                Credits = 20,
                ModuleCode = "TestModule",
                Semester = 1,
                Title = "Programming For The Web",
                Url = "www.google.com",
                YearOfStudy = 3
            });

            //Assert that TitleExists response is given
            Assert.AreEqual(SaveModuleResponseEnum.TitleExists, result);
        }

        [TestMethod]
        public async Task AddNewModule_Returns_CodeExists_IfModuleWithSameCodeExistsAlready()
        {
            //Act - Add new module
            var result = await _moduleRepository.AddNewModule(new SaveModuleDto()
            {
                Credits = 20,
                ModuleCode = "COMP1011",
                Semester = 1,
                Title = "Test",
                Url = "www.google.com",
                YearOfStudy = 3
            });

            //Assert that CodeExists response is given
            Assert.AreEqual(SaveModuleResponseEnum.CodeExists, result);
        }

        [TestMethod]
        public async Task GetModuleEditInformation_Returns_CorrctModuleInformationDto()
        {
            var title = "TestTitle";

            //Remove test module if it exists
            var moduleToRemove = _entities.Modules.FirstOrDefault(m => m.Title == title);
            if (moduleToRemove != null)
            {
                _entities.Modules.Remove(moduleToRemove);
                await _entities.SaveChangesAsync();
            }

            //Attempt to add new module
            await _moduleRepository.AddNewModule(new SaveModuleDto()
            {
                Credits = 20,
                ModuleCode = "TestModule",
                Semester = 1,
                Title = title,
                Url = "www.google.com",
                YearOfStudy = 3
            });

            //Get module information
            var module = _entities.Modules.First(m => m.Title == title);

            //Create new module edit information
            var moduleEditInformationToCompare = new ModuleEditInformationDto()
            {
                ModuleId = module.ModuleId,
                Credits = module.Credits,
                ModuleCode = module.ModuleCode,
                Title = module.Title,
                Url = module.Url,
                Semester = module.Semester,
                YearOfStudy = module.YearOfStudy
            };

            //Get the module edit information
            var result = _moduleRepository.GetModuleEditInformation(module.ModuleId);

            //Test That all values that come back are the correct ones
            Assert.AreEqual(moduleEditInformationToCompare.Credits, result.Credits);
            Assert.AreEqual(moduleEditInformationToCompare.ModuleCode, result.ModuleCode);
            Assert.AreEqual(moduleEditInformationToCompare.ModuleId, result.ModuleId);
            Assert.AreEqual(moduleEditInformationToCompare.Semester, result.Semester);
            Assert.AreEqual(moduleEditInformationToCompare.Title, result.Title);
            Assert.AreEqual(moduleEditInformationToCompare.Url, result.Url);
            Assert.AreEqual(moduleEditInformationToCompare.YearOfStudy, result.YearOfStudy);

            //Remove test module 
            _entities.Modules.Remove(module);
            await _entities.SaveChangesAsync();
        }


        [TestMethod]
        public async Task SaveExistingModule_Returns_Success_IfEditSuccessful()
        {
            //Remove module if already exists
            var moduleToRemove = _entities.Modules.FirstOrDefault(m => m.Title == "TestTitle");

            if (moduleToRemove != null)
            {
                _entities.Modules.Remove(moduleToRemove);
                _entities.SaveChanges();
            }

            //Act - Add new module
            var moduleAdded = _entities.Modules.Add(new Module()
            {
                Credits = 20,
                ModuleCode = "TestModule123",
                Semester = 1,
                Title = "TestTitle",
                Url = "www.google.com",
                YearOfStudy = 3
            });

            _entities.SaveChanges();

            //Act - Add new module
            var result = await _moduleRepository.SaveExistingModule(new SaveModuleDto()
            {
                ModuleId = moduleAdded.ModuleId,
                Credits = 40,
                ModuleCode = "TestModule123",
                Semester = 1,
                Title = "TestTitle",
                Url = "www.google.com",
                YearOfStudy = 3
            });

            //Get the module from the database
            _entities = new Context();
            var module = _entities.Modules.First(m => m.Title == "TestTitle");

            //Check that edit was successful. Also check that credits are now 40 instead of 20.
            Assert.AreEqual(SaveModuleResponseEnum.Success, result);
            Assert.IsTrue(module.Credits == 40);

            //Remove test module 
            _entities.Modules.Remove(module);
            _entities.SaveChanges();
        }

        [TestMethod]
        public async Task SaveExistingModule_Returns_TitleExists_IfModuleWithSameTitleExistsAlready()
        {
            //Act - Add new module
            var result = await _moduleRepository.SaveExistingModule(new SaveModuleDto()
            {
                ModuleId = Guid.NewGuid(),
                Credits = 20,
                ModuleCode = "TestModule",
                Semester = 1,
                Title = "Programming For The Web",
                Url = "www.google.com",
                YearOfStudy = 3
            });

            //Assert that TitleExists response is given
            Assert.AreEqual(SaveModuleResponseEnum.TitleExists, result);
        }

        [TestMethod]
        public async Task SaveExistingModule_Returns_CodeExists_IfModuleWithSameCodeExistsAlready()
        {
            //Act - Add new module
            var result = await _moduleRepository.SaveExistingModule(new SaveModuleDto()
            {
                ModuleId = Guid.NewGuid(),
                Credits = 20,
                ModuleCode = "COMP1011",
                Semester = 1,
                Title = "Test",
                Url = "www.google.com",
                YearOfStudy = 3
            });

            //Assert that CodeExists response is given
            Assert.AreEqual(SaveModuleResponseEnum.CodeExists, result);
        }

        [TestMethod]
        public async Task GetCompactModules_Returns_ListOfCompactDtos()
        {
            //Act - Get compact modules
            var result = await _moduleRepository.GetCompactModules();

            //Get current count of modules in database
            var moduleCount = _entities.Modules.Count();

            //Assert that the correct result type is returned and correct number of modules
            Assert.AreEqual(moduleCount, result.Count());
            Assert.IsInstanceOfType(result, typeof(List<ModuleCompactDto>));
        }

        [TestMethod]
        public async Task SavePrerequisite_Returns_Success_If_New()
        {
            //Initialise variables
            var moduleId = Guid.Parse("e6d23c4b-0b02-e811-b1b9-bca8a6ba1926");
            var prerequisiteId = Guid.Parse("dbd23c4b-0b02-e811-b1b9-bca8a6ba1926");

            //Remove link if its already there.
            var moduleLinkToRemove = _entities.ModuleLinks.FirstOrDefault(m => m.ModuleId == moduleId && m.PrerequisiteId == prerequisiteId);
            if (moduleLinkToRemove != null)
            {
                _entities.ModuleLinks.Remove(moduleLinkToRemove);
                await _entities.SaveChangesAsync();
            }

            //Save the prerequisite
            var result = await _moduleRepository.SavePrerequisite(moduleId, prerequisiteId);

            //Assert that the prerequisite saves successfully
            Assert.IsInstanceOfType(result, typeof(SavePrerequisiteResponseEnum));
            Assert.AreEqual(SavePrerequisiteResponseEnum.Success, result);

            //Remove link afterwards
            _entities = new Context();
            var moduleLink = _entities.ModuleLinks.First(m => m.ModuleId == moduleId && m.PrerequisiteId == prerequisiteId);
            _entities.ModuleLinks.Remove(moduleLink);
            await _entities.SaveChangesAsync();
        }

        [TestMethod]
        public async Task SavePrerequisite_Returns_AlreadyExists_If_LinkAlreadyExists()
        {
            //Introduction To Web Technologies - Year 1 Semester 2
            var moduleId = Guid.Parse("dcd23c4b-0b02-e811-b1b9-bca8a6ba1926");
            //Programming For The Web - Year 1 Semester 1
            var prerequisiteId = Guid.Parse("dbd23c4b-0b02-e811-b1b9-bca8a6ba1926");

            //Attempt to save item
            var result = await _moduleRepository.SavePrerequisite(moduleId, prerequisiteId);

            //Assert that the prerequisite does not get saved
            Assert.IsInstanceOfType(result, typeof(SavePrerequisiteResponseEnum));
            Assert.AreEqual(SavePrerequisiteResponseEnum.AlreadyExists, result);
        }

        [TestMethod]
        public async Task SavePrerequisite_Returns_FutureModuleUsed_If_FutureModuleUsedAsPrereq()
        {
            //Introduction To Web Technologies - Year 1
            var moduleId = Guid.Parse("dcd23c4b-0b02-e811-b1b9-bca8a6ba1926");
            //Image Analysis - Year 4
            var prerequisiteId = Guid.Parse("650e5161-cc31-e811-b1be-bca8a6ba1926");

            //Attempt to save item
            var result = await _moduleRepository.SavePrerequisite(moduleId, prerequisiteId);

            //Assert that the prerequisite does not get saved
            Assert.IsInstanceOfType(result, typeof(SavePrerequisiteResponseEnum));
            Assert.AreEqual(SavePrerequisiteResponseEnum.FutureModuleUsed, result);
        }

        [TestMethod]
        public async Task GetModulesForUser_Returns_ListOfStudentModuleInformationDtos()
        {
            //Get the modules for the username noted
            var username = "callumgoodridge1";
            var result = await _moduleRepository.GetModulesForUser(username);

            //Check that the list of student modules information is returned
            Assert.IsInstanceOfType(result, typeof(List<StudentModuleInformationDto>));
        }

        [TestMethod]
        public async Task GetModulesForCourse_Returns_ListOfStudentModuleInformationDtos()
        {
            //Get the modules for the course
            var courseId = Guid.Parse("a0a0efb4-6c36-4b14-b150-abe2a073dc34");
            var result = await _moduleRepository.GetModulesForCourse(courseId);

            //Check that the list of student modules information is returned
            Assert.IsInstanceOfType(result, typeof(List<StudentModuleInformationDto>));
        }

        [TestMethod]
        public async Task GetPrerequisites_Returns_ListOfPrerequisiteListDtos()
        {
            //Get the prerequisites and return the result as a list.
            var result = await _moduleRepository.GetPrerequisites();

            //Get the current numberOfRequisites
            var numberOfRequisites = _entities.ModuleLinks.Count();

            //Check that the list of prerequisite list dtos is returned
            Assert.AreEqual(numberOfRequisites, result.Count());
            Assert.IsInstanceOfType(result, typeof(List<PrerequisiteListDto>));
        }

        [TestMethod]
        public void GetPrerequisitesForModule_Returns_CorrectPrerequisites()
        {
            //Introduction to Web Technologies
            var moduleIdParam = Guid.Parse("dcd23c4b-0b02-e811-b1b9-bca8a6ba1926");

            //Act - Get module prerequisites
            var result = _moduleRepository.GetPrerequisitesForModule(moduleIdParam);

            //Assert that the correct prerequisites are in the list
            Assert.IsInstanceOfType(result, typeof(List<PrerequisitesForModuleDto>));
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("Programming For The Web", result.First().PrerequisiteTitle);
        }

        [TestMethod]
        public void GetFutureAvailableOptionsForModule_Returns_CorrectFutureOptions()
        {
            //Social and Mobile Web Developments
            var moduleIdParam = Guid.Parse("e7d23c4b-0b02-e811-b1b9-bca8a6ba1926");

            //Act - Get future module options
            var result = _moduleRepository.GetFutureAvailableOptionsForModule(moduleIdParam);

            //Assert that the correct future options are available
            Assert.IsInstanceOfType(result, typeof(List<FutureAvailableOptionsFromModuleDto>));
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("Web Services & Web Data", result.First().FutureAvailableTitle);
        }

        [TestMethod]
        public async Task GetModulePreferencesForStudentUser_Returns_Preferences()
        {
            //Act - Get students preferences
            var result = await _moduleRepository.GetModulePreferencesForStudentUser("student");

            //Assert - that the students preferences are correct
            Assert.IsInstanceOfType(result, typeof(List<Guid>));
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual(Guid.Parse("e6d23c4b-0b02-e811-b1b9-bca8a6ba1926"), result.First());
        }

        [TestMethod]
        public async Task SaveStudentPreferredModuleOption_Returns_Success_If_SavedSuccessfully()
        {
            //Web Application Development.
            var moduleId = Guid.Parse("e7d23c4b-0b02-e811-b1b9-bca8a6ba1926");
            var username = "student";

            //Remove if its an option already
            await _moduleRepository.RemoveStudentPreferredModuleOption(moduleId, username);

            //Act - Saved preferred option
            var result = await _moduleRepository.SaveStudentPreferredModuleOption(moduleId, username);

            //Assert - The option saved successfully
            Assert.IsInstanceOfType(result, typeof(SaveStudentPreferredModuleOptionResponse));
            Assert.AreEqual(SaveStudentPreferredModuleOptionResponse.Success, result);
        }

        [TestMethod]
        public async Task SaveStudentPreferredModuleOption_Returns_PreferenceAlreadyExists_If_AlreadyExists()
        {
            //Web Application Development.
            var moduleId = Guid.Parse("e7d23c4b-0b02-e811-b1b9-bca8a6ba1926");
            var username = "student";

            //Save preference.
            await _moduleRepository.SaveStudentPreferredModuleOption(moduleId, username);

            //Act - Try and save preference again
            var result = await _moduleRepository.SaveStudentPreferredModuleOption(moduleId, username);

            //Assert - The option did not save successfully
            Assert.IsInstanceOfType(result, typeof(SaveStudentPreferredModuleOptionResponse));
            Assert.AreEqual(SaveStudentPreferredModuleOptionResponse.PreferenceAlreadyExists, result);
        }

        [TestMethod]
        public async Task RemoveStudentPreferredModuleOption()
        {
            //Web Application Development.
            var moduleId = Guid.Parse("e7d23c4b-0b02-e811-b1b9-bca8a6ba1926");

            //Save module to make sure it is an option.
            await _moduleRepository.SaveStudentPreferredModuleOption(moduleId, "student");

            //Count how many preferences before
            var smpBefore = _entities.StudentModulePreferences
                .Where(smp => smp.Student.User.LastName == "TestUser");

            var countBefore = smpBefore.Count();

            //Act - Remove preference for student
            await _moduleRepository.RemoveStudentPreferredModuleOption(moduleId, "student");

            _entities = new Context();

            //Count how many preferences after
            var smpAfter = _entities.StudentModulePreferences
                .Where(smp => smp.Student.User.LastName == "TestUser");

            var countAfter = smpAfter.Count();

            //Check that a preference was removed
            Assert.IsTrue(countAfter == countBefore - 1);
        }
    }
}

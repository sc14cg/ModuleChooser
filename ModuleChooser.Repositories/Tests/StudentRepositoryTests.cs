﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ModuleChooser.Entities;
using ModuleChooser.Resources;
using ModuleChooser.Resources.Dtos;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;

namespace ModuleChooser.Repositories.Tests
{
    [TestClass]
    public class StudentRepositoryTests
    {
        //Initialise the test context, repository and test data
        public Context _entities = new Context();
        public IStudentRepository _studentRepository = new StudentRepository(new Context());

        [TestMethod]
        public async Task GetStudents_Returns_ListOfStudentListInformationDtos()
        {
            //Get the students
            var result = await _studentRepository.GetStudents();

            //Get the current student count
            var studentCount = await _entities.Students.CountAsync();

            //Assert that the correct number and type of student information returned
            Assert.AreEqual(studentCount, result.Count);
            Assert.IsInstanceOfType(result, typeof(List<StudentListInformationDto>));
        }

        [TestMethod]
        public async Task GetStudentEditInformation_Returns_CorrectStudentEditInformationDto()
        {
            //Student to compare
            var studentId = Guid.Parse("89eb038b-2d0f-e811-b1ba-bca8a6ba1926");

            //Dto used to compare details
            var studentInfoToCompare = new StudentEditInformationDto()
            {
                CourseId = Guid.Empty,
                FirstName = "Callum",
                Surname = "TestUser",
                StudentId = studentId,
                YearOfStudy = 1,
            };

            //Act - Get information
            var result = await _studentRepository.GetStudentEditInformation(studentId);

            //Assert that result is of same type as expected and that all values come back as expected.
            Assert.IsInstanceOfType(result, typeof(StudentEditInformationDto));
            Assert.AreEqual(studentInfoToCompare.CourseId, result.CourseId);
            Assert.AreEqual(studentInfoToCompare.FirstName, result.FirstName);
            Assert.AreEqual(studentInfoToCompare.StudentId, result.StudentId);
            Assert.AreEqual(studentInfoToCompare.Surname, result.Surname);
            Assert.AreEqual(studentInfoToCompare.YearOfStudy, result.YearOfStudy);
        }

        [TestMethod]
        public async Task SaveExistingStudent_ChangesAndSavesInformation()
        {
            var studentId = Guid.Parse("89eb038b-2d0f-e811-b1ba-bca8a6ba1926");

            //Make sure the students year is equal to 1;
            var studentBefore = await _entities.Students.FirstAsync(s => s.StudentId == studentId);
            if (studentBefore.YearOfStudy != 1)
            {
                studentBefore.YearOfStudy = 1;
                await _entities.SaveChangesAsync();
            }

            //Details to save
            var saveStudentDto = new SaveStudentDto()
            {
                CourseId = Guid.Empty,
                FirstName = "Callum",
                Surname = "TestUser",
                StudentId = studentId,
                YearOfStudy = 2,
            };

            //Act - Save existing student details
            await _studentRepository.SaveExistingStudent(saveStudentDto);

            //Year = 2;
            _entities = new Context();
            var studentAfter = await _entities.Students.FirstAsync(s => s.StudentId == studentId);

            //Assert - Year of study changed
            Assert.AreEqual(1, studentBefore.YearOfStudy);
            Assert.AreEqual(2, studentAfter.YearOfStudy);

            //Change back to 1
            studentAfter.YearOfStudy = 1;
            await _entities.SaveChangesAsync();
        }

        [TestMethod]
        public async Task GetStudentName_Returns_StudentsName()
        {
            //Act - Get the students name using the username.
            var result = await _studentRepository.GetStudentName("student");
            
            //Expected Result
            var expected = "Callum TestUser";

            //Assert
            Assert.AreEqual(expected, result);
        }
    }
}

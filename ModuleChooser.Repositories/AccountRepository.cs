﻿using ModuleChooser.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Data.Entity;

namespace ModuleChooser.Repositories
{
    public class AccountRepository : IAccountRepository
    {
        //Initialise the context to get data
        private readonly Context _entities;

        //Entities injected to repository and value is assigned to the variable
        public AccountRepository(Context entities)
        {
            _entities = entities;
        }

        public async Task<List<string>> GetRolesForUser(string username)
        {
            //Using the username, get the login information.
            var login = await _entities.Logins.FindAsync(username);

            //Usinf the login information, check the memberships and get the role information and return this
            return await _entities.Memberships.Where(r => r.UserId == login.UserId)
                .Select(m => m.Role.Name)
                .ToListAsync();
        }

        public async Task<bool> CheckLoginDetails(string username, string password)
        {
            //Check if there are any logins which match the details provided
            return await _entities.Logins
                .AnyAsync(l => l.Username == username && l.Password == password);
        }
    }
}

﻿using ModuleChooser.Resources;
using ModuleChooser.Resources.Dtos;
using ModuleChooser.Resources.Enums;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ModuleChooser.Repositories
{
    //Interface declerations for repository methods defined here
    public interface IModuleRepository
    {
        Task<List<ModuleInformationDto>> GetModules();

        Task<SaveModuleResponseEnum> AddNewModule(SaveModuleDto dto);

        ModuleEditInformationDto GetModuleEditInformation(Guid moduleId);

        Task<SaveModuleResponseEnum> SaveExistingModule(SaveModuleDto dto);

        Task<List<ModuleCompactDto>> GetCompactModules();

        Task<SavePrerequisiteResponseEnum> SavePrerequisite(Guid moduleId, Guid prerequisiteId);

        Task<List<StudentModuleInformationDto>> GetModulesForUser(string username);

        Task<List<PrerequisiteListDto>> GetPrerequisites();

        List<PrerequisitesForModuleDto> GetPrerequisitesForModule(Guid moduleId);

        List<FutureAvailableOptionsFromModuleDto> GetFutureAvailableOptionsForModule(Guid moduleId);

        Task<List<Guid>> GetModulePreferencesForStudentUser(string username);

        Task<SaveStudentPreferredModuleOptionResponse> SaveStudentPreferredModuleOption(Guid moduleId, string username);

        Task RemoveStudentPreferredModuleOption(Guid moduleId, string username);

        Task<List<StudentModuleInformationDto>> GetModulesForCourse(Guid courseId);
    }
}
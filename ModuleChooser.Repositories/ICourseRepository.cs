﻿using ModuleChooser.Resources;
using ModuleChooser.Resources.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ModuleChooser.Repositories
{
    //Interface declerations for repository methods defined here
    public interface ICourseRepository
    {
        Task<List<CourseCompactDto>> GetCourses();

        Task SaveModuleToCourse(Guid moduleId, Guid courseId, bool compulsory);

        Task RemoveModuleFromCourse(Guid moduleId, Guid courseId);

        Task<List<CourseModuleDto>> GetModulesForCourse(Guid courseId);

        Task ChangeCompulsoryStatus(Guid moduleId, Guid courseId);

        Task<bool> SaveNewCourse(SaveCourseDto dto);

        CourseCompactDto GetCourseForStudentUser(string username);
    }
}
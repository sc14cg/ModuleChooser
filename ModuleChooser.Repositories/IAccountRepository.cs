﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace ModuleChooser.Repositories
{
    //Interface declerations for repository methods defined here
    public interface IAccountRepository
    {
        Task<List<string>> GetRolesForUser(string username);

        Task<bool> CheckLoginDetails(string username, string password);
    }
}
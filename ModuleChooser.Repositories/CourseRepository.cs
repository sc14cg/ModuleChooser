﻿using ModuleChooser.Entities;
using ModuleChooser.Resources;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Data.Entity;
using System;
using ModuleChooser.Resources.Dtos;

namespace ModuleChooser.Repositories
{
    public class CourseRepository : ICourseRepository
    {
        //Initialise the context to get data
        private readonly Context _entities;

        //Entities injected to repository and value is assigned to the variable
        public CourseRepository(Context entities)
        {
            _entities = entities;
        }

        public async Task<List<CourseCompactDto>> GetCourses()
        {
            //Get courses, making sure to map to CourseCompactDto and remove any test data.
            return await _entities.Courses.Where(c => !c.Title.Contains("Test")).Select(c => new CourseCompactDto
            {
                Id = c.Id,
                Title = c.Title
            })
            .ToListAsync();
        }

        public async Task SaveModuleToCourse(Guid moduleId, Guid courseId, bool compulsory)
        {
            //Using the details passed through the method, add a new CourseModule to the database
            _entities.CourseModules.Add(new CourseModule
            {
                CourseId = courseId,
                ModuleId = moduleId,
                Compulsory = compulsory
            });

            await _entities.SaveChangesAsync();
        }

        public async Task RemoveModuleFromCourse(Guid moduleId, Guid courseId)
        {
            //Get the course module
            var courseModule = await _entities.CourseModules.FirstAsync(cm => cm.ModuleId == moduleId && cm.CourseId == courseId);

            //Remove module from course and save this change
            _entities.CourseModules.Remove(courseModule);
            await _entities.SaveChangesAsync();
        }

        public async Task ChangeCompulsoryStatus(Guid moduleId, Guid courseId)
        {
            //Get the course module
            var courseModule = await _entities.CourseModules.FirstAsync(cm => cm.ModuleId == moduleId && cm.CourseId == courseId);

            //Change the compulsory value from true to false or from false to true - save this
            courseModule.Compulsory = !courseModule.Compulsory;
            await _entities.SaveChangesAsync();
        }

        public async Task<List<CourseModuleDto>> GetModulesForCourse(Guid courseId)
        {
            //Get the course modules
            var courseModules = await _entities.CourseModules.Where(cm => cm.CourseId == courseId).ToListAsync();

            //Map the course module details to the dto class
            var dtos = courseModules.Select(cm => new CourseModuleDto()
            {
                Compulsory = cm.Compulsory,
                CourseId = cm.CourseId,
                ModuleId = cm.ModuleId
            })
            .ToList();

            //return the list
            return dtos;
        }

        public async Task<bool> SaveNewCourse(SaveCourseDto dto)
        {
            //Get courses
            var courses = _entities.Courses;

            //Check if the name is already in the database
            var nameExists = courses.Any(c => c.Title == dto.Title);

            //If the course already exists then return false
            if (nameExists)
            {
                return false;
            }

            //Add a new course and save this to the database
            courses.Add(new Course()
            {
                NumberOfYears = dto.NumberOfYears,
                Title = dto.Title,
                Id = Guid.NewGuid()
            });

            await _entities.SaveChangesAsync();

            //return true if save successful
            return true;
        }

        public CourseCompactDto GetCourseForStudentUser(string username)
        {
            //Get students login information
            var login = _entities.Logins.Find(username);

            //Get students information
            var student =  _entities.Students
                .FirstOrDefault(s => s.UserId == login.User.UserId);

            //Get the course details
            var courseDto = new CourseCompactDto()
            {
                Id = student.Course.Id,
                Title = student.Course.Title
            };

            //Return the students course
            return courseDto;
        }
    }
}
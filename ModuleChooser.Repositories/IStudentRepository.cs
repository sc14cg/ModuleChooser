﻿using ModuleChooser.Resources;
using ModuleChooser.Resources.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ModuleChooser.Repositories
{
    //Interface declerations for repository methods defined here
    public interface IStudentRepository
    {
        Task<List<StudentListInformationDto>> GetStudents();

        Task<StudentEditInformationDto> GetStudentEditInformation(Guid studentId);

        Task SaveExistingStudent(SaveStudentDto dto);

        Task SaveNewStudent(SaveStudentDto dto);

        Task<string> GetStudentName(string username);
    }
}
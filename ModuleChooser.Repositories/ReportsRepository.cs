﻿using ModuleChooser.Entities;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System;
using ModuleChooser.Resources.Dtos;

namespace ModuleChooser.Repositories
{
    public class ReportsRepository : IReportsRepository
    {
        //Initialise the context to get data
        private readonly Context _entities;

        //Entities injected to repository and value is assigned to the variable
        public ReportsRepository(Context entities)
        {
            _entities = entities;
        }

        public async Task<List<NumberOfTimesModulePreferredReportDataDto>> NumberOfTimesModulePreferredReportData()
        {
            //Get the modules
            var modules = _entities.Modules;

            //Get all of the preferences 
            var allRecords = _entities.StudentModulePreferences;
            var resultList = new List<NumberOfTimesModulePreferredReportDataDto>();

            foreach (var module in modules)
            {
                //Get the number of times a module has been preferred
                var numberOfTimesPreferred = await _entities.StudentModulePreferences.CountAsync(smp => smp.ModuleId == module.ModuleId);

                //If the module has been prefered at least once then add this to the result list along with the module code and times preferred
                if (numberOfTimesPreferred > 0)
                {
                    resultList.Add(new NumberOfTimesModulePreferredReportDataDto()
                    {
                        ModuleCode = module.ModuleCode,
                        NumberOfTimesPreferred = numberOfTimesPreferred
                    });
                }
            }

            //Return list of modules and times preferred
            return resultList;
        }

        public List<PreferredTogetherModulesChartDataDto> PercentageOfStudentsWhoPreferCertainModulesTogetherChartData(string moduleCode)
        {
            //Get all preference entries for the selected module
            var modulePreferredEntries = _entities.StudentModulePreferences.Where(smp => smp.Module.ModuleCode == moduleCode);

            //Modules which have been preferred along with the selected module
            var preferredTogetherModules = new List<StudentModulePreference>();

            //Initialise result list
            var resultList = new List<PreferredTogetherModulesChartDataDto>();


            foreach (var modulePreferredEntry in modulePreferredEntries)
            {
                //Using the entries, find if any other modules selected with same semester and year for that student
                var preferredTogetherModulesForStudent = _entities.StudentModulePreferences
                    .Where(smp => (smp.Module.Semester == modulePreferredEntry.Module.Semester || smp.Module.Semester == 3) &&
                    smp.Module.YearOfStudy == modulePreferredEntry.Module.YearOfStudy &&
                    smp.StudentId == modulePreferredEntry.StudentId &&
                    smp.ModuleId != modulePreferredEntry.ModuleId);

                foreach (var preferredModule in preferredTogetherModulesForStudent)
                {
                    preferredTogetherModules.Add(preferredModule);
                }
            }

            var distinctList = preferredTogetherModules
                .GroupBy(ptm => ptm.Module.ModuleCode)
                .Select(group => group.First());

            foreach (var preferredTogetherModule in distinctList)
            {
                //Check how many times the module has been preferred in the database
                double numberOfStudents = _entities.Students.Count();
                double preferredTogetherCount = preferredTogetherModules.Count(ptm => ptm.ModuleId == preferredTogetherModule.ModuleId);
                resultList.Add(new PreferredTogetherModulesChartDataDto()
                {
                    ModuleCode = preferredTogetherModule.Module.ModuleCode,
                    PercentageOfTimesChosenTogether = Math.Round((preferredTogetherCount / numberOfStudents) * 100)
                });
            }

            return resultList;
        }

        public List<PreferredTogetherModulesChartDataDto> PercentageOfTimesModulePreferredIfSelectedModuleHasAlsoBeenPreferredChartData(string moduleCode)
        {
            //Get all preference entries for the selected module
            var modulePreferredEntries = _entities.StudentModulePreferences.Where(smp => smp.Module.ModuleCode == moduleCode);

            //Modules which have been preferred along with the selected module
            var preferredTogetherModules = new List<StudentModulePreference>();

            //Initialise result list
            var resultList = new List<PreferredTogetherModulesChartDataDto>();

            foreach (var modulePreferredEntry in modulePreferredEntries)
            {
                //Using the entries, find if any other modules selected with same semester and year for that student
                var preferredTogetherModulesForStudent = _entities.StudentModulePreferences
                    .Where(smp => (smp.Module.Semester == modulePreferredEntry.Module.Semester || smp.Module.Semester == 3) &&
                    smp.Module.YearOfStudy == modulePreferredEntry.Module.YearOfStudy &&
                    smp.StudentId == modulePreferredEntry.StudentId &&
                    smp.ModuleId != modulePreferredEntry.ModuleId);

                foreach (var preferredModule in preferredTogetherModulesForStudent)
                {
                    preferredTogetherModules.Add(preferredModule);
                }
            }

            var distinctList = preferredTogetherModules
                .GroupBy(ptm => ptm.Module.ModuleCode)
                .Select(group => group.First());
            foreach (var preferredTogetherModule in distinctList)
            {
                //Check how many times the module has been preferred in the database
                double amountOfTimeModuleHasBeenPreferred = _entities.StudentModulePreferences.Count(smp => smp.Module.ModuleCode == moduleCode);
                double preferredTogetherCount = preferredTogetherModules.Count(ptm => ptm.ModuleId == preferredTogetherModule.ModuleId);
                resultList.Add(new PreferredTogetherModulesChartDataDto()
                {
                    ModuleCode = preferredTogetherModule.Module.ModuleCode,
                    PercentageOfTimesChosenTogether = Math.Round((preferredTogetherCount / amountOfTimeModuleHasBeenPreferred) * 100)
                });
            }

            return resultList.OrderByDescending(r => r.PercentageOfTimesChosenTogether).ToList();
        }

        public List<PreferredTogetherModulesChartDataDto> PercentageOfTimesModulePreferredIfSelectedModuleHasAlsoBeenPreferred(string moduleCode)
        {
            //Get all preference entries for the selected module
            var modulePreferredEntries = _entities.StudentModulePreferences.Where(smp => smp.Module.ModuleCode == moduleCode);

            //Modules which have been preferred along with the selected module
            var preferredTogetherModules = new List<StudentModulePreference>();

            //Initialise result list
            var resultList = new List<PreferredTogetherModulesChartDataDto>();

            foreach (var modulePreferredEntry in modulePreferredEntries)
            {
                //Using the entries, find if any other modules selected with same semester and year for that student
                var preferredTogetherModulesForStudent = _entities.StudentModulePreferences
                    .Where(smp => smp.StudentId == modulePreferredEntry.StudentId && smp.ModuleId != modulePreferredEntry.ModuleId);

                foreach (var preferredModule in preferredTogetherModulesForStudent)
                {
                    preferredTogetherModules.Add(preferredModule);
                }
            }

            var distinctList = preferredTogetherModules
                .GroupBy(ptm => ptm.Module.ModuleCode)
                .Select(group => group.First());

            foreach (var preferredTogetherModule in distinctList)
            {
                //Check how many times the module has been preferred in the database
                double amountOfTimeModuleHasBeenPreferred = _entities.StudentModulePreferences.Count(smp => smp.Module.ModuleCode == moduleCode);
                double preferredTogetherCount = preferredTogetherModules.Count(ptm => ptm.ModuleId == preferredTogetherModule.ModuleId);
                resultList.Add(new PreferredTogetherModulesChartDataDto()
                {
                    ModuleCode = preferredTogetherModule.Module.ModuleCode,
                    PercentageOfTimesChosenTogether = Math.Round((preferredTogetherCount / amountOfTimeModuleHasBeenPreferred) * 100)
                });
            }

            return resultList.OrderByDescending(r => r.PercentageOfTimesChosenTogether).ToList();
        }

        public List<MostPopularSetsOfPreferredModulesDto> MostPopularSetsOfPreferredModules()
        {
            //Get all the modules from the database
            var allModules = _entities.Modules;

            //Initialise result list
            var setsOfPreferredModules = new List<MostPopularSetsOfPreferredModulesDto>();
            var setOfModules = new List<KeyValuePair<string, string>>();

            foreach (var module in allModules)
            {
                //Get all preference entries for the selected module
                var modulePreferredEntries = _entities.StudentModulePreferences.Where(smp => smp.Module.ModuleCode == module.ModuleCode);

                //Modules which have been preferred along with the selected module
                var preferredTogetherModules = new List<StudentModulePreference>();


                foreach (var modulePreferredEntry in modulePreferredEntries)
                {
                    //Using the entries, find if any other modules selected with same semester and year for that student
                    var preferredTogetherModulesForStudent = _entities.StudentModulePreferences
                        .Where(smp => (smp.Module.Semester == modulePreferredEntry.Module.Semester || smp.Module.Semester == 3) &&
                        smp.Module.YearOfStudy == modulePreferredEntry.Module.YearOfStudy &&
                        smp.StudentId == modulePreferredEntry.StudentId &&
                        smp.ModuleId != modulePreferredEntry.ModuleId);

                    foreach (var preferredModule in preferredTogetherModulesForStudent)
                    {
                        preferredTogetherModules.Add(preferredModule);
                    }
                }

                var distinctList = preferredTogetherModules
                    .GroupBy(ptm => ptm.Module.ModuleCode)
                    .Select(group => group.First());

                foreach (var preferredTogetherModule in distinctList)
                {
                    //Check how many times the module has been preferred in the database
                    double numberOfStudents = _entities.Students.Count();
                    double preferredTogetherCount = preferredTogetherModules.Count(ptm => ptm.ModuleId == preferredTogetherModule.ModuleId);

                    //Check if combination has already been added to list
                    if (!setOfModules.Contains(new KeyValuePair<string, string>(preferredTogetherModule.Module.ModuleCode, module.ModuleCode)) &&
                        !setOfModules.Contains(new KeyValuePair<string, string>(module.ModuleCode, preferredTogetherModule.Module.ModuleCode)))
                    {
                        //Add Set (in both directions) to the list.
                        setOfModules.Add(new KeyValuePair<string, string>(preferredTogetherModule.Module.ModuleCode, module.ModuleCode));
                        setOfModules.Add(new KeyValuePair<string, string>(module.ModuleCode, preferredTogetherModule.Module.ModuleCode));

                        //Add the preferred module set to the final results
                        setsOfPreferredModules.Add(new MostPopularSetsOfPreferredModulesDto()
                        {
                            ModuleSet = string.Format("{0} - {1}", preferredTogetherModule.Module.ModuleCode, module.ModuleCode),
                            PercentageOfStudentsWhoPreferModuleSet = Math.Round((preferredTogetherCount / numberOfStudents) * 100)
                        });
                    }
                }
            }

            return setsOfPreferredModules.OrderByDescending(spm => spm.PercentageOfStudentsWhoPreferModuleSet).Take(10).ToList();
        }
    }
}

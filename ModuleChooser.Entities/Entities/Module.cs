﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ModuleChooser.Entities
{
    public class Module
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ModuleId { get; set; }

        [Required, Index(IsUnique = true), StringLength(20)]
        public String ModuleCode { get; set; }

        [Required, Index(IsUnique = true), StringLength(100)]
        public String Title { get; set; }

        [Required]
        public int Semester { get; set; }

        [Required]
        public int Credits { get; set; }

        [Required]
        public int YearOfStudy { get; set; }

        public string Url { get; set; }
    }
}
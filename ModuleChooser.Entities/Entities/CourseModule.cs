﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace ModuleChooser.Entities
{
    public class CourseModule
    {
        [ForeignKey("CourseId")]
        public virtual Course Course { get; set; }

        [Required]
        [Key, Column(Order = 0)]
        public Guid CourseId { get; set; }

        [ForeignKey("ModuleId")]
        public virtual Module Module { get; set; }

        [Required]
        [Key, Column(Order = 1)]
        public Guid ModuleId { get; set; }

        public bool Compulsory { get; set; }
    }
}

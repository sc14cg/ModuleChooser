﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ModuleChooser.Entities
{
    public class Login
    {
        [Key]
        public string Username { get; set; }

        [Required]
        public string Password { get; set; }

        [ForeignKey("UserId")]
        public virtual User User { get; set; }

        [Required, Index(IsUnique = true)]
        public Guid UserId { get; set; }
    }
}

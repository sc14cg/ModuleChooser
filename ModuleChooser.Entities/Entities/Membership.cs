﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ModuleChooser.Entities
{
    public class Membership
    {
        [ForeignKey("UserId")]
        public virtual User User { get; set; }

        [Required]
        [Key, Column(Order = 0)]
        public Guid UserId { get; set; }
        
        [ForeignKey("RoleId")]
        public virtual Role Role { get; set; }

        [Required]
        [Key, Column(Order = 1)]
        public int RoleId { get; set; }
    }
}

﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ModuleChooser.Entities
{
    public class StudentModulePreference
    {
        [ForeignKey("ModuleId")]
        public virtual Module Module { get; set; }

        [Key, Column(Order = 0)]
        public Guid ModuleId { get; set; }
        
        [ForeignKey("StudentId")]
        public virtual Student Student { get; set; }

        [Key, Column(Order = 1)]
        public Guid StudentId { get; set; }
    }
}

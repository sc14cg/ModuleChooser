﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ModuleChooser.Entities
{
    public class Course
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; set; }

        [Required, Index(IsUnique = true), StringLength(100)]
        public string Title { get; set; }

        [Required]
        public int NumberOfYears { get; set; }
    }
}

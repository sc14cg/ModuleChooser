﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ModuleChooser.Entities
{
    public class ModuleLink
    {
        [Required]
        [ForeignKey("ModuleId")]
        public virtual Module Module { get; set; }

        [Key, Column(Order = 0)]
        public Guid ModuleId { get; set; }

        [Required]
        [ForeignKey("PrerequisiteId")]
        public virtual Module PrerequisiteModule { get; set; }

        [Key, Column(Order = 1)]
        public Guid PrerequisiteId { get; set; }
    }
}

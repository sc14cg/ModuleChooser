namespace ModuleChooser.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update_Module_and_Courses_Many_to_Many : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ModuleCourses",
                c => new
                    {
                        Module_ModuleId = c.Guid(nullable: false),
                        Course_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.Module_ModuleId, t.Course_Id })
                .ForeignKey("dbo.Modules", t => t.Module_ModuleId, cascadeDelete: true)
                .ForeignKey("dbo.Courses", t => t.Course_Id, cascadeDelete: true)
                .Index(t => t.Module_ModuleId)
                .Index(t => t.Course_Id);
            
            DropColumn("dbo.Modules", "AppliedCSCore");
            DropColumn("dbo.Modules", "CSCore");
            DropColumn("dbo.Modules", "AppliedCSOption");
            DropColumn("dbo.Modules", "CSOption");
            DropColumn("dbo.Modules", "GeneralOption");
            DropColumn("dbo.Modules", "CSWithAI");
            DropColumn("dbo.Modules", "GraphicsAndEngineering");
            DropColumn("dbo.Modules", "Core");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Modules", "Core", c => c.Boolean(nullable: false));
            AddColumn("dbo.Modules", "GraphicsAndEngineering", c => c.Boolean(nullable: false));
            AddColumn("dbo.Modules", "CSWithAI", c => c.Boolean(nullable: false));
            AddColumn("dbo.Modules", "GeneralOption", c => c.Boolean(nullable: false));
            AddColumn("dbo.Modules", "CSOption", c => c.Boolean(nullable: false));
            AddColumn("dbo.Modules", "AppliedCSOption", c => c.Boolean(nullable: false));
            AddColumn("dbo.Modules", "CSCore", c => c.Boolean(nullable: false));
            AddColumn("dbo.Modules", "AppliedCSCore", c => c.Boolean(nullable: false));
            DropForeignKey("dbo.ModuleCourses", "Course_Id", "dbo.Courses");
            DropForeignKey("dbo.ModuleCourses", "Module_ModuleId", "dbo.Modules");
            DropIndex("dbo.ModuleCourses", new[] { "Course_Id" });
            DropIndex("dbo.ModuleCourses", new[] { "Module_ModuleId" });
            DropTable("dbo.ModuleCourses");
        }
    }
}

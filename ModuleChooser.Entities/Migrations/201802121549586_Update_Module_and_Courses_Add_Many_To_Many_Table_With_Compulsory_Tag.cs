namespace ModuleChooser.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update_Module_and_Courses_Add_Many_To_Many_Table_With_Compulsory_Tag : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ModuleCourses", "Module_ModuleId", "dbo.Modules");
            DropForeignKey("dbo.ModuleCourses", "Course_Id", "dbo.Courses");
            DropIndex("dbo.ModuleCourses", new[] { "Module_ModuleId" });
            DropIndex("dbo.ModuleCourses", new[] { "Course_Id" });
            CreateTable(
                "dbo.CourseModules",
                c => new
                    {
                        CourseId = c.Guid(nullable: false),
                        ModuleId = c.Guid(nullable: false),
                        Compulsory = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => new { t.CourseId, t.ModuleId })
                .ForeignKey("dbo.Courses", t => t.CourseId, cascadeDelete: true)
                .ForeignKey("dbo.Modules", t => t.ModuleId, cascadeDelete: true)
                .Index(t => t.CourseId)
                .Index(t => t.ModuleId);
            
            AddColumn("dbo.Courses", "NumberOfYears", c => c.Int(nullable: false));
            DropTable("dbo.ModuleCourses");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.ModuleCourses",
                c => new
                    {
                        Module_ModuleId = c.Guid(nullable: false),
                        Course_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.Module_ModuleId, t.Course_Id });
            
            DropForeignKey("dbo.CourseModules", "ModuleId", "dbo.Modules");
            DropForeignKey("dbo.CourseModules", "CourseId", "dbo.Courses");
            DropIndex("dbo.CourseModules", new[] { "ModuleId" });
            DropIndex("dbo.CourseModules", new[] { "CourseId" });
            DropColumn("dbo.Courses", "NumberOfYears");
            DropTable("dbo.CourseModules");
            CreateIndex("dbo.ModuleCourses", "Course_Id");
            CreateIndex("dbo.ModuleCourses", "Module_ModuleId");
            AddForeignKey("dbo.ModuleCourses", "Course_Id", "dbo.Courses", "Id", cascadeDelete: true);
            AddForeignKey("dbo.ModuleCourses", "Module_ModuleId", "dbo.Modules", "ModuleId", cascadeDelete: true);
        }
    }
}

namespace ModuleChooser.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_Role : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Roles",
                c => new
                    {
                        RoleId = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.RoleId);
            CreateIndex("dbo.Roles", "Name", unique: true);
        }
        
        public override void Down()
        {
            DropIndex("dbo.Roles", new[] { "Name" });
            DropTable("dbo.Roles");
        }
    }
}

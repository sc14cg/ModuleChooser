namespace ModuleChooser.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Module_RemoveCoreOption : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Modules", "Core");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Modules", "Core", c => c.Boolean(nullable: false));
        }
    }
}

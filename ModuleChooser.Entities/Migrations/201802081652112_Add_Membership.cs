namespace ModuleChooser.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_Membership : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Memberships",
                c => new
                {
                    UserId = c.Guid(nullable: false),
                    RoleId = c.Int(nullable: false),
                })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.Roles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
        }

        public override void Down()
        {
            DropIndex("dbo.Memberships", new[] { "RoleId" });
            DropIndex("dbo.Memberships", new[] { "UserId" });
            DropForeignKey("dbo.Memberships", "UserId", "dbo.Users");
            DropForeignKey("dbo.Memberships", "RoleId", "dbo.Roles");
            DropTable("dbo.Memberships");
        }
    }
}

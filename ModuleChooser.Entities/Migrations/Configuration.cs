namespace ModuleChooser.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<Context>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Context context)
        {
            //context.Courses.AddOrUpdate(new Course
            //{
            //    Id = Guid.Parse("a0a0efb4-6c36-4b14-b150-abe2a073dc34"),
            //    Title = "Computer Science"
            //});

            //#region ADD MODULES
            //#region Year 1
            //context.Modules.AddOrUpdate(new Module
            //{
            //    Title = "Programming For The Web",
            //    AppliedCSCore = false,
            //    AppliedCSOption = false,
            //    Credits = 10,
            //    CSCore = false,
            //    CSOption = false,
            //    CSWithAI = false,
            //    GeneralOption = true,
            //    GraphicsAndEngineering = false,
            //    Core = false,
            //    ModuleCode = "COMP1011",
            //    Semester = 1,
            //    YearOfStudy = 1,
            //    ModuleId = Guid.Parse("DBD23C4B-0B02-E811-B1B9-BCA8A6BA1926")
            //});
            
            //context.Modules.AddOrUpdate(new Module
            //{
            //    Title = "Introduction to Web Technologies",
            //    AppliedCSCore = false,
            //    AppliedCSOption = false,
            //    Credits = 10,
            //    CSCore = false,
            //    CSOption = false,
            //    CSWithAI = false,
            //    GeneralOption = true,
            //    GraphicsAndEngineering = false,
            //    Core = false,
            //    ModuleCode = "COMP1021",
            //    Semester = 2,
            //    YearOfStudy = 1,
            //    ModuleId = Guid.Parse("DCD23C4B-0B02-E811-B1B9-BCA8A6BA1926")
            //});
            //context.Modules.AddOrUpdate(new Module
            //{
            //    Title = "Databases",
            //    AppliedCSCore = false,
            //    AppliedCSOption = false,
            //    Credits = 10,
            //    CSCore = false,
            //    CSOption = false,
            //    CSWithAI = false,
            //    GeneralOption = false,
            //    GraphicsAndEngineering = false,
            //    Core = true,
            //    ModuleCode = "COMP1121",
            //    Semester = 2,
            //    YearOfStudy = 1,
            //    ModuleId = Guid.Parse("DDD23C4B-0B02-E811-B1B9-BCA8A6BA1926")
            //});
            //context.Modules.AddOrUpdate(new Module
            //{
            //    Title = "Computer Processors",
            //    AppliedCSCore = false,
            //    AppliedCSOption = false,
            //    Credits = 10,
            //    CSCore = false,
            //    CSOption = false,
            //    CSWithAI = false,
            //    GeneralOption = false,
            //    GraphicsAndEngineering = false,
            //    Core = true,
            //    ModuleCode = "COMP1212",
            //    Semester = 1,
            //    YearOfStudy = 1,
            //    ModuleId = Guid.Parse("DED23C4B-0B02-E811-B1B9-BCA8A6BA1926")
            //});
            //context.Modules.AddOrUpdate(new Module
            //{
            //    Title = "Computer Architecture",
            //    AppliedCSCore = false,
            //    AppliedCSOption = false,
            //    Credits = 10,
            //    CSCore = false,
            //    CSOption = false,
            //    CSWithAI = false,
            //    GeneralOption = false,
            //    GraphicsAndEngineering = false,
            //    Core = true,
            //    ModuleCode = "COMP1211",
            //    Semester = 2,
            //    YearOfStudy = 1,
            //    ModuleId = Guid.Parse("DFD23C4B-0B02-E811-B1B9-BCA8A6BA1926")
            //});
            //context.Modules.AddOrUpdate(new Module
            //{
            //    Title = "Fundamental Mathematical Concepts",
            //    AppliedCSCore = false,
            //    AppliedCSOption = false,
            //    Credits = 10,
            //    CSCore = false,
            //    CSOption = false,
            //    CSWithAI = false,
            //    GeneralOption = false,
            //    GraphicsAndEngineering = false,
            //    Core = true,
            //    ModuleCode = "COMP1421",
            //    Semester = 1,
            //    YearOfStudy = 1,
            //    ModuleId = Guid.Parse("E0D23C4B-0B02-E811-B1B9-BCA8A6BA1926")
            //});
            //context.Modules.AddOrUpdate(new Module
            //{
            //    Title = "Introduction to Discrete Mathematics",
            //    AppliedCSCore = false,
            //    AppliedCSOption = false,
            //    Credits = 10,
            //    CSCore = false,
            //    CSOption = false,
            //    CSWithAI = false,
            //    GeneralOption = false,
            //    GraphicsAndEngineering = false,
            //    Core = true,
            //    ModuleCode = "COMP1511",
            //    Semester = 2,
            //    YearOfStudy = 1,
            //    ModuleId = Guid.Parse("E1D23C4B-0B02-E811-B1B9-BCA8A6BA1926")
            //});
            //context.Modules.AddOrUpdate(new Module
            //{
            //    Title = "Procedural Programming",
            //    AppliedCSCore = false,
            //    AppliedCSOption = false,
            //    Credits = 10,
            //    CSCore = false,
            //    CSOption = false,
            //    CSWithAI = false,
            //    GeneralOption = false,
            //    GraphicsAndEngineering = false,
            //    Core = true,
            //    ModuleCode = "COMP1711",
            //    Semester = 1,
            //    YearOfStudy = 1,
            //    ModuleId = Guid.Parse("E2D23C4B-0B02-E811-B1B9-BCA8A6BA1926")
            //});
            //context.Modules.AddOrUpdate(new Module
            //{
            //    Title = "Object Oriented Programming",
            //    AppliedCSCore = false,
            //    AppliedCSOption = false,
            //    Credits = 10,
            //    CSCore = false,
            //    CSOption = false,
            //    CSWithAI = false,
            //    GeneralOption = false,
            //    GraphicsAndEngineering = false,
            //    Core = true,
            //    ModuleCode = "COMP1721",
            //    Semester = 2,
            //    YearOfStudy = 1,
            //    ModuleId = Guid.Parse("E3D23C4B-0B02-E811-B1B9-BCA8A6BA1926")
            //});
            //context.Modules.AddOrUpdate(new Module
            //{
            //    Title = "Professional Computing",
            //    AppliedCSCore = false,
            //    AppliedCSOption = false,
            //    Credits = 20,
            //    CSCore = false,
            //    CSOption = false,
            //    CSWithAI = false,
            //    GeneralOption = false,
            //    GraphicsAndEngineering = false,
            //    Core = true,
            //    ModuleCode = "COMP1911",
            //    Semester = 1,
            //    YearOfStudy = 1,
            //    ModuleId = Guid.Parse("E4D23C4B-0B02-E811-B1B9-BCA8A6BA1926")
            //});
            //context.Modules.AddOrUpdate(new Module
            //{
            //    Title = "Programming Project",
            //    AppliedCSCore = false,
            //    AppliedCSOption = false,
            //    Credits = 10,
            //    CSCore = false,
            //    CSOption = false,
            //    CSWithAI = false,
            //    GeneralOption = false,
            //    GraphicsAndEngineering = false,
            //    Core = true,
            //    ModuleCode = "COMP1921",
            //    Semester = 2,
            //    YearOfStudy = 1,
            //    ModuleId = Guid.Parse("E5D23C4B-0B02-E811-B1B9-BCA8A6BA1926")
            //});
            //#endregion
            //#region Year 2
            //context.Modules.AddOrUpdate(new Module
            //{
            //    Title = "Web Application Development",
            //    AppliedCSCore = false,
            //    AppliedCSOption = false,
            //    Credits = 10,
            //    CSCore = false,
            //    CSOption = false,
            //    CSWithAI = false,
            //    GeneralOption = true,
            //    GraphicsAndEngineering = false,
            //    Core = false,
            //    ModuleCode = "COMP2011",
            //    Semester = 1,
            //    YearOfStudy = 2,
            //    ModuleId = Guid.Parse("E6D23C4B-0B02-E811-B1B9-BCA8A6BA1926")
            //});
            //context.Modules.AddOrUpdate(new Module
            //{
            //    Title = "Social and Mobile Web Developments",
            //    AppliedCSCore = false,
            //    AppliedCSOption = false,
            //    Credits = 10,
            //    CSCore = false,
            //    CSOption = false,
            //    CSWithAI = false,
            //    GeneralOption = true,
            //    GraphicsAndEngineering = false,
            //    Core = false,
            //    ModuleCode = "COMP2021",
            //    Semester = 2,
            //    YearOfStudy = 2,
            //    ModuleId = Guid.Parse("E7D23C4B-0B02-E811-B1B9-BCA8A6BA1926")
            //});
            //context.Modules.AddOrUpdate(new Module
            //{
            //    Title = "Data Mining",
            //    AppliedCSCore = false,
            //    AppliedCSOption = false,
            //    Credits = 10,
            //    CSCore = false,
            //    CSOption = false,
            //    CSWithAI = false,
            //    GeneralOption = true,
            //    GraphicsAndEngineering = false,
            //    Core = false,
            //    ModuleCode = "COMP2121",
            //    Semester = 2,
            //    YearOfStudy = 2,
            //    ModuleId = Guid.Parse("E8D23C4B-0B02-E811-B1B9-BCA8A6BA1926")
            //});
            //context.Modules.AddOrUpdate(new Module
            //{
            //    Title = "Operating Systems",
            //    AppliedCSCore = false,
            //    AppliedCSOption = false,
            //    Credits = 10,
            //    CSCore = false,
            //    CSOption = false,
            //    CSWithAI = false,
            //    GeneralOption = false,
            //    GraphicsAndEngineering = false,
            //    Core = true,
            //    ModuleCode = "COMP2211",
            //    Semester = 1,
            //    YearOfStudy = 2,
            //    ModuleId = Guid.Parse("E9D23C4B-0B02-E811-B1B9-BCA8A6BA1926")
            //});
            //context.Modules.AddOrUpdate(new Module
            //{
            //    Title = "Networks",
            //    AppliedCSCore = false,
            //    AppliedCSOption = false,
            //    Credits = 10,
            //    CSCore = false,
            //    CSOption = false,
            //    CSWithAI = false,
            //    GeneralOption = false,
            //    GraphicsAndEngineering = false,
            //    Core = true,
            //    ModuleCode = "COMP2221",
            //    Semester = 2,
            //    YearOfStudy = 2,
            //    ModuleId = Guid.Parse("EAD23C4B-0B02-E811-B1B9-BCA8A6BA1926")
            //});
            //context.Modules.AddOrUpdate(new Module
            //{
            //    Title = "Formal Languages & Finite Automata",
            //    AppliedCSCore = false,
            //    AppliedCSOption = false,
            //    Credits = 10,
            //    CSCore = false,
            //    CSOption = false,
            //    CSWithAI = false,
            //    GeneralOption = false,
            //    GraphicsAndEngineering = false,
            //    Core = true,
            //    ModuleCode = "COMP2321",
            //    Semester = 2,
            //    YearOfStudy = 2,
            //    ModuleId = Guid.Parse("EBD23C4B-0B02-E811-B1B9-BCA8A6BA1926")
            //});
            //context.Modules.AddOrUpdate(new Module
            //{
            //    Title = "User Interfaces",
            //    AppliedCSCore = false,
            //    AppliedCSOption = false,
            //    Credits = 10,
            //    CSCore = false,
            //    CSOption = false,
            //    CSWithAI = false,
            //    GeneralOption = false,
            //    GraphicsAndEngineering = false,
            //    Core = true,
            //    ModuleCode = "COMP2811",
            //    Semester = 1,
            //    YearOfStudy = 2,
            //    ModuleId = Guid.Parse("ECD23C4B-0B02-E811-B1B9-BCA8A6BA1926")
            //});
            //context.Modules.AddOrUpdate(new Module
            //{
            //    Title = "Numerical Computation",
            //    AppliedCSCore = false,
            //    AppliedCSOption = true,
            //    Credits = 10,
            //    CSCore = true,
            //    CSOption = false,
            //    CSWithAI = false,
            //    GeneralOption = false,
            //    GraphicsAndEngineering = false,
            //    Core = false,
            //    ModuleCode = "COMP2421",
            //    Semester = 1,
            //    YearOfStudy = 2,
            //    ModuleId = Guid.Parse("EDD23C4B-0B02-E811-B1B9-BCA8A6BA1926")
            //});
            //context.Modules.AddOrUpdate(new Module
            //{
            //    Title = "Algorithms & Data Structures I",
            //    AppliedCSCore = false,
            //    AppliedCSOption = false,
            //    Credits = 10,
            //    CSCore = false,
            //    CSOption = false,
            //    CSWithAI = false,
            //    GeneralOption = false,
            //    GraphicsAndEngineering = false,
            //    Core = true,
            //    ModuleCode = "COMP2711",
            //    Semester = 1,
            //    YearOfStudy = 2,
            //    ModuleId = Guid.Parse("EED23C4B-0B02-E811-B1B9-BCA8A6BA1926")
            //});
            //context.Modules.AddOrUpdate(new Module
            //{
            //    Title = "Algorithms & Data Structures II",
            //    AppliedCSCore = false,
            //    AppliedCSOption = true,
            //    Credits = 10,
            //    CSCore = true,
            //    CSOption = false,
            //    CSWithAI = false,
            //    GeneralOption = false,
            //    GraphicsAndEngineering = false,
            //    Core = false,
            //    ModuleCode = "COMP2721",
            //    Semester = 2,
            //    YearOfStudy = 2,
            //    ModuleId = Guid.Parse("EFD23C4B-0B02-E811-B1B9-BCA8A6BA1926")
            //});
            //context.Modules.AddOrUpdate(new Module
            //{
            //    Title = "Artificial Intelligence",
            //    AppliedCSCore = false,
            //    AppliedCSOption = false,
            //    Credits = 10,
            //    CSCore = false,
            //    CSOption = false,
            //    CSWithAI = false,
            //    GeneralOption = false,
            //    GraphicsAndEngineering = false,
            //    Core = true,
            //    ModuleCode = "COMP2611",
            //    Semester = 2,
            //    YearOfStudy = 2,
            //    ModuleId = Guid.Parse("F0D23C4B-0B02-E811-B1B9-BCA8A6BA1926")
            //});
            //context.Modules.AddOrUpdate(new Module
            //{
            //    Title = "Software Engineering",
            //    AppliedCSCore = false,
            //    AppliedCSOption = false,
            //    Credits = 20,
            //    CSCore = false,
            //    CSOption = false,
            //    CSWithAI = false,
            //    GeneralOption = false,
            //    GraphicsAndEngineering = false,
            //    Core = true,
            //    ModuleCode = "COMP2931",
            //    Semester = 3,
            //    YearOfStudy = 2,
            //    ModuleId = Guid.Parse("F1D23C4B-0B02-E811-B1B9-BCA8A6BA1926")
            //});
            //context.Modules.AddOrUpdate(new Module
            //{
            //    Title = "Systems Thinking",
            //    AppliedCSCore = true,
            //    AppliedCSOption = false,
            //    Credits = 20,
            //    CSCore = false,
            //    CSOption = true,
            //    CSWithAI = false,
            //    GeneralOption = false,
            //    GraphicsAndEngineering = false,
            //    Core = false,
            //    ModuleCode = "COMP2921",
            //    Semester = 1,
            //    YearOfStudy = 2,
            //    ModuleId = Guid.Parse("F2D23C4B-0B02-E811-B1B9-BCA8A6BA1926")
            //});
            //context.Modules.AddOrUpdate(new Module
            //{
            //    Title = "Requirements Engineering",
            //    AppliedCSCore = true,
            //    AppliedCSOption = false,
            //    Credits = 20,
            //    CSCore = false,
            //    CSOption = true,
            //    CSWithAI = false,
            //    GeneralOption = false,
            //    GraphicsAndEngineering = false,
            //    Core = false,
            //    ModuleCode = "COMP2911",
            //    Semester = 2,
            //    YearOfStudy = 2,
            //    ModuleId = Guid.Parse("F3D23C4B-0B02-E811-B1B9-BCA8A6BA1926")
            //});
            //#endregion
            //#region Year 3
            //context.Modules.AddOrUpdate(new Module
            //{
            //    Title = "Web Services & Web Data",
            //    AppliedCSCore = false,
            //    AppliedCSOption = false,
            //    Credits = 10,
            //    CSCore = false,
            //    CSOption = false,
            //    CSWithAI = false,
            //    GeneralOption = true,
            //    GraphicsAndEngineering = false,
            //    Core = false,
            //    ModuleCode = "COMP3011",
            //    Semester = 1,
            //    YearOfStudy = 3,
            //    ModuleId = Guid.Parse("F4D23C4B-0B02-E811-B1B9-BCA8A6BA1926")
            //});
            //context.Modules.AddOrUpdate(new Module
            //{
            //    Title = "User Adaptive Intelligent Systems",
            //    AppliedCSCore = false,
            //    AppliedCSOption = false,
            //    Credits = 10,
            //    CSCore = false,
            //    CSOption = false,
            //    CSWithAI = false,
            //    GeneralOption = true,
            //    GraphicsAndEngineering = false,
            //    Core = false,
            //    ModuleCode = "COMP3771",
            //    Semester = 2,
            //    YearOfStudy = 3,
            //    ModuleId = Guid.Parse("F5D23C4B-0B02-E811-B1B9-BCA8A6BA1926")
            //});
            //context.Modules.AddOrUpdate(new Module
            //{
            //    Title = "Parallel Computation",
            //    AppliedCSCore = false,
            //    AppliedCSOption = false,
            //    Credits = 10,
            //    CSCore = false,
            //    CSOption = false,
            //    CSWithAI = false,
            //    GeneralOption = true,
            //    GraphicsAndEngineering = false,
            //    Core = false,
            //    ModuleCode = "COMP3221",
            //    Semester = 1,
            //    YearOfStudy = 3,
            //    ModuleId = Guid.Parse("F6D23C4B-0B02-E811-B1B9-BCA8A6BA1926")
            //});
            //context.Modules.AddOrUpdate(new Module
            //{
            //    Title = "Distributed Systems",
            //    AppliedCSCore = false,
            //    AppliedCSOption = false,
            //    Credits = 10,
            //    CSCore = false,
            //    CSOption = false,
            //    CSWithAI = false,
            //    GeneralOption = true,
            //    GraphicsAndEngineering = false,
            //    Core = false,
            //    ModuleCode = "COMP3211",
            //    Semester = 2,
            //    YearOfStudy = 3,
            //    ModuleId = Guid.Parse("F7D23C4B-0B02-E811-B1B9-BCA8A6BA1926")
            //});
            //context.Modules.AddOrUpdate(new Module
            //{
            //    Title = "Programming Languages & Compilation",
            //    AppliedCSCore = false,
            //    AppliedCSOption = false,
            //    Credits = 10,
            //    CSCore = false,
            //    CSOption = false,
            //    CSWithAI = false,
            //    GeneralOption = true,
            //    GraphicsAndEngineering = false,
            //    Core = false,
            //    ModuleCode = "COMP3321",
            //    Semester = 1,
            //    YearOfStudy = 3,
            //    ModuleId = Guid.Parse("F8D23C4B-0B02-E811-B1B9-BCA8A6BA1926")
            //});
            //context.Modules.AddOrUpdate(new Module
            //{
            //    Title = "Mobile Application Development",
            //    AppliedCSCore = false,
            //    AppliedCSOption = false,
            //    Credits = 10,
            //    CSCore = false,
            //    CSOption = false,
            //    CSWithAI = false,
            //    GeneralOption = true,
            //    GraphicsAndEngineering = false,
            //    Core = false,
            //    ModuleCode = "COMP3222",
            //    Semester = 1,
            //    YearOfStudy = 3,
            //    ModuleId = Guid.Parse("F9D23C4B-0B02-E811-B1B9-BCA8A6BA1926")
            //});
            //context.Modules.AddOrUpdate(new Module
            //{
            //    Title = "Computer Graphics",
            //    AppliedCSCore = false,
            //    AppliedCSOption = false,
            //    Credits = 10,
            //    CSCore = false,
            //    CSOption = false,
            //    CSWithAI = false,
            //    GeneralOption = false,
            //    GraphicsAndEngineering = true,
            //    Core = false,
            //    ModuleCode = "COMP3811",
            //    Semester = 1,
            //    YearOfStudy = 3,
            //    ModuleId = Guid.Parse("FAD23C4B-0B02-E811-B1B9-BCA8A6BA1926")
            //});
            //context.Modules.AddOrUpdate(new Module
            //{
            //    Title = "Information Visualisation",
            //    AppliedCSCore = false,
            //    AppliedCSOption = false,
            //    Credits = 10,
            //    CSCore = false,
            //    CSOption = false,
            //    CSWithAI = false,
            //    GeneralOption = true,
            //    GraphicsAndEngineering = false,
            //    Core = false,
            //    ModuleCode = "COMP3736",
            //    Semester = 2,
            //    YearOfStudy = 3,
            //    ModuleId = Guid.Parse("FBD23C4B-0B02-E811-B1B9-BCA8A6BA1926")
            //});
            //context.Modules.AddOrUpdate(new Module
            //{
            //    Title = "Graph Algorithms and Complexity Theory",
            //    AppliedCSCore = false,
            //    AppliedCSOption = false,
            //    Credits = 10,
            //    CSCore = false,
            //    CSOption = false,
            //    CSWithAI = false,
            //    GeneralOption = true,
            //    GraphicsAndEngineering = false,
            //    Core = false,
            //    ModuleCode = "COMP3940",
            //    Semester = 1,
            //    YearOfStudy = 3,
            //    ModuleId = Guid.Parse("FCD23C4B-0B02-E811-B1B9-BCA8A6BA1926")
            //});
            //context.Modules.AddOrUpdate(new Module
            //{
            //    Title = "Combinatorial Optimisation",
            //    AppliedCSCore = false,
            //    AppliedCSOption = false,
            //    Credits = 10,
            //    CSCore = false,
            //    CSOption = false,
            //    CSWithAI = false,
            //    GeneralOption = true,
            //    GraphicsAndEngineering = false,
            //    Core = false,
            //    ModuleCode = "COMP3910",
            //    Semester = 2,
            //    YearOfStudy = 3,
            //    ModuleId = Guid.Parse("FDD23C4B-0B02-E811-B1B9-BCA8A6BA1926")
            //});
            //context.Modules.AddOrUpdate(new Module
            //{
            //    Title = "Machine Learning",
            //    AppliedCSCore = false,
            //    AppliedCSOption = false,
            //    Credits = 10,
            //    CSCore = false,
            //    CSOption = false,
            //    CSWithAI = true,
            //    GeneralOption = false,
            //    GraphicsAndEngineering = false,
            //    Core = false,
            //    ModuleCode = "COMP3611",
            //    Semester = 1,
            //    YearOfStudy = 3,
            //    ModuleId = Guid.Parse("FED23C4B-0B02-E811-B1B9-BCA8A6BA1926")
            //});
            //context.Modules.AddOrUpdate(new Module
            //{
            //    Title = "Intelligent Systems & Robotics",
            //    AppliedCSCore = false,
            //    AppliedCSOption = false,
            //    Credits = 20,
            //    CSCore = false,
            //    CSOption = false,
            //    CSWithAI = true,
            //    GeneralOption = false,
            //    GraphicsAndEngineering = false,
            //    Core = false,
            //    ModuleCode = "COMP3631",
            //    Semester = 3,
            //    YearOfStudy = 3,
            //    ModuleId = Guid.Parse("FFD23C4B-0B02-E811-B1B9-BCA8A6BA1926")
            //});
            //context.Modules.AddOrUpdate(new Module
            //{
            //    Title = "Functional Programming",
            //    AppliedCSCore = false,
            //    AppliedCSOption = false,
            //    Credits = 10,
            //    CSCore = false,
            //    CSOption = false,
            //    CSWithAI = false,
            //    GeneralOption = true,
            //    GraphicsAndEngineering = false,
            //    Core = false,
            //    ModuleCode = "COMP3941",
            //    Semester = 1,
            //    YearOfStudy = 3,
            //    ModuleId = Guid.Parse("00D33C4B-0B02-E811-B1B9-BCA8A6BA1926")
            //});
            //context.Modules.AddOrUpdate(new Module
            //{
            //    Title = "Cryptography",
            //    AppliedCSCore = false,
            //    AppliedCSOption = false,
            //    Credits = 10,
            //    CSCore = false,
            //    CSOption = false,
            //    CSWithAI = false,
            //    GeneralOption = true,
            //    GraphicsAndEngineering = false,
            //    Core = false,
            //    ModuleCode = "COMP3223",
            //    Semester = 2,
            //    YearOfStudy = 3,
            //    ModuleId = Guid.Parse("01D33C4B-0B02-E811-B1B9-BCA8A6BA1926")
            //});
            //context.Modules.AddOrUpdate(new Module
            //{
            //    Title = "Secure Computing",
            //    AppliedCSCore = false,
            //    AppliedCSOption = false,
            //    Credits = 10,
            //    CSCore = false,
            //    CSOption = false,
            //    CSWithAI = false,
            //    GeneralOption = true,
            //    GraphicsAndEngineering = false,
            //    Core = false,
            //    ModuleCode = "COMP3911",
            //    Semester = 1,
            //    YearOfStudy = 3,
            //    ModuleId = Guid.Parse("02D33C4B-0B02-E811-B1B9-BCA8A6BA1926")
            //});
            //context.Modules.AddOrUpdate(new Module
            //{
            //    Title = "Individual Project",
            //    AppliedCSCore = false,
            //    AppliedCSOption = false,
            //    Credits = 40,
            //    CSCore = false,
            //    CSOption = false,
            //    CSWithAI = false,
            //    GeneralOption = false,
            //    GraphicsAndEngineering = false,
            //    Core = true,
            //    ModuleCode = "COMP3931",
            //    Semester = 3,
            //    YearOfStudy = 3,
            //    ModuleId = Guid.Parse("03D33C4B-0B02-E811-B1B9-BCA8A6BA1926")
            //});
            //#endregion
            //#region Year 4
            //context.Modules.AddOrUpdate(new Module
            //{
            //    Title = "Semantic Technologies and Applications",
            //    AppliedCSCore = false,
            //    AppliedCSOption = false,
            //    Credits = 15,
            //    CSCore = false,
            //    CSOption = false,
            //    CSWithAI = false,
            //    GeneralOption = true,
            //    GraphicsAndEngineering = false,
            //    Core = false,
            //    ModuleCode = "COMP5860M",
            //    Semester = 2,
            //    YearOfStudy = 4,
            //    ModuleId = Guid.Parse("04D33C4B-0B02-E811-B1B9-BCA8A6BA1926")
            //});
            //context.Modules.AddOrUpdate(new Module
            //{
            //    Title = "Data Science",
            //    AppliedCSCore = false,
            //    AppliedCSOption = false,
            //    Credits = 15,
            //    CSCore = false,
            //    CSOption = false,
            //    CSWithAI = false,
            //    GeneralOption = true,
            //    GraphicsAndEngineering = false,
            //    Core = false,
            //    ModuleCode = "COMP5122M",
            //    Semester = 1,
            //    YearOfStudy = 4,
            //    ModuleId = Guid.Parse("05D33C4B-0B02-E811-B1B9-BCA8A6BA1926")
            //});
            //context.Modules.AddOrUpdate(new Module
            //{
            //    Title = "Big Data Systems",
            //    AppliedCSCore = false,
            //    AppliedCSOption = false,
            //    Credits = 15,
            //    CSCore = false,
            //    CSOption = false,
            //    CSWithAI = false,
            //    GeneralOption = true,
            //    GraphicsAndEngineering = false,
            //    Core = false,
            //    ModuleCode = "COMP5111M",
            //    Semester = 2,
            //    YearOfStudy = 4,
            //    ModuleId = Guid.Parse("06D33C4B-0B02-E811-B1B9-BCA8A6BA1926")
            //});
            //context.Modules.AddOrUpdate(new Module
            //{
            //    Title = "Cloud Computing",
            //    AppliedCSCore = false,
            //    AppliedCSOption = false,
            //    Credits = 15,
            //    CSCore = false,
            //    CSOption = false,
            //    CSWithAI = false,
            //    GeneralOption = true,
            //    GraphicsAndEngineering = false,
            //    Core = false,
            //    ModuleCode = "COMP5850M",
            //    Semester = 2,
            //    YearOfStudy = 4,
            //    ModuleId = Guid.Parse("07D33C4B-0B02-E811-B1B9-BCA8A6BA1926")
            //});
            //context.Modules.AddOrUpdate(new Module
            //{
            //    Title = "Parallel and Concurrent Programming",
            //    AppliedCSCore = false,
            //    AppliedCSOption = false,
            //    Credits = 15,
            //    CSCore = false,
            //    CSOption = false,
            //    CSWithAI = false,
            //    GeneralOption = false,
            //    GraphicsAndEngineering = true,
            //    Core = false,
            //    ModuleCode = "COMP5811M",
            //    Semester = 1,
            //    YearOfStudy = 4,
            //    ModuleId = Guid.Parse("08D33C4B-0B02-E811-B1B9-BCA8A6BA1926")
            //});
            //context.Modules.AddOrUpdate(new Module
            //{
            //    Title = "High Performance Graphics",
            //    AppliedCSCore = false,
            //    AppliedCSOption = false,
            //    Credits = 15,
            //    CSCore = false,
            //    CSOption = false,
            //    CSWithAI = false,
            //    GeneralOption = false,
            //    GraphicsAndEngineering = true,
            //    Core = false,
            //    ModuleCode = "COMP5822M",
            //    Semester = 2,
            //    YearOfStudy = 4,
            //    ModuleId = Guid.Parse("09D33C4B-0B02-E811-B1B9-BCA8A6BA1926")
            //});
            //context.Modules.AddOrUpdate(new Module
            //{
            //    Title = "Games Engines and Workflow",
            //    AppliedCSCore = false,
            //    AppliedCSOption = false,
            //    Credits = 15,
            //    CSCore = false,
            //    CSOption = false,
            //    CSWithAI = false,
            //    GeneralOption = false,
            //    GraphicsAndEngineering = true,
            //    Core = false,
            //    ModuleCode = "COMP5813M",
            //    Semester = 1,
            //    YearOfStudy = 4,
            //    ModuleId = Guid.Parse("0AD33C4B-0B02-E811-B1B9-BCA8A6BA1926")
            //});
            //context.Modules.AddOrUpdate(new Module
            //{
            //    Title = "Geometric Processing",
            //    AppliedCSCore = false,
            //    AppliedCSOption = false,
            //    Credits = 15,
            //    CSCore = false,
            //    CSOption = false,
            //    CSWithAI = false,
            //    GeneralOption = false,
            //    GraphicsAndEngineering = true,
            //    Core = false,
            //    ModuleCode = "COMP5821M",
            //    Semester = 2,
            //    YearOfStudy = 4,
            //    ModuleId = Guid.Parse("0BD33C4B-0B02-E811-B1B9-BCA8A6BA1926")
            //});
            //context.Modules.AddOrUpdate(new Module
            //{
            //    Title = "Foundations of Modelling and Rendering",
            //    AppliedCSCore = false,
            //    AppliedCSOption = false,
            //    Credits = 15,
            //    CSCore = false,
            //    CSOption = false,
            //    CSWithAI = false,
            //    GeneralOption = false,
            //    GraphicsAndEngineering = true,
            //    Core = false,
            //    ModuleCode = "COMP5812M",
            //    Semester = 1,
            //    YearOfStudy = 4,
            //    ModuleId = Guid.Parse("0CD33C4B-0B02-E811-B1B9-BCA8A6BA1926")
            //});
            //context.Modules.AddOrUpdate(new Module
            //{
            //    Title = "Computer Animaton and Simulation",
            //    AppliedCSCore = false,
            //    AppliedCSOption = false,
            //    Credits = 15,
            //    CSCore = false,
            //    CSOption = false,
            //    CSWithAI = false,
            //    GeneralOption = false,
            //    GraphicsAndEngineering = true,
            //    Core = false,
            //    ModuleCode = "COMP5823M",
            //    Semester = 2,
            //    YearOfStudy = 4,
            //    ModuleId = Guid.Parse("0DD33C4B-0B02-E811-B1B9-BCA8A6BA1926")
            //});
            //context.Modules.AddOrUpdate(new Module
            //{
            //    Title = "Scientific Computation",
            //    AppliedCSCore = false,
            //    AppliedCSOption = false,
            //    Credits = 15,
            //    CSCore = false,
            //    CSOption = false,
            //    CSWithAI = false,
            //    GeneralOption = true,
            //    GraphicsAndEngineering = false,
            //    Core = false,
            //    ModuleCode = "COMP5930M",
            //    Semester = 1,
            //    YearOfStudy = 4,
            //    ModuleId = Guid.Parse("0ED33C4B-0B02-E811-B1B9-BCA8A6BA1926")
            //});
            //context.Modules.AddOrUpdate(new Module
            //{
            //    Title = "Scheduling",
            //    AppliedCSCore = false,
            //    AppliedCSOption = false,
            //    Credits = 15,
            //    CSCore = false,
            //    CSOption = false,
            //    CSWithAI = false,
            //    GeneralOption = true,
            //    GraphicsAndEngineering = false,
            //    Core = false,
            //    ModuleCode = "COMP5920M",
            //    Semester = 1,
            //    YearOfStudy = 4,
            //    ModuleId = Guid.Parse("0FD33C4B-0B02-E811-B1B9-BCA8A6BA1926")
            //});
            //context.Modules.AddOrUpdate(new Module
            //{
            //    Title = "Graph Theory: Structure and Algorithms",
            //    AppliedCSCore = false,
            //    AppliedCSOption = false,
            //    Credits = 15,
            //    CSCore = false,
            //    CSOption = false,
            //    CSWithAI = false,
            //    GeneralOption = true,
            //    GraphicsAndEngineering = false,
            //    Core = false,
            //    ModuleCode = "COMP5940M",
            //    Semester = 2,
            //    YearOfStudy = 4,
            //    ModuleId = Guid.Parse("10D33C4B-0B02-E811-B1B9-BCA8A6BA1926")
            //});
            //context.Modules.AddOrUpdate(new Module
            //{
            //    Title = "Bio-Computation",
            //    AppliedCSCore = false,
            //    AppliedCSOption = false,
            //    Credits = 15,
            //    CSCore = false,
            //    CSOption = false,
            //    CSWithAI = true,
            //    GeneralOption = false,
            //    GraphicsAndEngineering = false,
            //    Core = false,
            //    ModuleCode = "COMP5400M",
            //    Semester = 2,
            //    YearOfStudy = 4,
            //    ModuleId = Guid.Parse("11D33C4B-0B02-E811-B1B9-BCA8A6BA1926")
            //});
            //context.Modules.AddOrUpdate(new Module
            //{
            //    Title = "Knowledge Representation and Reasoning",
            //    AppliedCSCore = false,
            //    AppliedCSOption = false,
            //    Credits = 15,
            //    CSCore = false,
            //    CSOption = false,
            //    CSWithAI = true,
            //    GeneralOption = false,
            //    GraphicsAndEngineering = false,
            //    Core = false,
            //    ModuleCode = "COMP5450M",
            //    Semester = 1,
            //    YearOfStudy = 4,
            //    ModuleId = Guid.Parse("12D33C4B-0B02-E811-B1B9-BCA8A6BA1926")
            //});
            //context.Modules.AddOrUpdate(new Module
            //{
            //    Title = "Data Mining and Text Analytics",
            //    AppliedCSCore = false,
            //    AppliedCSOption = false,
            //    Credits = 15,
            //    CSCore = false,
            //    CSOption = false,
            //    CSWithAI = true,
            //    GeneralOption = false,
            //    GraphicsAndEngineering = false,
            //    Core = false,
            //    ModuleCode = "COMP5840M",
            //    Semester = 2,
            //    YearOfStudy = 4,
            //    ModuleId = Guid.Parse("13D33C4B-0B02-E811-B1B9-BCA8A6BA1926")
            //});
            //context.Modules.AddOrUpdate(new Module
            //{
            //    Title = "Advanced Software Engineering",
            //    AppliedCSCore = false,
            //    AppliedCSOption = false,
            //    Credits = 15,
            //    CSCore = false,
            //    CSOption = false,
            //    CSWithAI = false,
            //    GeneralOption = true,
            //    GraphicsAndEngineering = false,
            //    Core = false,
            //    ModuleCode = "COMP5911M",
            //    Semester = 1,
            //    YearOfStudy = 4,
            //    ModuleId = Guid.Parse("14D33C4B-0B02-E811-B1B9-BCA8A6BA1926")
            //});
            //context.Modules.AddOrUpdate(new Module
            //{
            //    Title = "Group Project",
            //    AppliedCSCore = false,
            //    AppliedCSOption = false,
            //    Credits = 30,
            //    CSCore = false,
            //    CSOption = false,
            //    CSWithAI = false,
            //    GeneralOption = false,
            //    GraphicsAndEngineering = false,
            //    Core = true,
            //    ModuleCode = "COMP5530M",
            //    Semester = 2,
            //    YearOfStudy = 4,
            //    ModuleId = Guid.Parse("15D33C4B-0B02-E811-B1B9-BCA8A6BA1926")
            //});
            //#endregion
            //#endregion

            //#region ADD ROLES
            //var admin = new Role
            //{
            //    Name = "Admin",
            //    RoleId = 1
            //};
            //var student = new Role
            //{
            //    Name = "Student",
            //    RoleId = 2
            //};

            //context.Roles.AddOrUpdate(admin);
            //context.Roles.AddOrUpdate(student);
            //#endregion

            //#region Add Users
            //#region Add Admin
            //context.Users.AddOrUpdate(new User
            //{
            //    FirstName = "Demo",
            //    LastName = "User",
            //    UserId = Guid.Parse("8e47053d-51c6-46e6-8f46-e2dc13e6d026")
            //});
            //context.Memberships.AddOrUpdate(new Membership
            //{
            //    RoleId = 1,
            //    UserId = Guid.Parse("8e47053d-51c6-46e6-8f46-e2dc13e6d026")
            //});
            //context.Memberships.AddOrUpdate(new Membership
            //{
            //    RoleId = 2,
            //    UserId = Guid.Parse("8e47053d-51c6-46e6-8f46-e2dc13e6d026")
            //});
            //context.Logins.AddOrUpdate(new Login
            //{
            //    UserId = Guid.Parse("8e47053d-51c6-46e6-8f46-e2dc13e6d026"),
            //    Username = "demo",
            //    Password = "demo"
            //});
            //#endregion
            
            //#region Add Student
            //context.Users.AddOrUpdate(new User
            //{
            //    FirstName = "Student",
            //    LastName = "User",
            //    UserId = Guid.Parse("49e7c6c8-2f2e-414f-b772-c9baa9ce47ea")
            //});
            //context.Memberships.AddOrUpdate(new Membership
            //{
            //    RoleId = 2,
            //    UserId = Guid.Parse("49e7c6c8-2f2e-414f-b772-c9baa9ce47ea")
            //});
            //context.Logins.AddOrUpdate(new Login
            //{
            //    UserId = Guid.Parse("49e7c6c8-2f2e-414f-b772-c9baa9ce47ea"),
            //    Username = "student",
            //    Password = "student"
            //});
            //context.Students.AddOrUpdate(new Student
            //{
            //    UserId = Guid.Parse("49e7c6c8-2f2e-414f-b772-c9baa9ce47ea"),
            //    YearOfStudy = 2,
            //    StudentId = Guid.Parse("e1a8c634-6d7d-4e56-8ddb-cf9fad6a48f2"),
            //    CourseId = Guid.Parse("a0a0efb4-6c36-4b14-b150-abe2a073dc34")
            //});

            //#endregion
            //#endregion
        }
    }
}

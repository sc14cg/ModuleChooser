namespace ModuleChooser.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Module_AddURL : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Modules", "Url", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Modules", "Url");
        }
    }
}

namespace ModuleChooser.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Edit_Course_Add_Unique_And_Identity : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.CourseModules", "CourseId", "dbo.Courses");
            DropForeignKey("dbo.Students", "CourseId", "dbo.Courses");
            DropPrimaryKey("dbo.Courses");
            AlterColumn("dbo.Courses", "Id", c => c.Guid(nullable: false, identity: true));
            AlterColumn("dbo.Courses", "Title", c => c.String(nullable: false, maxLength: 100));
            AddPrimaryKey("dbo.Courses", "Id");
            CreateIndex("dbo.Courses", "Title", unique: true);
            AddForeignKey("dbo.CourseModules", "CourseId", "dbo.Courses", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Students", "CourseId", "dbo.Courses", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Students", "CourseId", "dbo.Courses");
            DropForeignKey("dbo.CourseModules", "CourseId", "dbo.Courses");
            DropIndex("dbo.Courses", new[] { "Title" });
            DropPrimaryKey("dbo.Courses");
            AlterColumn("dbo.Courses", "Title", c => c.String(nullable: false));
            AlterColumn("dbo.Courses", "Id", c => c.Guid(nullable: false));
            AddPrimaryKey("dbo.Courses", "Id");
            AddForeignKey("dbo.Students", "CourseId", "dbo.Courses", "Id", cascadeDelete: true);
            AddForeignKey("dbo.CourseModules", "CourseId", "dbo.Courses", "Id", cascadeDelete: true);
        }
    }
}

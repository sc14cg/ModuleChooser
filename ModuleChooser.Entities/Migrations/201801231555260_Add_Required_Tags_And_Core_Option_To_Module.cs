namespace ModuleChooser.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_Required_Tags_And_Core_Option_To_Module : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Modules", "Core", c => c.Boolean(nullable: false));
            AlterColumn("dbo.Courses", "Title", c => c.String(nullable: false));
            AlterColumn("dbo.Modules", "Title", c => c.String(nullable: false));
            AlterColumn("dbo.Students", "FirstName", c => c.String(nullable: false));
            AlterColumn("dbo.Students", "LastName", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Students", "LastName", c => c.String());
            AlterColumn("dbo.Students", "FirstName", c => c.String());
            AlterColumn("dbo.Modules", "Title", c => c.String());
            AlterColumn("dbo.Courses", "Title", c => c.String());
            DropColumn("dbo.Modules", "Core");
        }
    }
}

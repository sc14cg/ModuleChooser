namespace ModuleChooser.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_StudentModulePreferences : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.StudentModulePreferences",
                c => new
                    {
                        ModuleId = c.Guid(nullable: false),
                        StudentId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.ModuleId, t.StudentId })
                .ForeignKey("dbo.Modules", t => t.ModuleId, cascadeDelete: true)
                .ForeignKey("dbo.Students", t => t.StudentId, cascadeDelete: true)
                .Index(t => t.ModuleId)
                .Index(t => t.StudentId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.StudentModulePreferences", "StudentId", "dbo.Students");
            DropForeignKey("dbo.StudentModulePreferences", "ModuleId", "dbo.Modules");
            DropIndex("dbo.StudentModulePreferences", new[] { "StudentId" });
            DropIndex("dbo.StudentModulePreferences", new[] { "ModuleId" });
            DropTable("dbo.StudentModulePreferences");
        }
    }
}

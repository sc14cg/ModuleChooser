namespace ModuleChooser.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_ModuleLinks_Table : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ModuleLinks",
                c => new
                    {
                        ModuleId = c.Guid(nullable: false),
                        PrerequisiteId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.ModuleId, t.PrerequisiteId })
                .ForeignKey("dbo.Modules", t => t.ModuleId, cascadeDelete: false)
                .ForeignKey("dbo.Modules", t => t.PrerequisiteId, cascadeDelete: false)
                .Index(t => t.ModuleId)
                .Index(t => t.PrerequisiteId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ModuleLinks", "PrerequisiteId", "dbo.Modules");
            DropForeignKey("dbo.ModuleLinks", "ModuleId", "dbo.Modules");
            DropIndex("dbo.ModuleLinks", new[] { "PrerequisiteId" });
            DropIndex("dbo.ModuleLinks", new[] { "ModuleId" });
            DropTable("dbo.ModuleLinks");
        }
    }
}

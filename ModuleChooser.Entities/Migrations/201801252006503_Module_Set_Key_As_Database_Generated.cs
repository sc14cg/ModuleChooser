namespace ModuleChooser.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Module_Set_Key_As_Database_Generated : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.Modules");
            AddColumn("dbo.Modules", "ModuleId", c => c.Guid(nullable: false, identity: true));
            AlterColumn("dbo.Modules", "ModuleCode", c => c.String(nullable: false, maxLength: 20));
            AlterColumn("dbo.Modules", "Title", c => c.String(nullable: false, maxLength: 100));
            AddPrimaryKey("dbo.Modules", "ModuleId");
            CreateIndex("dbo.Modules", "ModuleCode", unique: true);
            CreateIndex("dbo.Modules", "Title", unique: true);
        }
        
        public override void Down()
        {
            DropIndex("dbo.Modules", new[] { "Title" });
            DropIndex("dbo.Modules", new[] { "ModuleCode" });
            DropPrimaryKey("dbo.Modules");
            AlterColumn("dbo.Modules", "Title", c => c.String(nullable: false));
            AlterColumn("dbo.Modules", "ModuleCode", c => c.String(nullable: false, maxLength: 128));
            DropColumn("dbo.Modules", "ModuleId");
            AddPrimaryKey("dbo.Modules", "ModuleCode");
        }
    }
}

namespace ModuleChooser.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Modules",
                c => new
                    {
                        ModuleCode = c.String(nullable: false, maxLength: 128),
                        Title = c.String(),
                        Semester = c.Int(nullable: false),
                        Credits = c.Int(nullable: false),
                        YearOfStudy = c.Int(nullable: false),
                        Core = c.Boolean(nullable: false),
                        AppliedCSCore = c.Boolean(nullable: false),
                        CSCore = c.Boolean(nullable: false),
                        AppliedCSOption = c.Boolean(nullable: false),
                        CSOption = c.Boolean(nullable: false),
                        GeneralOption = c.Boolean(nullable: false),
                        CSWithAI = c.Boolean(nullable: false),
                        GraphicsAndEngineering = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ModuleCode);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Modules");
        }
    }
}

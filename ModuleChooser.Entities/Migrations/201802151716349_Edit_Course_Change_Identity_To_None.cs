namespace ModuleChooser.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Edit_Course_Change_Identity_To_None : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.CourseModules", "CourseId", "dbo.Courses");
            DropForeignKey("dbo.Students", "CourseId", "dbo.Courses");
            DropPrimaryKey("dbo.Courses");
            AlterColumn("dbo.Courses", "Id", c => c.Guid(nullable: false));
            AddPrimaryKey("dbo.Courses", "Id");
            AddForeignKey("dbo.CourseModules", "CourseId", "dbo.Courses", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Students", "CourseId", "dbo.Courses", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Students", "CourseId", "dbo.Courses");
            DropForeignKey("dbo.CourseModules", "CourseId", "dbo.Courses");
            DropPrimaryKey("dbo.Courses");
            AlterColumn("dbo.Courses", "Id", c => c.Guid(nullable: false, identity: true));
            AddPrimaryKey("dbo.Courses", "Id");
            AddForeignKey("dbo.Students", "CourseId", "dbo.Courses", "Id", cascadeDelete: true);
            AddForeignKey("dbo.CourseModules", "CourseId", "dbo.Courses", "Id", cascadeDelete: true);
        }
    }
}

namespace ModuleChooser.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_Users_And_Edit_Student_Accordingly : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.Students");
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        UserId = c.Guid(nullable: false, identity: true),
                        FirstName = c.String(nullable: false),
                        LastName = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.UserId);
            
            AddColumn("dbo.Students", "StudentId", c => c.Guid(nullable: false, identity: true));
            AddColumn("dbo.Students", "UserId", c => c.Guid(nullable: false));
            AddPrimaryKey("dbo.Students", "StudentId");
            CreateIndex("dbo.Students", "UserId");
            AddForeignKey("dbo.Students", "UserId", "dbo.Users", "UserId", cascadeDelete: true);
            DropColumn("dbo.Students", "Id");
            DropColumn("dbo.Students", "FirstName");
            DropColumn("dbo.Students", "LastName");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Students", "LastName", c => c.String(nullable: false));
            AddColumn("dbo.Students", "FirstName", c => c.String(nullable: false));
            AddColumn("dbo.Students", "Id", c => c.Guid(nullable: false));
            DropForeignKey("dbo.Students", "UserId", "dbo.Users");
            DropIndex("dbo.Students", new[] { "UserId" });
            DropPrimaryKey("dbo.Students");
            DropColumn("dbo.Students", "UserId");
            DropColumn("dbo.Students", "StudentId");
            DropTable("dbo.Users");
            AddPrimaryKey("dbo.Students", "Id");
        }
    }
}

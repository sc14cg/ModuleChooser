﻿using System.Data.Entity;

namespace ModuleChooser.Entities
{
    public class Context : DbContext
    {
        public Context() : base("name=ConnectionString")
        {

        }

        public DbSet<Module> Modules { get; set; }

        public DbSet<Course> Courses { get; set; }

        public DbSet<Student> Students { get; set; }

        public DbSet<ModuleLink> ModuleLinks { get; set; }

        public DbSet<Role> Roles { get; set; }

        public DbSet<User> Users { get; set; }

        public DbSet<Membership> Memberships { get; set; }

        public DbSet<Login> Logins { get; set; }

        public DbSet<CourseModule> CourseModules { get; set; }

        public DbSet<StudentModulePreference> StudentModulePreferences { get; set; }

        static void Main(string[] args)
        {

        }
    }
}

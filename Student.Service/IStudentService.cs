﻿using ModuleChooser.Resources;
using ModuleChooser.Resources.Dtos;
using ModuleChooser.Resources.Enums;
using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Threading.Tasks;

namespace Student.Service
{
    //Interface declerations for service methods defined here
    [ServiceContract]
    public interface IStudentService
    {
        [OperationContract]
        Task<List<ModuleInformationDto>> GetModules();

        [OperationContract]
        Task<List<StudentModuleInformationDto>> GetModulesForUser(string username);

        [OperationContract]
        CourseCompactDto GetCourseForStudentUser(string username);

        [OperationContract]
        List<PrerequisitesForModuleDto> GetPrerequisitesForModule(Guid moduleId);

        [OperationContract]
        List<FutureAvailableOptionsFromModuleDto> GetFutureAvailableOptionsForModule(Guid moduleId);

        [OperationContract]
        Task<List<Guid>> GetModulePreferencesForStudentUser(string username);

        [OperationContract]
        Task<SaveStudentPreferredModuleOptionResponse> SaveStudentPreferredModuleOption(Guid moduleId, string username);

        [OperationContract]
        Task RemoveStudentPreferredModuleOption(Guid moduleId, string username);

        [OperationContract]
        Task<string> GetStudentName(string username);

        Task<PreferredTogetherModulesChartDataDto> PreferredTogetherModuleSuggestion(string moduleCode, string username);
    }
}

﻿using ModuleChooser.Repositories;
using ModuleChooser.Resources;
using ModuleChooser.Resources.Dtos;
using ModuleChooser.Resources.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Student.Service
{
    public class StudentService : IStudentService
    {
        //Initialise the repositories to get data
        private readonly IModuleRepository _moduleRepository;
        private readonly ICourseRepository _courseRepository;
        private readonly IStudentRepository _studentRepository;
        private readonly IReportsRepository _reportsRepository;

        //Repositories injected to service and values are assigned to the variables
        public StudentService(
            IModuleRepository moduleRepository,
            ICourseRepository courseRepository,
            IStudentRepository studentRepository,
            IReportsRepository reportsRepository
            )
        {
            _moduleRepository = moduleRepository;
            _courseRepository = courseRepository;
            _studentRepository = studentRepository;
            _reportsRepository = reportsRepository;
        }

        public async Task<List<ModuleInformationDto>> GetModules()
        {
            //Call the GetModules method of the module repository and return the result
            return await _moduleRepository.GetModules();
        }
        public async Task<List<StudentModuleInformationDto>> GetModulesForUser(string username)
        {
            //Call the GetModulesForUser method of the module repository and return the result
            return await _moduleRepository.GetModulesForUser(username);
        }

        public CourseCompactDto GetCourseForStudentUser(string username)
        {
            //Call the GetCourseForStudentUser method of the course repository and return the result
            return _courseRepository.GetCourseForStudentUser(username);
        }

        public List<PrerequisitesForModuleDto> GetPrerequisitesForModule(Guid moduleId)
        {
            //Call the GetPrerequisitesForModule method of the module repository and return the result
            return _moduleRepository.GetPrerequisitesForModule(moduleId);
        }

        public List<FutureAvailableOptionsFromModuleDto> GetFutureAvailableOptionsForModule(Guid moduleId)
        {
            //Call the GetFutureAvailableOptionsForModule method of the module repository and return the result
            return _moduleRepository.GetFutureAvailableOptionsForModule(moduleId);
        }

        public async Task<List<Guid>> GetModulePreferencesForStudentUser(string username)
        {
            //Call the GetModulePreferencesForStudentUser method of the module repository and return the result
            return await _moduleRepository.GetModulePreferencesForStudentUser(username);
        }

        public async Task<SaveStudentPreferredModuleOptionResponse> SaveStudentPreferredModuleOption(Guid moduleId, string username)
        {
            //Call the SaveStudentPreferredModuleOption method of the module repository and return the result
            return await _moduleRepository.SaveStudentPreferredModuleOption(moduleId, username);
        }

        public async Task RemoveStudentPreferredModuleOption(Guid moduleId, string username)
        {
            //Call the RemoveStudentPreferredModuleOption method of the module repository
            await _moduleRepository.RemoveStudentPreferredModuleOption(moduleId, username);
        }

        public async Task<string> GetStudentName(string username)
        {
            //Call the GetStudentName method of the student repository and return the result
            return await _studentRepository.GetStudentName(username);
        }

        public async Task<PreferredTogetherModulesChartDataDto> PreferredTogetherModuleSuggestion(string moduleCode, string username)
        {
            //Call the GetCourseForStudentUser method of the course repository
            var course = _courseRepository.GetCourseForStudentUser(username);

            //Get the modules on the course
            var moudlesOnCourse = await _moduleRepository.GetModulesForCourse(course.Id);

            //Get the Percentage Of Times Module has been Preferred If the Selected Module Has Also Been Preferred
            var resultList = _reportsRepository.PercentageOfTimesModulePreferredIfSelectedModuleHasAlsoBeenPreferred(moduleCode);

            var possibleModuleChoices = new List<PreferredTogetherModulesChartDataDto>();

            //For each result, assign possible module choices to a module so a user can be presented with suggestions
            foreach (var result in resultList) {
                if (moudlesOnCourse.Any(m => m.ModuleCode == result.ModuleCode))
                {
                    possibleModuleChoices.Add(new PreferredTogetherModulesChartDataDto()
                    {
                        ModuleCode = result.ModuleCode,
                        PercentageOfTimesChosenTogether = result.PercentageOfTimesChosenTogether
                    });
                }
            }

            if (possibleModuleChoices.Any())
            {
                //Order the suggestions by highest percentage
                return possibleModuleChoices.OrderByDescending(r => r.PercentageOfTimesChosenTogether).First();
            }

            //Return null if no possible module suggestions
            return null;
        }
    }
}

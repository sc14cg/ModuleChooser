﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ModuleChooser.Repositories;
using ModuleChooser.Resources;
using ModuleChooser.Resources.Dtos;
using NSubstitute;

namespace Student.Service.Tests
{
    [TestClass]
    public class StudentServiceTests
    {
        //Mock the Student Service
        private IStudentService _studentService;
        private readonly IModuleRepository _moduleRepository = Substitute.For<IModuleRepository>();
        private readonly ICourseRepository _courseRepository = Substitute.For<ICourseRepository>();
        private readonly IStudentRepository _studentRepository = Substitute.For<IStudentRepository>();
        private readonly IReportsRepository _reportsRepository = Substitute.For<IReportsRepository>();

        public void SetUp()
        {
            //Set up service with mocked repo's
            _studentService = new StudentService(_moduleRepository, _courseRepository, _studentRepository, _reportsRepository);
        }

        [TestMethod]
        public async Task GetModules_Calls_ModuleRepositoryMethod()
        {
            SetUp();

            //Act
            await _studentService.GetModules();
            
            //Assert
            await _moduleRepository.Received(1).GetModules();
        }

        [TestMethod]
        public async Task GetModulesForUser_Calls_ModuleRepositoryMethod()
        {
            //SetUp
            SetUp();
            var username = "student";

            //Act
            await _studentService.GetModulesForUser(username);

            //Assert
            await _moduleRepository.Received(1).GetModulesForUser(username);
        }

        [TestMethod]
        public void GetCourseForStudentUser_Calls_CourseRepositoryMethod()
        {
            //SetUp
            SetUp();
            var username = "student";

            //Act
            _studentService.GetCourseForStudentUser(username);

            //Assert
            _courseRepository.Received(1).GetCourseForStudentUser(username);
        }

        [TestMethod]
        public void GetPrerequisitesForModule_Calls_ModuleRepositoryMethod()
        {            
            //SetUp
            SetUp();
            var moduleId = Guid.NewGuid();

            //Act
            _studentService.GetPrerequisitesForModule(moduleId);

            //Assert
            _moduleRepository.Received(1).GetPrerequisitesForModule(moduleId);
        }

        [TestMethod]
        public void GetFutureAvailableOptionsForModule_Calls_ModuleRepositoryMethod()
        {
            //SetUp
            SetUp();
            var moduleId = Guid.NewGuid();

            //Act
            _studentService.GetFutureAvailableOptionsForModule(moduleId);

            //Assert
            _moduleRepository.Received(1).GetFutureAvailableOptionsForModule(moduleId);
        }

        [TestMethod]
        public async Task GetModulePreferencesForStudentUser_Calls_ModuleRepositoryMethod()
        {
            //SetUp
            SetUp();
            var username = "student";

            //Act
            await _studentService.GetModulePreferencesForStudentUser(username);

            //Assert
            await _moduleRepository.Received(1).GetModulePreferencesForStudentUser(username);
        }

        [TestMethod]
        public async Task SaveStudentPreferredModuleOption_Calls_ModuleRepositoryMethod()
        {
            //SetUp
            SetUp();
            var username = "student";
            var moduleId = Guid.NewGuid();

            //Act
            await _studentService.SaveStudentPreferredModuleOption(moduleId, username);

            //Assert
            await _moduleRepository.Received(1).SaveStudentPreferredModuleOption(moduleId, username);
        }

        [TestMethod]
        public async Task RemoveStudentPreferredModuleOption_Calls_ModuleRepositoryMethod()
        {
            //SetUp
            SetUp();
            var username = "student";
            var moduleId = Guid.NewGuid();

            //Act
            await _studentService.RemoveStudentPreferredModuleOption(moduleId, username);

            //Assert
            await _moduleRepository.Received(1).RemoveStudentPreferredModuleOption(moduleId, username);
        }

        [TestMethod]
        public async Task GetStudentName_Calls_StudentRepositoryMethod()
        {
            //SetUp
            SetUp();
            var username = "student";

            //Act
            await _studentService.GetStudentName(username);

            //Assert
            await _studentRepository.Received(1).GetStudentName(username);
        }

        [TestMethod]
        public async Task PreferredTogetherModuleSuggestion_Calls_RepositoryMethods()
        {
            //SetUp
            SetUp();
            var username = "student";
            var moduleCode = "COMP1234";

            _courseRepository.GetCourseForStudentUser(Arg.Any<string>())
                .Returns(Substitute.For<CourseCompactDto>());

            _moduleRepository.GetModulesForCourse(Arg.Any<Guid>())
                .Returns(Task.FromResult(Substitute.For<List<StudentModuleInformationDto>>()));

            _reportsRepository.PercentageOfTimesModulePreferredIfSelectedModuleHasAlsoBeenPreferred(moduleCode)
                .Returns(Substitute.For<List<PreferredTogetherModulesChartDataDto>>());
            
            //Act
            await _studentService.PreferredTogetherModuleSuggestion(moduleCode, username);

            //Assert
            _courseRepository.Received(1).GetCourseForStudentUser(Arg.Any<string>());
            await _moduleRepository.Received(1).GetModulesForCourse(Arg.Any<Guid>());
            _reportsRepository.Received(1).PercentageOfTimesModulePreferredIfSelectedModuleHasAlsoBeenPreferred(moduleCode);


        }
    }
}
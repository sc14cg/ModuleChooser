﻿using ModuleChooser.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SignOn.Service
{
    public class SignOnService : ISignOnService
    {
        //Initialise the repository to get data
        private readonly IAccountRepository _accountRepository;

        //Repository injected to service and value is assigned to the variable
        public SignOnService(IAccountRepository accountRepository)
        {
            _accountRepository = accountRepository;
        }

        public async Task<List<string>> GetRolesForUser(string username)
        {
            //Call the GetRolesForUser method of the accounts repository and return the results
            return await _accountRepository.GetRolesForUser(username);
        }

        public async Task<bool> CheckLoginDetails(string username, string password)
        {
            //Call the CheckLoginDetails method of the accounts repository and return the result
            return await _accountRepository.CheckLoginDetails(username, password);
        }
    }
}

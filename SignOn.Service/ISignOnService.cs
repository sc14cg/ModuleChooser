﻿using System.Collections.Generic;
using System.ServiceModel;
using System.Threading.Tasks;

namespace SignOn.Service
{
    [ServiceContract]
    public interface ISignOnService
    {
        //Interface declerations for service methods defined here
        [OperationContract]
        Task<List<string>> GetRolesForUser(string username);

        [OperationContract]
        Task<bool> CheckLoginDetails(string username, string password);
    }
}

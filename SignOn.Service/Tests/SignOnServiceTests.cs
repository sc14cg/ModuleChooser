﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ModuleChooser.Repositories;
using NSubstitute;
using System.Threading.Tasks;

namespace SignOn.Service.Tests
{
    [TestClass]
    public class SignOnServiceTests
    {
        //Mock the Student Service
        private ISignOnService _signOnService;
        private readonly IAccountRepository _accountRepository = Substitute.For<IAccountRepository>();

        public void SetUp()
        {
            //Set up service with mocked repo's
            _signOnService = new SignOnService(_accountRepository);
        }

        [TestMethod]
        public async Task GetRolesForUser_Calls_AccountRepositoryMethod()
        {
            SetUp();

            //Act 
            await _signOnService.GetRolesForUser("student");

            //Assert
            await _accountRepository.Received(1).GetRolesForUser(Arg.Any<string>());
        }

        [TestMethod]
        public async Task CheckLoginDetails_Calls_AccountRepositoryMethod(string username, string password)
        {
            SetUp();

            //Act 
            await _signOnService.CheckLoginDetails("student", "TestPassword1");

            //Assert
            await _accountRepository.Received(1).CheckLoginDetails(Arg.Any<string>(), Arg.Any<string>());
        }
    }
}
﻿namespace ModuleChooser.Resources.Enums
{
    public enum SavePrerequisiteResponseEnum
    {
        Success,
        AlreadyExists,
        FutureModuleUsed,
    }
}

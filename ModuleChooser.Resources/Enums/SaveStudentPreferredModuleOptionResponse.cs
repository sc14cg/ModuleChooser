﻿namespace ModuleChooser.Resources.Enums
{
    public enum SaveStudentPreferredModuleOptionResponse
    {
        CreditsLimitReached,
        PreferenceAlreadyExists,
        Success
    }
}
﻿namespace ModuleChooser.Resources.Enums
{
    public enum SaveModuleResponseEnum
    {
        Success,
        CodeExists,
        TitleExists
    }
}

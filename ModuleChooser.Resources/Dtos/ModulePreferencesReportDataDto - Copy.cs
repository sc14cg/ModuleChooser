﻿namespace ModuleChooser.Resources.Dtos
{
    public class NumberOfTimesModulePreferredReportDataDto
    {
        public string ModuleCode { get; set; }

        public int NumberOfTimesPreferred { get; set; } 
    }
}

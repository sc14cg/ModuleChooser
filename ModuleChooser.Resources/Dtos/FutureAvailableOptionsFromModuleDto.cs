﻿using System;

namespace ModuleChooser.Resources.Dtos
{
    public class FutureAvailableOptionsFromModuleDto
    {
        public string FutureAvailableModuleCode { get; set; }

        public string FutureAvailableTitle { get; set; }

        public string FutureAvailableSemester { get; set; }

        public int FutureAvailableCredits { get; set; }

        public int FutureAvailableYearOfStudy { get; set; }

        public Guid FutureAvailableModuleId { get; set; }
    }
}

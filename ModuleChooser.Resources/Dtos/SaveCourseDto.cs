﻿using System;
namespace ModuleChooser.Resources
{
    public class SaveCourseDto
    {
        public Guid? CourseId { get; set; }

        public String Title { get; set; }
        
        public int NumberOfYears { get; set; }
    }
}

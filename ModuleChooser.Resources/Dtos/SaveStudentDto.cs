﻿using System;

namespace ModuleChooser.Resources
{
    public class SaveStudentDto
    {
        public Guid? StudentId { get; set; }

        public string FirstName { get; set; }

        public string Surname { get; set; }

        public Guid CourseId { get; set; }
        
        public int YearOfStudy { get; set; }

        public string Username { get; set; }
    }
}

﻿using System.Collections.Generic;

namespace ModuleChooser.Resources.Dtos
{
    public class StudentModuleInformationDto : ModuleInformationDto
    {
        public bool Compulsory { get; set; }

        public List<PrerequisitesForModuleDto> ModulePrerequisites { get; set; }

        public List<FutureAvailableOptionsFromModuleDto> FutureAvailableOptions { get; set; }

        public bool Preferred { get; set; }

        public bool AbleToBeChosen { get; set; }

        public PreferredTogetherModulesChartDataDto PreferredTogetherModule { get; set; }
    }
}
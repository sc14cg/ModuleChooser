﻿namespace ModuleChooser.Resources.Dtos
{
    public class MostPopularSetsOfPreferredModulesDto
    {
        public string ModuleSet { get; set; }

        public double PercentageOfStudentsWhoPreferModuleSet { get; set; }
    }
}

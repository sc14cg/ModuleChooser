﻿namespace ModuleChooser.Resources.Dtos
{
    public class PreferredTogetherModulesChartDataDto
    {
        public string ModuleCode { get; set; }

        public double PercentageOfTimesChosenTogether { get; set; } 
    }
}

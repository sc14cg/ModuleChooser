﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ModuleChooser.Resources
{
    public class SaveModuleDto
    {
        public Guid? ModuleId { get; set; }

        public String ModuleCode { get; set; }

        public String Title { get; set; }

        public int Semester { get; set; }
        
        public int Credits { get; set; }
        
        public int YearOfStudy { get; set; }

        public string Url { get; set; }
    }
}

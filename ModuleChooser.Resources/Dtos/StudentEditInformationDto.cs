﻿using System;

namespace ModuleChooser.Resources.Dtos
{
    public class StudentEditInformationDto
    {
        public Guid StudentId { get; set; }

        public string FirstName { get; set; }

        public string Surname { get; set; }

        public int YearOfStudy { get; set; }

        public Guid CourseId { get; set; }
    }
}

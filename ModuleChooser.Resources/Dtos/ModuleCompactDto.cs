﻿using System;

namespace ModuleChooser.Resources
{
    public class ModuleCompactDto
    {
        public Guid ModuleId { get; set; }

        public string Title { get; set; }

        public string ModuleCode { get; set; } 
    }
}

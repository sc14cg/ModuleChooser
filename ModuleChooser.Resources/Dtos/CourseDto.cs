﻿using System;
using System.Collections.Generic;

namespace ModuleChooser.Resources
{
    public class CourseDto
    {
        public Guid Id { get; set; }

        public string Title { get; set; }

        public ICollection<ModuleDto> Modules { get; set; } 
    }
}

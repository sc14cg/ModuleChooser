﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModuleChooser.Resources.Dtos
{
    public class PrerequisiteListDto
    {
        public string Module { get; set; }

        public string Prerequisite { get; set; }
    }
}

﻿using System;

namespace ModuleChooser.Resources.Dtos
{
    public class CourseModuleDto
    {
        public Guid CourseId { get; set; }

        public Guid ModuleId { get; set; }

        public bool Compulsory { get; set; }
    }
}

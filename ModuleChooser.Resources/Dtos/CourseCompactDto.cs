﻿using System;
using System.Collections.Generic;

namespace ModuleChooser.Resources
{
    public class CourseCompactDto
    {
        public Guid Id { get; set; }

        public string Title { get; set; }
    }
}

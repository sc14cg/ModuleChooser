﻿using System;

namespace ModuleChooser.Resources.Dtos
{
    public class PrerequisitesForModuleDto
    {
        public string PrerequisiteModuleCode { get; set; }

        public string PrerequisiteTitle { get; set; }

        public string PrerequisiteSemester { get; set; }

        public int PrerequisiteCredits { get; set; }

        public int PrerequisiteYearOfStudy { get; set; }

        public Guid PrerequisiteModuleId { get; set; }

        public bool PrerequisiteCompulsory { get; set; }

        public bool PrerequisitePreferred { get; set; }
    }
}

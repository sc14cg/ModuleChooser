﻿using System;
using System.Collections.Generic;

namespace ModuleChooser.Resources
{
    public class ModuleDto
    {
        public String ModuleCode { get; set; }

        public String Title { get; set; }

        public int Semester { get; set; }
        
        public int Credits { get; set; }
        
        public int YearOfStudy { get; set; }

        public ICollection<CourseDto> Courses { get; set; }
    }
}

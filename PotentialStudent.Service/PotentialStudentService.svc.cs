﻿using ModuleChooser.Repositories;
using ModuleChooser.Resources;
using ModuleChooser.Resources.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PotentialStudent.Service
{
    public class PotentialStudentService : IPotentialStudentService
    {
        //Initialise the repositories to get data
        private readonly IModuleRepository _moduleRepository;
        private readonly ICourseRepository _courseRepository;

        //Repositories injected to service and values are assigned to the variables
        public PotentialStudentService(
            IModuleRepository moduleRepository, 
            ICourseRepository courseRepository)
        {
            _moduleRepository = moduleRepository;
            _courseRepository = courseRepository;
        }

        public async Task<List<ModuleInformationDto>> GetModules()
        {
            //Call the GetModules method of the module repository and return the results
            return await _moduleRepository.GetModules();
        }

        public async Task<List<StudentModuleInformationDto>> GetModulesForCourse(Guid courseId)
        {
            //Call the GetModulesForCourse method of the module repository and return the results
            return await _moduleRepository.GetModulesForCourse(courseId);
        }
        
        public List<PrerequisitesForModuleDto> GetPrerequisitesForModule(Guid moduleId)
        {
            //Call the GetPrerequisitesForModule method of the module repository and return the results
            return _moduleRepository.GetPrerequisitesForModule(moduleId);
        }
        
        public List<FutureAvailableOptionsFromModuleDto> GetFutureAvailableOptionsForModule(Guid moduleId)
        {
            //Call the GetFutureAvailableOptionsForModule method of the module repository and return the results
            return _moduleRepository.GetFutureAvailableOptionsForModule(moduleId);
        }

        public async Task<List<CourseCompactDto>> GetCourses()
        {
            //Call the GetCourses method of the course repository and return the results
            return await _courseRepository.GetCourses();
        }
    }
}

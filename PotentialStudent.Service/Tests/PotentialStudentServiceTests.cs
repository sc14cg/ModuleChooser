﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ModuleChooser.Repositories;
using NSubstitute;
using System;
using System.Threading.Tasks;

namespace PotentialStudent.Service.Tests
{
    [TestClass]
    public class PotentialStudentServiceTests
    {
        //Mock the Student Service
        private IPotentialStudentService _potentialStudentService;
        private readonly IModuleRepository _moduleRepository = Substitute.For<IModuleRepository>();
        private readonly ICourseRepository _courseRepository = Substitute.For<ICourseRepository>();

        public void SetUp()
        {
            //Set up service with mocked repo's
            _potentialStudentService = new PotentialStudentService(_moduleRepository, _courseRepository);
        }

        [TestMethod]
        public async Task GetModules_Calls_ModuleRepositoryMethod()
        {
            SetUp();

            //Act
            await _potentialStudentService.GetModules();

            //Assert
            await _moduleRepository.Received(1).GetModules();
        }

        [TestMethod]
        public async Task GetModulesForCourse_Calls_ModuleRepositoryMethod()
        {
            SetUp();

            //Act
            await _potentialStudentService.GetModulesForCourse(Guid.NewGuid());

            //Assert
            await _moduleRepository.Received(1).GetModulesForCourse(Arg.Any<Guid>());
        }

        [TestMethod]
        public void GetPrerequisitesForModule_Calls_ModuleRepositoryMethod()
        {
            SetUp();

            //Act
            _potentialStudentService.GetPrerequisitesForModule(Guid.NewGuid());

            //Assert
            _moduleRepository.Received(1).GetPrerequisitesForModule(Arg.Any<Guid>());
        }

        [TestMethod]
        public void GetFutureAvailableOptionsForModule_Calls_ModuleRepositoryMethod()
        {
            SetUp();

            //Act
            _potentialStudentService.GetFutureAvailableOptionsForModule(Guid.NewGuid());

            //Assert
            _moduleRepository.Received(1).GetFutureAvailableOptionsForModule(Arg.Any<Guid>());
        }

        [TestMethod]
        public async Task GetCourses_Calls_CourseRepositoryMethod()
        {
            SetUp();

            //Act
            await _potentialStudentService.GetCourses();

            //Assert
            await _courseRepository.Received(1).GetCourses();
        }
    }
}
﻿using ModuleChooser.Repositories;
using ModuleChooser.Resources;
using ModuleChooser.Resources.Dtos;
using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Threading.Tasks;

namespace PotentialStudent.Service
{
    //Interface declerations for service methods defined here
    [ServiceContract]
    public interface IPotentialStudentService
    {
        [OperationContract]
        Task<List<ModuleInformationDto>> GetModules();

        [OperationContract]
        Task<List<StudentModuleInformationDto>> GetModulesForCourse(Guid courseId);

        [OperationContract]
        List<PrerequisitesForModuleDto> GetPrerequisitesForModule(Guid moduleId);

        [OperationContract]
        List<FutureAvailableOptionsFromModuleDto> GetFutureAvailableOptionsForModule(Guid moduleId);

        [OperationContract]
        Task<List<CourseCompactDto>> GetCourses();
    }
}

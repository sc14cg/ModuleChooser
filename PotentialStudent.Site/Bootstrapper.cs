using System.Web.Mvc;
using Unity.AspNet.Mvc;
using PotentialStudent.Service;
using ModuleChooser.Repositories;
using PotentialStudent.Site.Mappings;
using AutoMapper;
using PotentialStudent.Site.Controllers;
using Unity;
using Unity.Injection;

namespace PotentialStudent.Site
{
    public static class Bootstrapper
    {
        public static void Initialise()
        {
            //Create unity container and set dependency resolver
            var container = BuildUnityContainer();

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }

        private static IUnityContainer BuildUnityContainer()
        {
            //Create Container
            var container = new UnityContainer();

            //Register Repositories to container
            container.RegisterType<IModuleRepository, ModuleRepository>();
            container.RegisterType<ICourseRepository, CourseRepository>();

            //Register Services to container
            container.RegisterType<IPotentialStudentService, PotentialStudentService>();

            //Register Module Mappings to container
            var moduleMapperConfig = ModuleMappings.IntialiseAutoMapper();
            moduleMapperConfig.CreateMapper();
            container.RegisterType<IMapper, Mapper>(new InjectionConstructor(moduleMapperConfig));

            //Register Course Mappings to container
            var courseMapperConfig = ModuleMappings.IntialiseAutoMapper();
            courseMapperConfig.CreateMapper();
            container.RegisterType<IMapper, Mapper>(new InjectionConstructor(courseMapperConfig));

            //Register Controller to container
            container.RegisterType<IController, ModuleController>("Module");

            return container;
        }
    }
}
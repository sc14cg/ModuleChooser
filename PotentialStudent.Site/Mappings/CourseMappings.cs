﻿using AutoMapper;
using PotentialStudent.Site.Models;
using ModuleChooser.Resources;

namespace PotentialStudent.Site.Mappings
{
    public class CourseMappings
    {
        public static MapperConfiguration IntialiseAutoMapper()
        {
            //Use AutoMapper to map one class to another
            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<CourseCompactDto, CourseCompactModel>();
            });

            return config;
        }
    }
}
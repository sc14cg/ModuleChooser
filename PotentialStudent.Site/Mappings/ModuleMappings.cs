﻿using AutoMapper;
using PotentialStudent.Site.Models;
using ModuleChooser.Repositories;
using ModuleChooser.Resources;
using ModuleChooser.Resources.Dtos;

namespace PotentialStudent.Site.Mappings
{
    public class ModuleMappings
    {
        public static MapperConfiguration IntialiseAutoMapper()
        {
            //Use AutoMapper to map one class to another
            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<ModuleInformationDto, ModuleModel>();
                cfg.CreateMap<ModuleInformationDto, ModuleListModel>();
                cfg.CreateMap<StudentModuleInformationDto, ModuleListModel>();
                cfg.CreateMap<ModuleModel, ModuleInformationDto>();
            });

            return config;
        }
    }
}
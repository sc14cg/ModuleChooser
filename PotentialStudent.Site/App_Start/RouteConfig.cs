﻿using System.Web.Mvc;
using System.Web.Routing;

namespace PotentialStudent.Site
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{site}/{controller}/{action}/{id}",
                defaults: new { site = "PotentialStudent", controller = "Module", action = "ModuleList", id = UrlParameter.Optional },
                namespaces: new[] { "PotentialStudent.Site.Controllers.ModuleController" }
            );
        }
    }
}

﻿using System;

namespace PotentialStudent.Site.Models
{
    public class CourseCompactModel
    {
        public Guid Id { get; set; }

        public string Title { get; set; }
    }
}
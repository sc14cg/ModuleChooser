﻿using AutoMapper;
using PotentialStudent.Service;
using PotentialStudent.Site.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace PotentialStudent.Site.Controllers
{
    public class ModuleController : Controller
    {
        //Initialise the service to get data and the mapper used to map classes of one type to another
        private readonly IPotentialStudentService _service;
        private readonly IMapper _mapper;

        //Service and mappers injected to controller and values are assigned to the variables
        public ModuleController(IPotentialStudentService service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        public async Task<ViewResult> ModuleList()
        {
            //Get the courses from the database as a list of dtos.
            var courses = await _service.GetCourses();

            //Map the dto to the compact course model list to be used by the view
            var model = _mapper.Map<List<CourseCompactModel>>(courses);

            //Render the modulelist view, passing through the model
            return View(model);
        }

        public async Task<ViewResult> ModuleListPartial(Guid courseId)
        {
            //Get the modules from the database for the course as a dto list.
            var dtos = await _service.GetModulesForCourse(courseId);

            //Order the modules by compulsory ones, then by year of study and then by their semester
            dtos = dtos.OrderBy(m => m.Compulsory)
                    .ThenBy(m => m.YearOfStudy).ThenBy(m => m.Semester).ToList();

            //Get all of the compulsory modules
            var compulsoryModules = dtos.Where(m => m.Compulsory).Select(m => m.ModuleId);

            //Don't include compulsory modules in the foreach as they aren't relevant for this sections
            foreach (var module in dtos.Where(m => !m.Compulsory))
            {
                //Get the module prereqs. Order these by year and semester
                module.ModulePrerequisites = _service.GetPrerequisitesForModule(module.ModuleId)
                    .OrderBy(p => p.PrerequisiteYearOfStudy).ThenBy(p => p.PrerequisiteSemester).ToList();

                //Iterate over the module prereqs
                foreach (var mp in module.ModulePrerequisites)
                {
                    //Set the prereq as a compulsory prerequisite if it is one
                    if (compulsoryModules.Contains(mp.PrerequisiteModuleId))
                    {
                        mp.PrerequisiteCompulsory = true;
                    }
                }

                //If all of the module prerequisites are compulsory then make that module able to be chosen
                if (module.ModulePrerequisites.All(mp => mp.PrerequisiteCompulsory))
                {
                    module.AbleToBeChosen = true;
                }

                //Get all of the options that are enabled if this module is chosen and order by the year and semester
                module.FutureAvailableOptions = _service.GetFutureAvailableOptionsForModule(module.ModuleId)
                    .OrderBy(fao => fao.FutureAvailableYearOfStudy).ThenBy(fao => fao.FutureAvailableSemester).ToList();
            }

            //Map the dto list to the ModuleListModel list. This is later used by the view to render data on screen
            var model = _mapper.Map<List<ModuleListModel>>(dtos);

            //Render the modulelist partial view, passing through the model
            return View("_ModuleListPartial", model);
        }
    }
}
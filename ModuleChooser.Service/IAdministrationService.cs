﻿using ModuleChooser.Repositories;
using ModuleChooser.Resources;
using ModuleChooser.Resources.Dtos;
using ModuleChooser.Resources.Enums;
using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Threading.Tasks;

namespace Administration.Service
{
    //Interface declerations for service methods defined here
    [ServiceContract]
    public interface IAdministrationService
    {
        [OperationContract]
        Task<List<ModuleInformationDto>> GetModules();

        [OperationContract]
        Task<SaveModuleResponseEnum> AddNewModule(SaveModuleDto dto);

        [OperationContract]
        ModuleEditInformationDto GetModuleEditInformation(Guid moduleId);

        [OperationContract]
        Task<SaveModuleResponseEnum> SaveExistingModule(SaveModuleDto dto);

        [OperationContract]
        Task<List<ModuleCompactDto>> GetCompactModules();

        [OperationContract]
        Task<SavePrerequisiteResponseEnum> SavePrerequisite(Guid moduleId, Guid prerequisiteId);

        [OperationContract]
        Task<List<CourseCompactDto>> GetCourses();

        [OperationContract]
        Task SaveModuleToCourse(Guid moduleId, Guid courseId, bool compulsory);

        [OperationContract]
        Task RemoveModuleFromCourse(Guid moduleId, Guid courseId);

        [OperationContract]
        Task<List<CourseModuleDto>> GetModulesForCourse(Guid courseId);

        [OperationContract]
        Task ChangeCompulsoryStatus(Guid moduleId, Guid courseId);

        [OperationContract]
        Task<bool> SaveNewCourse(SaveCourseDto dto);

        [OperationContract]
        Task<List<StudentListInformationDto>> GetStudents();

        [OperationContract]
        Task<StudentEditInformationDto> GetStudentEditInformation(Guid studentId);

        [OperationContract]
        Task SaveExistingStudent(SaveStudentDto dtos);

        [OperationContract]
        Task<List<PrerequisiteListDto>> GetPrerequisites();

        [OperationContract]
        Task SaveNewStudent(SaveStudentDto dto);

        [OperationContract]
        Task<List<NumberOfTimesModulePreferredReportDataDto>> NumberOfTimesModulePreferredReportData();

        [OperationContract]
        List<PreferredTogetherModulesChartDataDto> PercentageOfStudentsWhoPreferCertainModulesTogetherChartData(string moduleCode);

        [OperationContract]
        List<PreferredTogetherModulesChartDataDto> PercentageOfTimesModulePreferredIfSelectedModuleHasAlsoBeenPreferredChartData(string moduleCode);

        [OperationContract]
        List<MostPopularSetsOfPreferredModulesDto> MostPopularSetsOfPreferredModules();
    }
}

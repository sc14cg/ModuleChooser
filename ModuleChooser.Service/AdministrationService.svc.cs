﻿using ModuleChooser.Repositories;
using ModuleChooser.Resources;
using ModuleChooser.Resources.Dtos;
using ModuleChooser.Resources.Enums;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Administration.Service
{
    public class AdministrationService : IAdministrationService
    {
        //Initialise the repositories to get data
        private readonly IModuleRepository _moduleRepository;
        private readonly ICourseRepository _courseRepository;
        private readonly IStudentRepository _studentRepository;
        private readonly IReportsRepository _reportsRepository;

        //Repositories injected to service and values are assigned to the variables
        public AdministrationService(
            IModuleRepository moduleRepository,
            ICourseRepository courseRepository,
            IStudentRepository studentRepository,
            IReportsRepository reportsRepository
            )
        {
            _moduleRepository = moduleRepository;
            _courseRepository = courseRepository;
            _studentRepository = studentRepository;
            _reportsRepository = reportsRepository;
        }

        public async Task<List<ModuleInformationDto>> GetModules()
        {
            //Call the GetModules method of the module repository and return the results
            return await _moduleRepository.GetModules();
        }

        public async Task<List<CourseCompactDto>> GetCourses()
        {
            //Call the GetCourses method of the course repository and return the results
            return await _courseRepository.GetCourses();
        }

        public async Task<SaveModuleResponseEnum> AddNewModule(SaveModuleDto dto)
        {
            //Call the AddNewModule method of the module repository and return the result
            return await _moduleRepository.AddNewModule(dto);
        }

        public async Task<SaveModuleResponseEnum> SaveExistingModule(SaveModuleDto dto)
        {
            //Call the SaveExistingModule method of the module repository and return the result
            return await _moduleRepository.SaveExistingModule(dto);            
        }

        public ModuleEditInformationDto GetModuleEditInformation(Guid moduleId)
        {
            //Call the GetModuleEditInformation method of the module repository and return the result
            return _moduleRepository.GetModuleEditInformation(moduleId);
        }

        public Task<List<ModuleCompactDto>> GetCompactModules()
        {
            //Call the GetCompactModules method of the module repository and return the results
            return _moduleRepository.GetCompactModules();
        }

        public async Task<SavePrerequisiteResponseEnum> SavePrerequisite(Guid moduleId, Guid prerequisiteId)
        {
            //Call the SavePrerequisite method of the module repository and return the result
            return await _moduleRepository.SavePrerequisite(moduleId, prerequisiteId);
        }

        public async Task SaveModuleToCourse(Guid moduleId, Guid courseId, bool compulsory)
        {
            //Call the SaveModuleToCourse method of the course repository
            await _courseRepository.SaveModuleToCourse(moduleId, courseId, compulsory);
        }

        public async Task RemoveModuleFromCourse(Guid moduleId, Guid courseId)
        {
            //Call the RemoveModuleFromCourse method of the course repository 
            await _courseRepository.RemoveModuleFromCourse(moduleId, courseId);
        }

        public async Task<List<CourseModuleDto>> GetModulesForCourse(Guid courseId)
        {
            //Call the RemoveModuleFromCourse method of the course repository and return the results
            return await _courseRepository.GetModulesForCourse(courseId);
        }

        public async Task ChangeCompulsoryStatus(Guid moduleId, Guid courseId)
        {
            //Call the ChangeCompulsoryStatus method of the course repository 
            await _courseRepository.ChangeCompulsoryStatus(moduleId, courseId);
        }

        public async Task<bool> SaveNewCourse(SaveCourseDto dto)
        {
            //Call the SaveNewCourse method of the course repository and return the result
            return await _courseRepository.SaveNewCourse(dto);
        }

        public async Task<List<StudentListInformationDto>> GetStudents()
        {
            //Call the GetStudents method of the student repository and return the results
            return await _studentRepository.GetStudents();
        }

        public async Task<StudentEditInformationDto> GetStudentEditInformation(Guid studentId)
        {
            //Call the GetStudentEditInformation method of the student repository and return the result
            return await _studentRepository.GetStudentEditInformation(studentId);
        }

        public async Task SaveExistingStudent(SaveStudentDto dto)
        {
            //Call the SaveExistingStudent method of the student repository
            await _studentRepository.SaveExistingStudent(dto);
        }

        public async Task<List<PrerequisiteListDto>> GetPrerequisites()
        {
            //Call the GetPrerequisites method of the module repository and return the results
            return await _moduleRepository.GetPrerequisites();
        }

        public async Task SaveNewStudent(SaveStudentDto dto)
        {
            //Call the SaveNewStudent method of the student repository 
            await _studentRepository.SaveNewStudent(dto);
        }

        public async Task<List<NumberOfTimesModulePreferredReportDataDto>> NumberOfTimesModulePreferredReportData()
        {
            //Call the NumberOfTimesModulePreferredReportData method of the reports repository and return the results
            return await _reportsRepository.NumberOfTimesModulePreferredReportData();
        }

        public List<PreferredTogetherModulesChartDataDto> PercentageOfStudentsWhoPreferCertainModulesTogetherChartData(string moduleCode)
        {
            //Call the PercentageOfStudentsWhoPreferCertainModulesTogetherChartData method of the reports repository and return the results
            return _reportsRepository.PercentageOfStudentsWhoPreferCertainModulesTogetherChartData(moduleCode);
        }

        public List<PreferredTogetherModulesChartDataDto> PercentageOfTimesModulePreferredIfSelectedModuleHasAlsoBeenPreferredChartData(string moduleCode)
        {
            //Call the PercentageOfTimesModulePreferredIfSelectedModuleHasAlsoBeenPreferredChartData method of the reports repository and return the results
            return _reportsRepository.PercentageOfTimesModulePreferredIfSelectedModuleHasAlsoBeenPreferredChartData(moduleCode);
        }

        public List<MostPopularSetsOfPreferredModulesDto> MostPopularSetsOfPreferredModules()
        {
            //Call the MostPopularSetsOfPreferredModules method of the reports repository and return the results
            return _reportsRepository.MostPopularSetsOfPreferredModules();
        }
    }
}

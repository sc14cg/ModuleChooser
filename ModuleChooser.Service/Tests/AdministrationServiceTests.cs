﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ModuleChooser.Repositories;
using ModuleChooser.Resources;
using NSubstitute;
using System;
using System.Threading.Tasks;

namespace Administration.Service.Tests
{
    [TestClass]
    public class AdministrationServiceTests
    {
        //Mock the Student Service
        private IAdministrationService _administrationService;
        private readonly IModuleRepository _moduleRepository = Substitute.For<IModuleRepository>();
        private readonly ICourseRepository _courseRepository = Substitute.For<ICourseRepository>();
        private readonly IStudentRepository _studentRepository = Substitute.For<IStudentRepository>();
        private readonly IReportsRepository _reportsRepository = Substitute.For<IReportsRepository>();

        public void SetUp()
        {
            //Set up service with mocked repo's
            _administrationService = new AdministrationService(_moduleRepository, _courseRepository, _studentRepository, _reportsRepository);
        }

        [TestMethod]
        public async Task GetModules_Calls_ModuleRepositoryMethod()
        {
            SetUp();

            //Act
            await _administrationService.GetModules();

            //Assert
            await _moduleRepository.Received(1).GetModules();
        }

        [TestMethod]
        public async Task GetCourses_Calls_CourseRepositoryMethod()
        {
            SetUp();

            //Act
            await _administrationService.GetCourses();

            //Assert
            await _courseRepository.Received(1).GetCourses();
        }

        [TestMethod]
        public async Task AddNewModule_Calls_ModuleRepositoryMethod()
        {
            SetUp();

            //Act
            await _administrationService.AddNewModule(Substitute.For<SaveModuleDto>());

            //Assert
            await _moduleRepository.Received(1).AddNewModule(Arg.Any<SaveModuleDto>());
        }

        [TestMethod]
        public async Task SaveExistingModule_Calls_ModuleRepositoryMethod()
        {
            SetUp();

            //Act
            await _administrationService.SaveExistingModule(Substitute.For<SaveModuleDto>());

            //Assert
            await _moduleRepository.Received(1).SaveExistingModule(Arg.Any<SaveModuleDto>());
        }

        [TestMethod]
        public void GetModuleEditInformation_Calls_ModuleRepositoryMethod()
        {
            SetUp();

            //Act
            _administrationService.GetModuleEditInformation(Guid.NewGuid());

            //Assert
            _moduleRepository.Received(1).GetModuleEditInformation(Arg.Any<Guid>());
        }

        [TestMethod]
        public async Task GetCompactModules_Calls_ModuleRepositoryMethod()
        {
            SetUp();

            //Act
            await _administrationService.GetCompactModules();

            //Assert
            await _moduleRepository.Received(1).GetCompactModules();
        }

        [TestMethod]
        public async Task SavePrerequisite_Calls_ModuleRepositoryMethod()
        {
            SetUp();

            //Act
            await _administrationService.SavePrerequisite(Guid.NewGuid(), Guid.NewGuid());

            //Assert
            await _moduleRepository.Received(1).SavePrerequisite(Arg.Any<Guid>(), Arg.Any<Guid>());
        }

        [TestMethod]
        public async Task SaveModuleToCourse_Calls_CourseRepositoryMethod()
        {
            SetUp();

            //Act
            await _administrationService.SaveModuleToCourse(Guid.NewGuid(), Guid.NewGuid(), true);

            //Assert
            await _courseRepository.Received(1).SaveModuleToCourse(Arg.Any<Guid>(), Arg.Any<Guid>(), Arg.Any<bool>());
        }

        [TestMethod]
        public async Task RemoveModuleFromCourse_Calls_CourseRepositoryMethod()
        {
            SetUp();

            //Act
            await _administrationService.RemoveModuleFromCourse(Guid.NewGuid(), Guid.NewGuid());

            //Assert
            await _courseRepository.Received(1).RemoveModuleFromCourse(Arg.Any<Guid>(), Arg.Any<Guid>());
        }

        [TestMethod]
        public async Task GetModulesForCourse_Calls_CourseRepositoryMethod()
        {
            SetUp();

            //Act
            await _administrationService.GetModulesForCourse(Guid.NewGuid());

            //Assert
            await _courseRepository.Received(1).GetModulesForCourse(Arg.Any<Guid>());
        }

        [TestMethod]
        public async Task ChangeCompulsoryStatus_Calls_CourseRepositoryMethod()
        {
            SetUp();

            //Act
            await _administrationService.ChangeCompulsoryStatus(Guid.NewGuid(), Guid.NewGuid());

            //Assert
            await _courseRepository.Received(1).ChangeCompulsoryStatus(Arg.Any<Guid>(), Arg.Any<Guid>());
        }

        [TestMethod]
        public async Task SaveNewCourse_Calls_CourseRepositoryMethod()
        {
            SetUp();

            //Act
            await _administrationService.SaveNewCourse(Substitute.For<SaveCourseDto>());

            //Assert
            await _courseRepository.Received(1).SaveNewCourse(Arg.Any<SaveCourseDto>());
        }

        [TestMethod]
        public async Task GetStudents_Calls_StudentRepositoryMethod()
        {
            SetUp();

            //Act
            await _administrationService.GetStudents();

            //Assert
            await _studentRepository.Received(1).GetStudents();
        }

        [TestMethod]
        public async Task GetStudentEditInformation_Calls_StudentRepositoryMethod()
        {
            SetUp();

            //Act
            await _administrationService.GetStudentEditInformation(Guid.NewGuid());

            //Assert
            await _studentRepository.Received(1).GetStudentEditInformation(Arg.Any<Guid>());
        }

        [TestMethod]
        public async Task SaveExistingStudent_Calls_StudentRepositoryMethod()
        {
            SetUp();

            //Act
            await _administrationService.SaveExistingStudent(Substitute.For<SaveStudentDto>());

            //Assert
            await _studentRepository.Received(1).SaveExistingStudent(Arg.Any<SaveStudentDto>());
        }

        [TestMethod]
        public async Task GetPrerequisites_Calls_ModuleRepositoryMethod()
        {
            SetUp();

            //Act
            await _administrationService.GetPrerequisites();

            //Assert
            await _moduleRepository.Received(1).GetPrerequisites();
        }

        [TestMethod]
        public async Task SaveNewStudent_Calls_StudentRepositoryMethod()
        {
            SetUp();

            //Act
            await _administrationService.SaveNewStudent(Substitute.For<SaveStudentDto>());

            //Assert
            await _studentRepository.Received(1).SaveNewStudent(Arg.Any<SaveStudentDto>());
        }

        [TestMethod]
        public async Task NumberOfTimesModulePreferredReportData_Calls_ReportsRepositoryMethod()
        {
            SetUp();

            //Act
            await _administrationService.NumberOfTimesModulePreferredReportData();

            //Assert
            await _reportsRepository.Received(1).NumberOfTimesModulePreferredReportData();
        }

        [TestMethod]
        public void PercentageOfStudentsWhoPreferCertainModulesTogetherChartData_Calls_ReportsRepositoryMethod()
        {
            SetUp();

            //Act
            _administrationService.PercentageOfStudentsWhoPreferCertainModulesTogetherChartData("modulecode");

            //Assert
            _reportsRepository.Received(1).PercentageOfStudentsWhoPreferCertainModulesTogetherChartData(Arg.Any<string>());
        }

        [TestMethod]
        public void PercentageOfTimesModulePreferredIfSelectedModuleHasAlsoBeenPreferredChartData_Calls_ReportsRepositoryMethod()
        {
            SetUp();

            //Act
            _administrationService.PercentageOfTimesModulePreferredIfSelectedModuleHasAlsoBeenPreferredChartData("modulecode");

            //Assert
            _reportsRepository.Received(1).PercentageOfTimesModulePreferredIfSelectedModuleHasAlsoBeenPreferredChartData(Arg.Any<string>());
        }

        [TestMethod]
        public void MostPopularSetsOfPreferredModules_Calls_ReportsRepositoryMethod()
        {
            SetUp();

            //Act
            _administrationService.MostPopularSetsOfPreferredModules();

            //Assert
            _reportsRepository.Received(1).MostPopularSetsOfPreferredModules();
        }
    }
}
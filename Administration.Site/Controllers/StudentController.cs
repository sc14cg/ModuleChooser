﻿using AutoMapper;
using Administration.Site.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using System;
using ModuleChooser.Resources;
using Administration.Service;
using SignOn.Helpers;

namespace Administration.Site.Controllers
{
    //The user must have the role of an administrator to view the site.
    [ModuleChooserAuthorize(Roles = "Admin")]
    public class StudentController : Controller
    {
        //Initialise the service to get data and the mapper used to map classes of one type to another
        private readonly IAdministrationService _service;
        private readonly IMapper _mapper;

        //Service and mappers injected to controller and values are assigned to the variables
        public StudentController(IAdministrationService service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        public async Task<ViewResult> StudentList()
        {
            //Get all of the students
            var dtos = await _service.GetStudents();

            //Map the dto information to the model to be used by the view.
            var model = _mapper.Map<List<StudentModel>>(dtos);

            //Pass the model to the view and render StudentList page
            return View("StudentList", model);
        }

        public async Task<ActionResult> EditStudent(Guid studentId)
        {
            //Create new edit student model
            var model = new StudentEditModel();

            //Get all the courses
            var courses = await _service.GetCourses();

            //Get all of the informatiion needed to edit a student
            var studentInfoDto = await _service.GetStudentEditInformation(studentId);

            //Assign the student dto information to the model
            model.FirstName = studentInfoDto.FirstName;
            model.Surname = studentInfoDto.Surname;
            model.StudentId = studentId;
            model.YearOfStudy = studentInfoDto.YearOfStudy;
            
            //For each course, add an option to the select list. Assign the default selected value to the course the student is currently on.
            foreach(var course in courses)
            {
                model.CourseOptions.Add(new SelectListItem
                {
                    Text = course.Title,
                    Disabled = false,
                    Selected = studentInfoDto.CourseId == course.Id,
                    Value = course.Id.ToString(),
                });
            }

            //Render the edit student view, passing through the model
            return View(model);
        }

        public async Task<ActionResult> AddStudent()
        {
            //Create a new Add Student Model
            var model = new StudentAddModel();

            //Get all of the courses
            var courses = await _service.GetCourses();

            //For each course, add it as an option in the select list 
            foreach (var course in courses)
            {
                model.CourseOptions.Add(new SelectListItem
                {
                    Text = course.Title,
                    Disabled = false,
                    Value = course.Id.ToString(),
                });
            }

            //Render the AddStudent View, passing through the model
            return View(model);
        }

        public async Task<ActionResult> SaveExistingStudent(StudentEditModel model)
        {
            //Check that the form information is valid. If not return back to the view.
            if (!ModelState.IsValid)
            {
                //Get all of the courses
                var courses = await _service.GetCourses();

                //For each course, add it as an option in the select list 
                foreach (var course in courses)
                {
                    model.CourseOptions.Add(new SelectListItem
                    {
                        Text = course.Title,
                        Disabled = false,
                        Value = course.Id.ToString(),
                    });
                }

                return View("EditStudent", model);
            }

            //Create a new dto with the model information assigned, to be passed to the save method
            var dto = new SaveStudentDto()
            {
                CourseId = Guid.Parse(model.Course),
                FirstName = model.FirstName,
                YearOfStudy = model.YearOfStudy,
                StudentId = model.StudentId,
                Surname = model.Surname
            };

            //Save the existing student information
            await _service.SaveExistingStudent(dto);

            //Redirect to the student list action.
            return RedirectToAction("StudentList");
        }

        public async Task<ActionResult> AddNewStudent(StudentAddModel model)
        {
            //Check that the form information is valid. If not return back to the view.
            if (!ModelState.IsValid)
            {
                //Get all of the courses
                var courses = await _service.GetCourses();

                //For each course, add it as an option in the select list 
                foreach (var course in courses)
                {
                    model.CourseOptions.Add(new SelectListItem
                    {
                        Text = course.Title,
                        Disabled = false,
                        Value = course.Id.ToString(),
                    });
                }

                return View("AddStudent", model);
            }

            //Create a new dto with the model information assigned, to be passed to the save method
            var dto = new SaveStudentDto()
            {
                CourseId = Guid.Parse(model.Course),
                FirstName = model.FirstName,
                YearOfStudy = 1,
                Surname = model.Surname,   
                Username = model.Username
            };

            //Save the new student information
            await _service.SaveNewStudent(dto);

            //Redirect to the student list action.
            return RedirectToAction("StudentList");
        }
    }
}
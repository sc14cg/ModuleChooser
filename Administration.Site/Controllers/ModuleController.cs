﻿using AutoMapper;
using Administration.Site.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using System;
using ModuleChooser.Resources;
using Administration.Service;
using Administration.Models;
using System.Linq;
using SignOn.Helpers;
using ModuleChooser.Resources.Enums;

namespace Administration.Site.Controllers
{
    //The user must have the role of an administrator to view the site.
    [ModuleChooserAuthorize(Roles = "Admin")]
    public class ModuleController : Controller
    {
        //Initialise the service to get data and the mapper used to map classes of one type to another
        private readonly IAdministrationService _service;
        private readonly IMapper _mapper;

        //Service and mappers injected to controller and values are assigned to the variables
        public ModuleController(IAdministrationService service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        public async Task<ViewResult> ModuleList()
        {
            //Get the modules from the database as a dto.
            var dtos = await _service.GetModules();

            //Map the dto to the module model list to be used by the view
            var model = _mapper.Map<List<ModuleModel>>(dtos);

            //Render the modulelist view, passing through the model
            return View("ModuleList", model);
        }

        public ViewResult AddModule()
        {
            //Create a new instance of the ModuleAddModel and pass this to the View function
            var model = new ModuleAddModel();

            //Render AddModle page
            return View("AddModule", model);
        }

        public ViewResult Edit(Guid moduleId)
        {
            //Get the module information to be used by the edit page
            var dto = _service.GetModuleEditInformation(moduleId);

            //Map the dto object to the ModuleEditModel to be used in the View
            var model = _mapper.Map<ModuleEditModel>(dto);

            //Render the Edit view, passing through the model
            return View("Edit", model);
        }

        public async Task<ActionResult> SaveNewModule(ModuleAddModel model)
        {
            //Check if the form data is correct, if not return to the view
            if (!ModelState.IsValid)
            {
                return View("AddModule", model);
            }

            //Create a new dto used to save the module
            var dto = _mapper.Map<SaveModuleDto>(model);

            //Attempt to save the module. Check if this was successful
            var response = await _service.AddNewModule(dto);

            //If the response indicates the title exists already then return this error response.
            //If the response indicates the module code exists already then return this error response.
            //if everything is ok and the module saved, redirect to the modulelist action.
            switch (response)
            {
                case SaveModuleResponseEnum.TitleExists:
                    ModelState.AddModelError("Title", "A module with this title already exists");
                    return View("AddModule", model);
                    
                case SaveModuleResponseEnum.CodeExists:
                    ModelState.AddModelError("ModuleCode", "A module with this code already exists");
                    return View("AddModule", model);

                default:
                    return RedirectToAction("ModuleList");
            }
        }

        public async Task<ActionResult> SaveExistingModule(ModuleEditModel model)
        {
            //Check if the form data is correct, if not return to the view
            if (!ModelState.IsValid)
            {
                return View("Edit", model);
            }

            //Create a new dto used to save the module
            var dto = _mapper.Map<SaveModuleDto>(model);

            //Attempt to save the existing module. Check if this was successful
            var response = await _service.SaveExistingModule(dto);

            //If the response indicates the title exists already then return this error response.
            //If the response indicates the module code exists already then return this error response.
            //if everything is ok and the module saved, redirect to the modulelist action.
            switch (response)
            {
                case SaveModuleResponseEnum.TitleExists:
                    ModelState.AddModelError("Title", "A module with this title already exists");
                    return View("Edit", model);

                case SaveModuleResponseEnum.CodeExists:
                    ModelState.AddModelError("ModuleCode", "A module with this code already exists");
                    return View("Edit", model);

                default:
                    return RedirectToAction("ModuleList");
            }
        }

        public async Task<ViewResult> Prerequisites()
        {
            //Get all the prerequisites that have been added to the database
            var prerequisites = await _service.GetPrerequisites();

            //Return the prerequisites view, passing the list of items to it
            return View(prerequisites);
        }

        public async Task<ViewResult> AddPrerequisite()
        {            
            //Get all the relevant module information
            var modules = await _service.GetCompactModules();

            //Create a dropdown of all the modules
            var modulesDropDownList = modules.Select(s => new SelectListItem()
            {
                Text = s.Title + " (" + s.ModuleCode + ")",
                Value = s.ModuleId.ToString()
            });

            //Add the dropdowns to a module list and prerequisite list to be used to add prerequisites
            var model = new AddPrerequisiteModel()
            {
                Modules = modulesDropDownList,
                PrerequisiteModules = modulesDropDownList
            };

            //Return the AddPrerequisites view, passing through the model
            return View(model);
        }

        public async Task<JsonResult> SavePrerequisite(Guid moduleId, Guid prerequisiteId)
        {
            //Attempt to save a prerequisite, and capture the response.
            var response = await _service.SavePrerequisite(moduleId, prerequisiteId);

            //Return response as a JSON payload to the view.
            return Json(response);
        }
    }
}
﻿using System.Threading.Tasks;
using System.Web.Mvc;
using Administration.Service;
using System.Linq;
using SignOn.Helpers;

namespace Administration.Site.Controllers
{
    //The user must have the role of an administrator to view the site.
    [ModuleChooserAuthorize(Roles = "Admin")]
    public class ReportsController : Controller
    {
        //Initialise the service to get data
        private readonly IAdministrationService _service;

        //Service injected to controller and value are assigned to the variable
        public ReportsController(IAdministrationService service)
        {
            _service = service;
        }

        public ViewResult Reports()
        {
            //Return the basic reports view.
            return View();
        }

        public async Task<ViewResult> NumberOfTimesModulePreferred()
        {
            //Get the number of times that the module has been preferred
            var dtos = await _service.NumberOfTimesModulePreferredReportData();

            //Order this list by most prefered in descending order
            dtos = dtos.OrderByDescending(d => d.NumberOfTimesPreferred).ToList();

            //Return _MostPreferredModules partial view, passing the reporting information
            return View("_MostPreferredModules", dtos);
        }

        public async Task<ViewResult> StudentsWhoPreferCertainModulesTogether()
        {
            //Get all the modules.
            var dtos = await _service.GetModules();

            //Extract only modules which aren't in first year (as these are all compulsory in first year so won't have preferences)
            dtos = dtos.Where(m => m.YearOfStudy != 1).ToList();

            //Return _StudentsWhoPreferCertainModulesTogethers partial view, passing the module information
            return View("_StudentsWhoPreferCertainModulesTogether", dtos);
        }

        public ViewResult PercentageOfStudentsWhoPreferCertainModulesTogetherChartData(string moduleCode)
        {
            //Get a list of the modules and the percentage of students that prefer modules together. E.g. 30% of students prefer COMP1234 and COMP1233 together
            var dtos = _service.PercentageOfStudentsWhoPreferCertainModulesTogetherChartData(moduleCode);

            //Order this list by percentage of students who have chosen modules together in descending order
            dtos = dtos.OrderByDescending(d => d.PercentageOfTimesChosenTogether).ToList();

            //Pass the module code as an extra value for the view
            ViewBag.ModuleCode = moduleCode;

            //Return _StudentsWhoPreferCertainModulesTogethers partial view, passing the reporting information
            return View("_StudentsWhoPreferCertainModulesTogetherChart", dtos);
        }

        public async Task<ViewResult> ModulesPreferredIfAnotherModuleHasAlsoBeenPreferred()
        {
            //Get all the modules.
            var dtos = await _service.GetModules();
            //Extract only modules which aren't in first year (as these are all compulsory in first year so won't have preferences)
            dtos = dtos.Where(m => m.YearOfStudy != 1).ToList();

            //Return _ModulesPreferredIfAnotherModuleHasAlsoBeenPreferred partial view, passing the module information
            return View("_ModulesPreferredIfAnotherModuleHasAlsoBeenPreferred", dtos);
        }

        public ViewResult PercentageOfTimesModulePreferredIfSelectedModuleHasAlsoBeenPreferredChartData(string moduleCode)
        {
            //Get a list of the modules and the percentage of students that prefer modules if they have also 
            //chosen another module. E.g. 30% of students who choose COMP1234 will also choose COMP1233. 
            //However only 15% of students who choose COMP1233 will also choose COMP1234.
            var dtos = _service.PercentageOfTimesModulePreferredIfSelectedModuleHasAlsoBeenPreferredChartData(moduleCode);

            //Order this list by most prefered if another has been prefered in descending order
            dtos = dtos.OrderByDescending(d => d.PercentageOfTimesChosenTogether).ToList();

            //Pass the module code as an extra value for the view
            ViewBag.ModuleCode = moduleCode;

            //Return _ModulesPreferredIfAnotherModuleHasAlsoBeenPreferredChart partial view, passing the reporting information
            return View("_ModulesPreferredIfAnotherModuleHasAlsoBeenPreferredChart", dtos);
        }

        public ViewResult MostPopularSetsOfPreferredModules()
        {
            //Get the most prefered sets of modules, e.g. COMP1234 and COMP1233 are prefered 55% of the time together
            var dtos = _service.MostPopularSetsOfPreferredModules();

            //Return _MostPopularSetsOfPreferredModules partial view, passing the reporting information
            return View("_MostPopularSetsOfPreferredModules", dtos);
        }
    }
}
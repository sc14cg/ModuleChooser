﻿using AutoMapper;
using Administration.Site.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using System;
using ModuleChooser.Resources;
using Administration.Service;
using System.Linq;
using SignOn.Helpers;

namespace Administration.Site.Controllers
{
    //The user must have the role of an administrator to view the site.
    [ModuleChooserAuthorize(Roles = "Admin")]
    public class CourseController : Controller
    {
        //Initialise the service to get data and the mapper used to map classes of one type to another
        private readonly IAdministrationService _service;
        private readonly IMapper _mapper;

        //Service and mappers injected to controller and values are assigned to the variables
        public CourseController(IAdministrationService service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        public async Task<ViewResult> CourseList()
        {
            //Get the courses from the database
            var dtos = await _service.GetCourses();

            //Map the courses dto to the course model list to be used by the CourseList view
            var model = _mapper.Map<List<CourseModel>>(dtos);

            //Render the CourseList view
            return View("CourseList", model);
        }

        public async Task<ViewResult> AddModulesToCourse(Guid courseId)
        {
            //Get the modules which are available for a course
            var courseModules = await _service.GetModulesForCourse(courseId);

            //Get information on all modules
            var dtos = await _service.GetModules();

            //Map the module information dtos to the model
            var model = _mapper.Map<List<AddModulesToCourseModel>>(dtos);

            //for each module do the following
            foreach(var item in model)
            {
                //Check if the module is already assigned to the course
                var courseModule = courseModules.FirstOrDefault(cm => cm.ModuleId == item.ModuleId);

                //if the module is already assigned to the course then set the compulsory and option value.
                //Otherwise do nothing
                if (courseModule != null)
                {
                    item.Compulsory = courseModule.Compulsory;
                    item.Option = !courseModule.Compulsory;
                }
            }            

            //Set the course id to be used by the view
            ViewBag.CourseId = courseId;

            //Render the AddModulesToCourse view along with the view model.
            return View("AddModulesToCourse", model);
        }
        
        public async Task SaveModuleToCourse(Guid moduleId, Guid courseId, bool compulsory)
        {
            //Using the moduleid, courseid and whether the module is compulsory or not, save the module to the course
            await _service.SaveModuleToCourse(moduleId, courseId, compulsory);
        }

        public async Task RemoveModuleFromCourse(Guid moduleId, Guid courseId)
        {
            //Using the moduleid, and courseid, remove the module from the course
            await _service.RemoveModuleFromCourse(moduleId, courseId);
        }

        public async Task ChangeCompulsoryStatus(Guid moduleId, Guid courseId)
        {
            //Using the moduleid, and courseid, change the compulsory status of the course
            await _service.ChangeCompulsoryStatus(moduleId, courseId);
        }

        public ViewResult AddCourse()
        {
            //Render the AddCourse view passing through the model
            var model = new AddCourseModel();
            return View(model);
        }

        public async Task<ActionResult> SaveNewCourse(AddCourseModel model)
        {
            //Check if the form data is correct, if not return to the view
            if (!ModelState.IsValid)
            {
                return View("AddCourse", model);
            }

            //Create a new dto used to save the course
            var dto = new SaveCourseDto()
            {
                NumberOfYears = model.NumberOfYears,
                Title = model.Title
            };

            //Attempt to save the course. Check if this was successful
            var savedSuccessfully = await _service.SaveNewCourse(dto);

            //If the course already exists with the same title then return a model error stating that the course already exists.
            if (!savedSuccessfully) {
                ModelState.AddModelError("Title", "A course with this title already exists");
                return View("AddCourse", model);
            }

            //If the course saves successfully then redirect to the CourseList view method.
            return RedirectToAction("CourseList");
        }
    }
}
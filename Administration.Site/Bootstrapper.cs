using System.Web.Mvc;
using Administration.Site.Controllers;
using ModuleChooser.Repositories;
using AutoMapper;
using Administration.Site.Mappings;
using Administration.Service;
using Unity;
using Unity.Injection;
using Unity.AspNet.Mvc;

namespace Administration
{
    public static class Bootstrapper
    {
        public static void Initialise()
        {
            //Create unity container and set dependency resolver
            var container = BuildUnityContainer();

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }

        private static IUnityContainer BuildUnityContainer()
        {
            //Create Container
            var container = new UnityContainer();
            
            //Register Repositories to container
            container.RegisterType<IModuleRepository, ModuleRepository>();
            container.RegisterType<ICourseRepository, CourseRepository>();
            container.RegisterType<IStudentRepository, StudentRepository>();
            container.RegisterType<IReportsRepository, ReportsRepository>();

            //Register Services to container
            container.RegisterType<IAdministrationService, AdministrationService>();

            //Register AutoMapper to container
            var mapperConfig = ModuleMappings.IntialiseAutoMapper();
            var mapper = mapperConfig.CreateMapper();
            container.RegisterType<IMapper, Mapper>(new InjectionConstructor(mapperConfig));

            //Register Controllers to container
            container.RegisterType<IController,  ModuleController>("Module");
            container.RegisterType<IController, StudentController>("Student");
            container.RegisterType<IController, CourseController>("Course");
            container.RegisterType<IController, CourseController>("Reports");

            return container;
        }
    }
}
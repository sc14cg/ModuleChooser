﻿using ModuleChooser.Entities;
using System;
using System.Web.Security;
using System.Linq;

namespace Administration.Site.MyRoleProvider
{
    public class SiteRole : RoleProvider
    {
        private readonly Context _entities = new Context();

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override string ApplicationName
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override void CreateRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new NotImplementedException();
        }

        public override string[] GetAllRoles()
        {
            throw new NotImplementedException();
        }

        public override string[] GetRolesForUser(string username)
        {  
            //Using the users username, get the login details and then get the users roles from the database
            var login = _entities.Logins.Find(username);
            var result = _entities.Memberships.Where(r => r.UserId == login.UserId)
                .Select(m => m.Role.Name)
                .ToArray();

            return result;
        }

        public override string[] GetUsersInRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool IsUserInRole(string username, string roleName)
        {
            //Get the login details and check if the user has the role specified
            var login = _entities.Logins.Find(username);
            var result = _entities.Memberships
                .Any(r => r.UserId == login.UserId && r.Role.Name == roleName);

            return result;
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override bool RoleExists(string roleName)
        {
            throw new NotImplementedException();
        }
    }
}
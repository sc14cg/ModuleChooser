﻿using AutoMapper;
using Administration.Site.Models;
using ModuleChooser.Resources;

namespace Administration.Site.Mappings
{
    public class CourseMappings
    {
        public static MapperConfiguration IntialiseAutoMapper()
        { 
            //Use AutoMapper to map one class to another
            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<CourseModel, CourseCompactDto>();

                cfg.CreateMap<AddCourseModel, SaveCourseDto>()
                    .ForMember(dst => dst.CourseId, opt => opt.Ignore());
            });

            return config;
        }
    }
}
﻿using AutoMapper;
using Administration.Site.Models;
using ModuleChooser.Resources.Dtos;

namespace Administration.Site.Mappings
{
    public class StudentMappings
    {
        public static MapperConfiguration IntialiseAutoMapper()
        {
            //Use AutoMapper to map one class to another
            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<StudentListInformationDto, StudentModel>();
            });

            return config;
        }
    }
}
﻿using AutoMapper;
using Administration.Site.Models;
using ModuleChooser.Repositories;
using ModuleChooser.Resources;

namespace Administration.Site.Mappings
{
    public class ModuleMappings
    {
        public static MapperConfiguration IntialiseAutoMapper()
        {
            //Use AutoMapper to map one class to another
            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<ModuleInformationDto, ModuleModel>();
                cfg.CreateMap<ModuleModel, ModuleInformationDto>();
                cfg.CreateMap<ModuleAddModel, SaveModuleDto>()
                    .ForMember(dst => dst.ModuleId, opt => opt.Ignore());

                //Edit
                cfg.CreateMap<ModuleEditInformationDto, ModuleEditModel>();

                cfg.CreateMap<ModuleInformationDto, AddModulesToCourseModel>();
            });

            return config;
        }
    }
}
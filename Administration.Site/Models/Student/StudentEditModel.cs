﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using ModuleChooser.Resources.Strings;

namespace Administration.Site.Models
{
    public class StudentEditModel
    {
        public Guid StudentId { get; set; }

        [DisplayName("First Name")]
        [Required]
        public string FirstName { get; set; }
        
        [Required]
        public string Surname { get; set; }

        [DisplayName("Year of Study")]
        [Required(ErrorMessageResourceName = "YearOfStudyRequired", ErrorMessageResourceType = typeof(Strings))]
        public int YearOfStudy { get; set; }

        public IEnumerable<SelectListItem> YearOptions =
            new List<SelectListItem>
            {
                new SelectListItem
                {
                    Text = "Select your option",
                    Disabled = true,
                    Selected = true,
                    Value = "0",

                },
                new SelectListItem
                {
                    Text = "1",
                    Disabled = false,
                    Selected = false,
                    Value = "1",

                },
                new SelectListItem
                {
                    Text = "2",
                    Disabled = false,
                    Selected = false,
                    Value = "2"
                },
                new SelectListItem
                {
                    Text = "3",
                    Disabled = false,
                    Selected = false,
                    Value = "3"
                },
                new SelectListItem
                {
                    Text = "4",
                    Disabled = false,
                    Selected = false,
                    Value = "4"
                },
                new SelectListItem
                {
                    Text = "5",
                    Disabled = false,
                    Selected = false,
                    Value = "5"
                },
            };

        [DisplayName("Course")]
        [Required]
        public String Course { get; set; }

        public List<SelectListItem> CourseOptions = 
            new List<SelectListItem>
            {
                new SelectListItem
                {
                    Text = "Select the course",
                    Disabled = true,
                    Selected = true,
                    Value = "0",

                }
            };        
    }
}
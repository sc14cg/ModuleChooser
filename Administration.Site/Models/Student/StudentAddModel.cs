﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using ModuleChooser.Resources.Strings;

namespace Administration.Site.Models
{
    public class StudentAddModel
    {
        [DisplayName("First Name")]
        [Required]
        public string FirstName { get; set; }
        
        [Required]
        public string Surname { get; set; }
       

        [DisplayName("Course")]
        [Required]
        public string Course { get; set; }

        public List<SelectListItem> CourseOptions = 
            new List<SelectListItem>
            {
                new SelectListItem
                {
                    Text = "Select the course",
                    Disabled = true,
                    Selected = true,
                    Value = "0",

                }
            };

        [DisplayName("Username")]
        public string Username { get; set; }
    }
}
﻿using System;

namespace Administration.Site.Models
{
    public class StudentModel
    {
        public Guid StudentId { get; set; }

        public string FullName { get; set; }

        public int YearOfStudy { get; set; }

        public Guid CourseId { get; set; }

        public string CourseTitle { get; set; }
    }
}
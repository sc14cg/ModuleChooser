﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using ModuleChooser.Resources.Strings;

namespace Administration.Site.Models
{
    public class ModuleAddModel
    {
        [DisplayName("Module Code")]
        [Required(ErrorMessageResourceName = "ModuleCodeRequired", ErrorMessageResourceType = typeof(Strings))]
        public String ModuleCode { get; set; }

        [DisplayName("Title")]
        [Required(ErrorMessageResourceName = "TitleRequired", ErrorMessageResourceType = typeof(Strings))]
        public String Title { get; set; }

        [DisplayName("Semester")]
        [Required(ErrorMessageResourceName = "SemesterRequired", ErrorMessageResourceType = typeof(Strings))]
        public String Semester { get; set; }

        public IEnumerable<SelectListItem> SemesterOptions = 
            new List<SelectListItem>
            {
                new SelectListItem
                {
                    Text = "Select your option",
                    Disabled = true,
                    Selected = true,
                    Value = "0",

                },
                new SelectListItem
                {
                    Text = "One",
                    Disabled = false,
                    Selected = false,
                    Value = "1"
                },
                new SelectListItem
                {
                    Text = "Two",
                    Disabled = false,
                    Selected = false,
                    Value = "2"
                },
                new SelectListItem
                {
                    Text = "Both",
                    Disabled = false,
                    Selected = false,
                    Value = "3"
                },
            };
        
        [DisplayName("Credits")]
        [Required(ErrorMessageResourceName = "CreditsRequired", ErrorMessageResourceType = typeof(Strings))]
        [Range(0, 120, ErrorMessage = "Please enter a number between 0 and 120.")]
        public int Credits { get; set; }

        [DisplayName("Year of Study")]
        [Required(ErrorMessageResourceName = "YearOfStudyRequired", ErrorMessageResourceType = typeof(Strings))]
        public int YearOfStudy { get; set; }

        public IEnumerable<SelectListItem> YearOptions = 
            new List<SelectListItem>
            {
                new SelectListItem
                {
                    Text = "Select your option",
                    Disabled = true,
                    Selected = true,
                    Value = "0",

                },
                new SelectListItem
                {
                    Text = "1",
                    Disabled = false,
                    Selected = false,
                    Value = "1",

                },
                new SelectListItem
                {
                    Text = "2",
                    Disabled = false,
                    Selected = false,
                    Value = "2"
                },
                new SelectListItem
                {
                    Text = "3",
                    Disabled = false,
                    Selected = false,
                    Value = "3"
                },
                new SelectListItem
                {
                    Text = "4",
                    Disabled = false,
                    Selected = false,
                    Value = "4"
                },
                new SelectListItem
                {
                    Text = "5",
                    Disabled = false,
                    Selected = false,
                    Value = "5"
                },
            };
    }
}
﻿using System;

namespace Administration.Site.Models
{
    public class AddModulesToCourseModel
    {
        public String ModuleCode { get; set; }

        public String Title { get; set; }

        public int Semester { get; set; }

        public int Credits { get; set; }

        public int YearOfStudy { get; set; }

        public Guid ModuleId { get; set; }

        public bool Compulsory { get; set; }

        public bool Option { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Administration.Models
{
    public class AddPrerequisiteModel
    {
        [DisplayName("Module")]
        [Required]
        public String Module { get; set; }

        public IEnumerable<SelectListItem> Modules { get; set; }

        [DisplayName("Prerequisite")]
        [Required]
        public String Prerequisite { get; set; }

        public IEnumerable<SelectListItem> PrerequisiteModules { get; set; }
    }
}
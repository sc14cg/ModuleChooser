﻿using System;

namespace Administration.Site.Models
{
    public class ModuleModel
    {
        public ModuleModel()
        {

        }

        public String ModuleCode { get; set; }

        public String Title { get; set; }

        public int Semester { get; set; }

        public int Credits { get; set; }

        public int YearOfStudy { get; set; }

        public Guid ModuleId { get; set; }
    }
}
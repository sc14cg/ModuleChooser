﻿using System;

namespace Administration.Site.Models
{
    public class CourseModel
    {
        public Guid Id { get; set; }

        public String Title { get; set; }
    }
}
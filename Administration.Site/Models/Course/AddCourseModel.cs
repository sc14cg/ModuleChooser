﻿using ModuleChooser.Resources.Strings;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Administration.Site.Models
{
    public class AddCourseModel
    {
        [Required]
        public String Title { get; set; }

        [DisplayName("Length of Course (years)")]
        [Required(ErrorMessageResourceName = "YearOfStudyRequired", ErrorMessageResourceType = typeof(Strings)), Range(1, 5)]
        public int NumberOfYears { get; set; }

        public IEnumerable<SelectListItem> YearOptions =
            new List<SelectListItem>
            {
                new SelectListItem
                {
                    Text = "Select your option",
                    Disabled = true,
                    Selected = true,
                    Value = "0",

                },
                new SelectListItem
                {
                    Text = "1",
                    Disabled = false,
                    Selected = false,
                    Value = "1",

                },
                new SelectListItem
                {
                    Text = "2",
                    Disabled = false,
                    Selected = false,
                    Value = "2"
                },
                new SelectListItem
                {
                    Text = "3",
                    Disabled = false,
                    Selected = false,
                    Value = "3"
                },
                new SelectListItem
                {
                    Text = "4",
                    Disabled = false,
                    Selected = false,
                    Value = "4"
                },
                new SelectListItem
                {
                    Text = "5",
                    Disabled = false,
                    Selected = false,
                    Value = "5"
                },
            };
    }
}